create table if not exists AUTHOR (
	AUTHOR_ID int  primary key not null,
	AUTHOR_NAME nvarchar2(30) not null,
	EXPIRED timestamp,
)