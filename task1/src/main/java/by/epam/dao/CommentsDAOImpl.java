package by.epam.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import by.epam.entities.Comments;
import by.epam.entities.News;
import by.epam.exceptions.ActionNotDoneSQLException;

public class CommentsDAOImpl implements IDAO<Comments> {
	private final static String SQL_INSERT_COMMENT = "INSERT INTO K_COMMENT (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES (?,?,?,?)";
	private final static String SQL_SELECT_COMMENT_BY_ID = "SELECT NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM K_COMMENT WHERE COMMENT_ID = ?";
	private final static String SQL_DELETE_COMMENT = "DELETE FROM K_COMMENT WHERE COMMENT_ID = ?";
	private final static String SQL_UPDATE_COMMENT = "UPDATE K_COMMENT SET NEWS_ID = ? COMMENT_TEXT = ?, CREATION_DATE = ? WHERE COMMENT_ID = ?";
	private final static String SQL_SELECT_COUNT_OF_COMMENT_BY_NEWS = "SELECT COUNT(*) AS COMMENTS_COUNT FROM COMMENTS WHERE NEWS_ID = ?";
	DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Comments read(Long commentId) throws SQLException, ActionNotDoneSQLException {
		Comments comments = new Comments();
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		ps = connection.prepareStatement(SQL_SELECT_COMMENT_BY_ID);
		ps.setLong(1, commentId);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			comments.setCommentId(commentId);
			comments.setNewsId(rs.getLong("NEWS_ID"));
			comments.setCommentText(rs.getString("COMMENT_TEXT"));
			comments.setCreationDate(rs.getTimestamp("CREATION_DATE"));
		} else
			throw new ActionNotDoneSQLException("can't read the comment");
		return comments;
	}

	public void delete(Comments comment) throws SQLException, ActionNotDoneSQLException {
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		int count = 0;
		ps = connection.prepareStatement(SQL_DELETE_COMMENT);
		ps.setLong(1, comment.getCommentId());
		count = ps.executeUpdate();
		if (count == 0) {
			throw new ActionNotDoneSQLException("can't delete the comment");
		}
	}

	public void create(Comments comment) throws SQLException, ActionNotDoneSQLException {
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		int count = 0;
		ps = connection.prepareStatement(SQL_INSERT_COMMENT);
		ps.setLong(1, comment.getCommentId());
		ps.setString(2, comment.getCommentText());
		ps.setTimestamp(3, comment.getCreationDate());
		count = ps.executeUpdate();
		if (count == 0) {
			throw new ActionNotDoneSQLException("can't insert the comment");
		}
	}

	public void update(Comments comment) throws SQLException, ActionNotDoneSQLException {
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		int count = 0;
		ps = connection.prepareStatement(SQL_UPDATE_COMMENT);
		ps.setLong(1, comment.getNewsId());
		ps.setString(2, comment.getCommentText());
		ps.setTimestamp(3, comment.getCreationDate());
		ps.setLong(4, comment.getCommentId());
		count = ps.executeUpdate();
		if (count == 0) {
			throw new ActionNotDoneSQLException("can't update the comment");
		}
	}

	public int commentsCount(News news) throws SQLException, ActionNotDoneSQLException {
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		int count = 0;
		ps = connection.prepareStatement(SQL_SELECT_COUNT_OF_COMMENT_BY_NEWS);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			count = rs.getInt("COMMENTS_COUNT");
		} else
			throw new ActionNotDoneSQLException("can't  count comments");
		return count;
	}
}
