package by.epam.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import by.epam.entities.Author;
import by.epam.exceptions.ActionNotDoneSQLException;

@ContextConfiguration(locations = "dataSource.xml")
public class AuthorDAOImpl implements IDAO<Author> {
	private final static String SQL_SELECT_AUTHOR_BY_ID = "SELECT AUTHO_NAME,EXPIRED FROM AUTHOR WHERE AUTHOR_ID = ?";
	private final static String SQL_DELETE_AUTHOR = "UPDATE AUTHOR SET EXPIRED = ? WHERE AUTHOR_ID = ?";
	private final static String SQL_INSERT_AUTHOR = "INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (?,?)";
	private final static String SQL_SELECT_ALL_AUTHORS = "SELECT AUTHOR_ID,AUTHOR_NAME,EXPIRED FROM AUTHORS";
	private final static String SQL_UPDATE_AUTHOR = "UPDATE AUTHOR SET AUTHOR_NAME = ?, EXPIRED = ? WHERE AUTHOR_ID = ?";
	@Autowired
	DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public Author read(Long authorId) throws SQLException, ActionNotDoneSQLException {
		Author author = new Author();
		Connection connection = dataSource.getConnection();
		PreparedStatement ps = null;
		ps = connection.prepareStatement(SQL_SELECT_AUTHOR_BY_ID);
		ps.setLong(1, authorId);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			author.setAuthorId(authorId);
			author.setAuthorName(rs.getString("AUTHO_NAME"));
			author.setExpired(rs.getTimestamp("EXPIRED"));
		} else
			throw new ActionNotDoneSQLException("can't read the author");
		return author;
	}

	public void delete(Author author) throws SQLException, ActionNotDoneSQLException {
		PreparedStatement ps = null;
		int count = 0;
		Connection connection = dataSource.getConnection();
		ps = connection.prepareStatement(SQL_DELETE_AUTHOR);
		ps.setTimestamp(1, author.getExpired());
		ps.setLong(2, author.getAuthorId());
		count = ps.executeUpdate();
		if (count == 0) {
			throw new ActionNotDoneSQLException("can't delete the author");
		}
	}

	public void create(Author author) throws SQLException, ActionNotDoneSQLException {
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		int count = 0;
		ps = connection.prepareStatement(SQL_INSERT_AUTHOR);
		ps.setLong(1, author.getAuthorId());
		ps.setString(2, author.getAuthorName());
		count = ps.executeUpdate();
		if (count == 0) {
			throw new ActionNotDoneSQLException("can't insert the author");
		}
	}

	public void update(Author author) throws SQLException, ActionNotDoneSQLException {
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		int count = 0;
		ps = connection.prepareStatement(SQL_UPDATE_AUTHOR);
		ps.setString(1, author.getAuthorName());
		ps.setTimestamp(2, author.getExpired());
		ps.setLong(3, author.getAuthorId());
		count = ps.executeUpdate();
		if (count == 0) {
			throw new ActionNotDoneSQLException("can't update the author");
		}
	}

	public List<Author> viewAuthors() throws SQLException {
		List<Author> authorList = new LinkedList<Author>();
		Connection connection = dataSource.getConnection();
		Statement s = connection.createStatement();
		Author author;
		ResultSet rs = s.executeQuery(SQL_SELECT_ALL_AUTHORS);
		while (rs.next()) {
			author = new Author();
			author.setAuthorId(rs.getLong("AUTHOR_ID"));
			author.setAuthorName(rs.getString("AUTHOR_NAME"));
			author.setExpired(rs.getTimestamp("EXPIRED"));
			authorList.add(author);
		}
		return authorList;

	}

}
