package by.epam.dao;

import java.sql.SQLException;

import by.epam.exceptions.ActionNotDoneSQLException;

public interface IDAO<T> {

	public abstract T read(Long id) throws SQLException, ActionNotDoneSQLException;

	public abstract void delete(T entity) throws SQLException, ActionNotDoneSQLException;

	public abstract void create(T entity) throws SQLException, ActionNotDoneSQLException;

	public abstract void update(T entity) throws SQLException, ActionNotDoneSQLException;
}
