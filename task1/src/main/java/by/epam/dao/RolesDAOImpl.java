package by.epam.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import by.epam.entities.Roles;
import by.epam.exceptions.ActionNotDoneSQLException;

public class RolesDAOImpl implements IDAO<Roles> {
	private final static String SQL_SELECT_ROLE_BY_ID = "SELECT ROLE_NAME FROM ROLES WHERE USER_ID = ?";
	private final static String SQL_DELETE_ROLE = "DELETE FROM ROLES WHERE USER_ID = ?";
	private final static String SQL_INSERT_ROLE = "INSERT INTO ROLES (USER_ID, ROLE_NAME) VALUES (?,?)";
	private final static String SQL_UPDATE_ROLE = "UPDATE ROLES SET ROLE_NAME = ? WHERE USER_ID = ?";
	DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Roles read(Long userId) throws SQLException, ActionNotDoneSQLException {
		Roles roles = new Roles();
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		ps = connection.prepareStatement(SQL_SELECT_ROLE_BY_ID);
		ps.setLong(1, userId);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			roles.setUserId(userId);
			roles.setRoleName(rs.getString("ROLE_NAME"));
		} else
			throw new ActionNotDoneSQLException("can't read the role");
		return roles;
	}

	public void delete(Roles role) throws SQLException, ActionNotDoneSQLException {
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		int count = 0;
		ps = connection.prepareStatement(SQL_DELETE_ROLE);
		ps.setLong(1, role.getUserId());
		count = ps.executeUpdate();
		if (count == 0) {
			throw new ActionNotDoneSQLException("can't delete the role");
		}
	}

	public void create(Roles role) throws SQLException, ActionNotDoneSQLException {
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		int count = 0;
		ps = connection.prepareStatement(SQL_INSERT_ROLE);
		ps.setLong(1, role.getUserId());
		ps.setString(2, role.getRoleName());
		count = ps.executeUpdate();
		if (count == 0) {
			throw new ActionNotDoneSQLException("can't create the role");
		}
	}

	public void update(Roles role) throws SQLException, ActionNotDoneSQLException {
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		int count = 0;
		ps = connection.prepareStatement(SQL_UPDATE_ROLE);
		ps.setString(1, role.getRoleName());
		ps.setLong(4, role.getUserId());
		count = ps.executeUpdate();
		if (count == 0) {
			throw new ActionNotDoneSQLException("can't update the role");
		}
	}

}
