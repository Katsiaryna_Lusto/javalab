package by.epam.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import by.epam.entities.Tag;
import by.epam.exceptions.ActionNotDoneSQLException;

public class TagDAOImpl implements IDAO<Tag> {
	private final static String SQL_INSERT_TAG = "INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (?,?)";
	private final static String SQL_DELETE_TAG = "DELETE FROM TAG WHERE TAG_ID = ?";
	private final static String SQL_UPDATE_TAG = "UPDATE TAG SET TAG_NAME = ? WHERE TAG_ID = ?";
	private final static String SQL_SELECT_TAG_BY_ID = "SELECT TAG_NAME FROM TAG WHERE NEWS_ID = ?";
	DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Tag read(Long tagId) throws SQLException, ActionNotDoneSQLException {
		Tag tag = new Tag();
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		ps = connection.prepareStatement(SQL_SELECT_TAG_BY_ID);
		ps.setLong(1, tagId);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			tag.setTagId(tagId);
			tag.setTagName(rs.getString("TAG_NAME"));
		} else
			throw new ActionNotDoneSQLException("can't read the tag");
		return tag;
	}

	public void delete(Tag tag) throws SQLException, ActionNotDoneSQLException {
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		int count = 0;
		ps = connection.prepareStatement(SQL_DELETE_TAG);
		ps.setLong(1, tag.getTagId());
		count = ps.executeUpdate();
		if (count == 0) {
			throw new ActionNotDoneSQLException("can't delete the tag");
		}
	}

	public void create(Tag tag) throws SQLException, ActionNotDoneSQLException {
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		int count = 0;
		ps = connection.prepareStatement(SQL_INSERT_TAG);
		ps.setLong(1, tag.getTagId());
		ps.setString(2, tag.getTagName());
		count = ps.executeUpdate();
		if (count == 0) {
			throw new ActionNotDoneSQLException("can't create the tag");
		}
	}

	public void update(Tag tag) throws SQLException, ActionNotDoneSQLException {
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		int count = 0;
		ps = connection.prepareStatement(SQL_UPDATE_TAG);
		ps.setString(1, tag.getTagName());
		ps.setLong(2, tag.getTagId());
		count = ps.executeUpdate();
		if (count == 0) {
			throw new ActionNotDoneSQLException("can't update the tag");
		}
	}

}
