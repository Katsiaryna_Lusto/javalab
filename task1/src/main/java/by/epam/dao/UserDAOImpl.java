package by.epam.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import by.epam.entities.User;
import by.epam.exceptions.ActionNotDoneSQLException;

public class UserDAOImpl implements IDAO<User> {
	private final static String SQL_SELECT_USER_BY_ID = "SELECT * FROM K_USER WHERE USER_ID = ?";
	private final static String SQL_DELETE_USER = "DELETE FROM K_USER WHERE USER_ID = ?";
	private final static String SQL_INSERT_USER = "INSERT INTO K_USER (USER_ID, USER_NAME, LOGIN, PASSWORD) VALUES (?,?,?,?)";
	private final static String SQL_UPDATE_USER = "UPDATE K_USER SET USER_NAME = ?, LOGIN = ?, PASSWORD = ? WHERE USER_ID = ?";
	DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public User read(Long userId) throws SQLException, ActionNotDoneSQLException {
		User user = new User();
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		ps = connection.prepareStatement(SQL_SELECT_USER_BY_ID);
		ps.setLong(1, userId);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			user.setUserId(userId);
			user.setUserName(rs.getString("USER_NAME"));
			user.setLogin(rs.getString("LOGIN"));
			user.setPassword(rs.getString("PASSWORD"));
		} else
			throw new ActionNotDoneSQLException("can't read the user");
		return user;
	}

	public void delete(User user) throws SQLException, ActionNotDoneSQLException {
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		int count = 0;
		ps = connection.prepareStatement(SQL_DELETE_USER);
		ps.setLong(1, user.getUserId());
		count = ps.executeUpdate();
		if (count == 0) {
			throw new ActionNotDoneSQLException("can't delete the tag");
		}
	}

	public void create(User user) throws SQLException, ActionNotDoneSQLException {
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		int count = 0;
		ps = connection.prepareStatement(SQL_INSERT_USER);
		ps.setLong(1, user.getUserId());
		ps.setString(2, user.getUserName());
		ps.setString(3, user.getLogin());
		ps.setString(4, user.getPassword());
		count = ps.executeUpdate();
		if (count == 0) {
			throw new ActionNotDoneSQLException("can't create the tag");
		}
	}

	public void update(User user) throws SQLException, ActionNotDoneSQLException {
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		int count = 0;
		ps = connection.prepareStatement(SQL_UPDATE_USER);
		ps.setString(1, user.getUserName());
		ps.setString(2, user.getLogin());
		ps.setString(3, user.getPassword());
		ps.setLong(4, user.getUserId());
		count = ps.executeUpdate();
		if (count == 0) {
			throw new ActionNotDoneSQLException("can't update the tag");
		}
	}

}
