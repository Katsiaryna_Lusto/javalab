package by.epam.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import javax.sql.DataSource;

import by.epam.entities.News;
import by.epam.exceptions.ActionNotDoneSQLException;

public class NewsDAOImpl implements IDAO<News> {

	private final static String SQL_INSERT_NEWS = "INSER INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (?,?,?,?,?,?)";
	private final static String SQL_UPDATE_NEWS = "UPDATE NEWS SET TITLE = ?, SHORT_TEXT = ?, FULL_TEXT = ?, CREATION_DATE = ?, MODIFICATION_DATE = ? WHERE NEWS_ID = ?";
	private final static String SQL_DELETE_NEWS = "DELETE FROM NEWS WHERE NEWS_ID = ?";
	private final static String SQL_SELECT_NEWS_BY_ID = "SELECT TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM NEWS WHERE NEWS_ID = ?";
	private final static String SQL_SELECT_ALL_NEWS = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM NEWS";
	private final static String SQL_COUNT_NEWS = "SELECT COUNT(*) FROM NEWS";
	private final static String SQL_INSERT_NEWS_AUTHOR = "INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (?,?)";
	private final static String SQL_INSERT_NEWS_TAG = "INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (?,?)";

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public News read(Long newsId) throws SQLException, ActionNotDoneSQLException {
		News news = new News();
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		ps = connection.prepareStatement(SQL_SELECT_NEWS_BY_ID);
		ps.setLong(1, newsId);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			news.setNewsId(newsId);
			news.setTitle(rs.getString("TITLE"));
			news.setShortText(rs.getString("SHORT_TEXT"));
			news.setFullText(rs.getString("FULL_TEXT"));
			news.setCreationDate(rs.getTimestamp("CREATION_DATE"));
			news.setModificationDate(rs.getDate("MODIFICATION_DATE"));
		} else
			throw new ActionNotDoneSQLException("can't read the news");
		return news;
	}

	public void delete(News news) throws SQLException, ActionNotDoneSQLException {
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		int count = 0;
		ps = connection.prepareStatement(SQL_DELETE_NEWS);
		ps.setLong(1, news.getNewsId());
		count = ps.executeUpdate();
		if (count == 0) {
			throw new ActionNotDoneSQLException("can't delete the news");
		}
	}

	public void create(News news) throws SQLException, ActionNotDoneSQLException {
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		int count = 0;
		ps = connection.prepareStatement(SQL_INSERT_NEWS);
		ps.setLong(1, news.getNewsId());
		ps.setString(2, news.getTitle());
		ps.setString(3, news.getShortText());
		ps.setString(4, news.getFullText());
		ps.setTimestamp(5, news.getCreationDate());
		ps.setDate(6, news.getModificationDate());
		count = ps.executeUpdate();
		if (count == 0)
			throw new ActionNotDoneSQLException("can't create the news");
	}

	public void update(News news) throws SQLException, ActionNotDoneSQLException {
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		int count = 0;
		ps = connection.prepareStatement(SQL_UPDATE_NEWS);
		ps.setString(1, news.getTitle());
		ps.setString(2, news.getShortText());
		ps.setString(3, news.getFullText());
		ps.setTimestamp(4, news.getCreationDate());
		ps.setDate(5, news.getModificationDate());
		ps.setLong(6, news.getNewsId());
		count = ps.executeUpdate();
		if (count == 0) {
			throw new ActionNotDoneSQLException("can't update the news");
		}
	}

	public List<News> viewNews() throws SQLException {
		List<News> newsList = new LinkedList<News>();
		Connection connection = dataSource.getConnection();
		Statement s = connection.createStatement();
		News news;
		ResultSet rs = s.executeQuery(SQL_SELECT_ALL_NEWS);
		while (rs.next()) {
			news = new News();
			news.setNewsId(rs.getLong("NEWS_ID"));
			news.setTitle(rs.getString("TITLE"));
			news.setShortText(rs.getString("SHORT_TEXT"));
			news.setFullText(rs.getString("FULL_TEXT"));
			news.setCreationDate(rs.getTimestamp("CREATION_DATE"));
			news.setModificationDate(rs.getDate("MODIFICATION_DATE"));
			newsList.add(news);
		}
		return newsList;

	}

	public int countNews() throws SQLException, ActionNotDoneSQLException {
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		int count = 0;
		ps = connection.prepareStatement(SQL_COUNT_NEWS);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			count = rs.getInt("NEWS_COUNT");
		} else
			throw new ActionNotDoneSQLException("can't count news");
		return count;
	}

	public void createNewsAuthor(Long newsId, Long authorId) throws SQLException, ActionNotDoneSQLException {
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		int count = 0;
		ps = connection.prepareStatement(SQL_INSERT_NEWS_AUTHOR);
		ps.setLong(1, newsId);
		ps.setLong(2, authorId);
		count = ps.executeUpdate();
		if (count == 0) {
			throw new ActionNotDoneSQLException("can't insert the news_author");
		}
	}

	public void createNewsTag(Long newsId, Long tagId) throws SQLException, ActionNotDoneSQLException {
		PreparedStatement ps = null;
		Connection connection = dataSource.getConnection();
		int count = 0;
		ps = connection.prepareStatement(SQL_INSERT_NEWS_TAG);
		ps.setLong(1, newsId);
		ps.setLong(2, tagId);
		count = ps.executeUpdate();
		if (count == 0) {
			throw new ActionNotDoneSQLException("can't insert the news_tag");
		}
	}

}
