package by.epam.exceptions;

public class ActionNotDoneSQLException extends Exception {

	private static final long serialVersionUID = 1L;

	public ActionNotDoneSQLException() {
	}

	public ActionNotDoneSQLException(String msg) {
		super(msg);
	}

}
