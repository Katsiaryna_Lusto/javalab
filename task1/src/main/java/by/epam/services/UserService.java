package by.epam.services;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import by.epam.dao.IDAO;
import by.epam.entities.User;
import by.epam.exceptions.ActionNotDoneSQLException;

public class UserService {
	IDAO<User> dao;

	private static final Logger logger = LoggerFactory.getLogger(UserService.class);

	public void setDao(IDAO<User> dao) {
		this.dao = dao;
	}

	public void create(Long userId, String userName, String login, String password) {
		User user = new User();
		user.setUserId(userId);
		user.setUserName(userName);
		user.setLogin(login);
		user.setPassword(password);
		try {
			dao.create(user);
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public void delete(Long userId) {
		try {
			dao.delete(dao.read(userId));
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public void update(Long userId, String userName, String login, String password) {
		User user = new User();
		user.setUserId(userId);
		user.setUserName(userName);
		user.setLogin(login);
		user.setPassword(password);
		try {
			dao.update(user);
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}
}
