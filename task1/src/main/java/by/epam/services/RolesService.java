package by.epam.services;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import by.epam.dao.IDAO;
import by.epam.entities.Roles;
import by.epam.exceptions.ActionNotDoneSQLException;

public class RolesService {
	IDAO<Roles> dao;
	private static final Logger logger = LoggerFactory.getLogger(RolesService.class);

	public void setDao(IDAO<Roles> dao) {
		this.dao = dao;
	}

	public void create(Long userId, String roleName) {
		Roles role = new Roles();
		role.setUserId(userId);
		role.setRoleName(roleName);
		try {
			dao.create(role);
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public void delete(Long userId) {
		try {
			dao.delete(dao.read(userId));
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public void update(Long userId, String roleName) {

		Roles role = new Roles();
		role.setUserId(userId);
		role.setRoleName(roleName);
		try {
			dao.update(role);
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}
}
