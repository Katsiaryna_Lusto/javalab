package by.epam.services;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import by.epam.dao.IDAO;
import by.epam.entities.Tag;
import by.epam.exceptions.ActionNotDoneSQLException;

public class TagService {
	IDAO<Tag> dao;

	private static final Logger logger = LoggerFactory.getLogger(TagService.class);

	public void setDao(IDAO<Tag> dao) {
		this.dao = dao;
	}

	public void delete(Long tagId) {
		try {
			dao.delete(dao.read(tagId));
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public void create(Long tagId, String tagName) {
		Tag tag = new Tag();
		tag.setTagId(tagId);
		tag.setTagName(tagName);
		try {
			dao.create(tag);
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public void update(Long tagId, String tagName) {
		Tag tag = new Tag();
		tag.setTagId(tagId);
		tag.setTagName(tagName);
		try {
			dao.update(tag);
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}
}
