package by.epam.services;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import by.epam.dao.CommentsDAOImpl;
import by.epam.dao.IDAO;
import by.epam.dao.NewsDAOImpl;
import by.epam.entities.News;
import by.epam.exceptions.ActionNotDoneSQLException;

public class NewsService {
	IDAO<News> dao;
	private static final Logger logger = LoggerFactory.getLogger(NewsService.class);

	public void setDao(IDAO<News> dao) {
		this.dao = dao;
	}

	public void create(Long newsId, String title, String shortText, String fullText, Timestamp creationDate,
			Date modificationDate) {
		News news = new News();
		news.setNewsId(newsId);
		news.setTitle(title);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setCreationDate(creationDate);
		news.setModificationDate(modificationDate);
		try {
			dao.create(news);
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public void update(Long newsId, String title, String shortText, String fullText, Timestamp creationDate,
			Date modificationDate) {
		News news = new News();
		news.setNewsId(newsId);
		news.setTitle(title);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setCreationDate(creationDate);
		news.setModificationDate(modificationDate);
		try {
			dao.update(news);
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public void delete(Long newsId) {
		try {
			dao.delete(dao.read(newsId));
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public List<News> viewAll() {
		List<News> newList = new LinkedList<News>();
		try {
			newList = ((NewsDAOImpl) dao).viewNews();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		return newList;
	}

	public News view(Long newsId) {
		News news = new News();
		NewsDAOImpl newsDAO = new NewsDAOImpl();
		try {
			news = newsDAO.read(newsId);
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		return news;
	}

	public List<News> viewAllNewsSortedByComments() {
		List<News> newsList = new LinkedList<News>();
		Comparator<News> comp = new Comparator<News>() {
			CommentsDAOImpl commentsDAO = new CommentsDAOImpl();
			int count = 0;

			public int compare(News news1, News news2) {
				try {
					count = commentsDAO.commentsCount(news2) - commentsDAO.commentsCount(news1);
				} catch (ActionNotDoneSQLException e) {
					logger.error(e.getMessage());
				} catch (SQLException e) {
					logger.error(e.getMessage());
				}
				return count;
			}
		};

		try {
			newsList = ((NewsDAOImpl) dao).viewNews();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		Collections.sort(newsList, comp);
		return newsList;
	}

	public void createNewsAuthor(Long newsId, Long authorId) {
		try {
			((NewsDAOImpl) dao).createNewsAuthor(newsId, authorId);
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public void createNewsTag(Long newsId, Long tagId) {
		try {
			((NewsDAOImpl) dao).createNewsTag(newsId, tagId);
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}
}
