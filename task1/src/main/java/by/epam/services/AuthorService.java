package by.epam.services;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import by.epam.dao.AuthorDAOImpl;
import by.epam.dao.IDAO;
import by.epam.entities.Author;
import by.epam.exceptions.ActionNotDoneSQLException;

public class AuthorService {
	IDAO<Author> dao;
	private static final Logger logger = LoggerFactory.getLogger(AuthorService.class);

	public void setDao(IDAO<Author> dao) {
		this.dao = dao;
	}

	public void create(Long authorId, String authorName) {
		Author author = new Author();
		author.setAuthorId(authorId);
		author.setAuthorName(authorName);
		try {
			dao.create(author);
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public void delete(Long authorId) {
		try {
			Author author = dao.read(authorId);
			author.setExpired((Timestamp) Calendar.getInstance().getTime());
			dao.delete(author);
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public void update(Long authorId, String authorName) {
		Author author = new Author();
		author.setAuthorId(authorId);
		author.setAuthorName(authorName);
		try {
			dao.update(author);
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public List<Author> viewAll() {
		List<Author> authorList = new LinkedList<Author>();
		try {
			authorList = ((AuthorDAOImpl) dao).viewAuthors();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		return authorList;
	}
}
