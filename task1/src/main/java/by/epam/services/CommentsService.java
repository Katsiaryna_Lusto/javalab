package by.epam.services;

import java.sql.SQLException;
import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import by.epam.dao.CommentsDAOImpl;
import by.epam.dao.IDAO;
import by.epam.entities.Comments;
import by.epam.exceptions.ActionNotDoneSQLException;

public class CommentsService {
	IDAO<Comments> dao;
	private static final Logger logger = LoggerFactory.getLogger(CommentsService.class);

	public void setDao(IDAO<Comments> dao) {
		this.dao = dao;
	}

	public void create(Long commentId, Long newsId, String commentText, Timestamp creationDate) {
		Comments comment = new Comments();
		comment.setCommentText(commentText);
		comment.setCreationDate(creationDate);
		comment.setCommentId(commentId);
		comment.setNewsId(newsId);
		try {
			dao.create(comment);
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	public void delete(Long commentId) {
		Comments comment;
		try {
			comment = ((CommentsDAOImpl) dao).read(commentId);
			dao.delete(comment);
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}
}
