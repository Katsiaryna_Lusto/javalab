package by.epam.dao;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;

import by.epam.entities.News;
import by.epam.exceptions.ActionNotDoneSQLException;

@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/dataSource.xml" })
public class NewsDAOImplTest {
	private static final Logger logger = LoggerFactory.getLogger(AuthorDAOImplTest.class);

	@Autowired
	private NewsDAOImpl newsDAO;

	@Test
	@DatabaseSetup("/news/dataset.xml")
	public void testRead() {

		News expected = new News(1L, "TITLE1", "SHORT1", "FULL1", Timestamp.valueOf("1997-12-01 10:00:00.00"),
				Date.valueOf("1999-05-11"));
		News actual;
		try {
			actual = newsDAO.read(1L);
			assertEquals(expected, actual);
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}

	}

	@Test
	@DatabaseSetup("/news/dataset.xml")
	@ExpectedDatabase("/news/expectedDataSetUpdate.xml")
	public void testUpdate() {
		try {
			newsDAO.update(new News(2L, "TITLE_2", "SHORT2", "FULL2", Timestamp.valueOf("2001-12-01 10:00:00.00"),
					Date.valueOf("2001-05-12")));
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	@Test

	@DatabaseSetup("/news/dataset.xml")

	@ExpectedDatabase("/news/expectedDataSetCreate.xml")
	public void testCreate() {
		try {
			newsDAO.create(new News(4L, "TITLE4", "SHORT4", "FULL4", Timestamp.valueOf("2003-12-01 10:00:00.00"),
					Date.valueOf("2008-05-05")));
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	@Test

	@DatabaseSetup("/news/dataset.xml")

	@ExpectedDatabase("/news/expectedDataSetDelete.xml")
	public void testDelete() {
		try {
			newsDAO.delete(new News(3L, "TITLE3", "SHORT3", "FULL3", Timestamp.valueOf("2002-12-01 10:00:00.00"),
					Date.valueOf("2005-05-03")));
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	@Test
	@DatabaseSetup("/news/dataset.xml")
	public void testViewNews() {
		// List<News> expected = new LinkedList<News>();
		// expected.add(new News(1L, "TITLE1", "SHORT1", "FULL1",
		// Timestamp.valueOf("1997-12-01 10:00:00.00"),
		// Date.valueOf("1999-05-11")));
		// expected.add(new News(2L, "TITLE2", "SHORT2", "FULL2",
		// Timestamp.valueOf("2001-12-01 10:00:00.00"),
		// Date.valueOf("2001-05-12")));
		// expected.add(new News(3L, "TITLE3", "SHORT3", "FULL3",
		// Timestamp.valueOf("2002-12-01 10:00:00.00"),
		// Date.valueOf("2005-05-03")));
		// List<News> actual;
		// try {
		// actual = newsDAO.viewNews();
		// assertEquals(expected, actual);
		// } catch (SQLException e) { // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
	}
}
