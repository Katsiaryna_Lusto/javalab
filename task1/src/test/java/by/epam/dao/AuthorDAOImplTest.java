package by.epam.dao;

import static org.junit.Assert.assertEquals;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;

import by.epam.entities.Author;
import by.epam.exceptions.ActionNotDoneSQLException;

@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/dataSource.xml" })
public class AuthorDAOImplTest {
	private static final Logger logger = LoggerFactory.getLogger(AuthorDAOImplTest.class);

	@Autowired
	private AuthorDAOImpl authorDAO;

	@Test
	@DatabaseSetup("/author/dataset.xml")
	public void testRead() {
		Author expected = new Author(1L, "Kate", null);
		Author actual;
		try {
			actual = authorDAO.read(1L);
			assertEquals(expected, actual);
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	@Test
	@DatabaseSetup("/author/dataset.xml")
	public void testViewAuthors() {
		List<Author> expected = new ArrayList<Author>();
		expected.add(new Author(1L, "Kate", null));
		expected.add(new Author(2L, "Nikita", null));
		expected.add(new Author(3L, "Ringo", null));
		List<Author> actual;
		try {
			actual = authorDAO.viewAuthors();
			assertEquals(expected, actual);
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	@Test
	@DatabaseSetup("/author/dataset.xml")
	@ExpectedDatabase("/author/expectedDataSetUpdate.xml")
	public void testUpdate() {
		try {
			authorDAO.update(new Author(1L, "Jane", null));
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	@Test
	@DatabaseSetup("/author/dataset.xml")
	@ExpectedDatabase("/author/expectedDataSetCreate.xml")
	public void testCreate() {
		try {
			authorDAO.create(new Author(4L, "Nadia", null));
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

	@Test
	@DatabaseSetup("/author/dataset.xml")
	@ExpectedDatabase("/author/expectedDataSetDelete.xml")
	public void testDelete() {
		try {
			authorDAO.delete(new Author(3L, "Ringo", null));
		} catch (ActionNotDoneSQLException e) {
			logger.error(e.getMessage());
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

}