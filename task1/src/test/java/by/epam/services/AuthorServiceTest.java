package by.epam.services;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.mockito.InjectMocks;

public class AuthorServiceTest {
	@InjectMocks
	private AuthorService authorService = mock(AuthorService.class);

	@Test
	public void testCreate() {
		authorService.create((long) anyInt(), anyString());
		verify(authorService).create((long) anyInt(), anyString());
	}

	@Test
	public void testDelete() {
		authorService.delete((long) anyInt());
		verify(authorService).delete((long) anyInt());
	}

	@Test
	public void testUpdate() {
		authorService.update((long) anyInt(), anyString());
		verify(authorService).update((long) anyInt(), anyString());
	}

	@Test
	public void testViewAll() {
		authorService.viewAll();
		verify(authorService).viewAll();
	}

}
