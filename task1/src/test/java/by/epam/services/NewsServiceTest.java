package by.epam.services;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.sql.Date;
import java.sql.Timestamp;

import org.junit.Test;
import org.mockito.InjectMocks;

public class NewsServiceTest {
	@InjectMocks
	private NewsService newsService = mock(NewsService.class);

	@Test
	public void testCreate() {
		newsService.create(1L, "TITLE1", "SHORT1", "FULL1", Timestamp.valueOf("1997-12-01 10:00:00.00"),
				Date.valueOf("1999-05-11"));
		verify(newsService).create(1L, "TITLE1", "SHORT1", "FULL1", Timestamp.valueOf("1997-12-01 10:00:00.00"),
				Date.valueOf("1999-05-11"));
	}

	@Test
	public void testUpdate() {
		newsService.update(1L, "TITLE1", "SHORT1", "FULL1", Timestamp.valueOf("1997-12-01 10:00:00.00"),
				Date.valueOf("1999-05-11"));
		verify(newsService).update(1L, "TITLE1", "SHORT1", "FULL1", Timestamp.valueOf("1997-12-01 10:00:00.00"),
				Date.valueOf("1999-05-11"));
	}

	@Test
	public void testDelete() {
		newsService.delete(1L);
		verify(newsService).delete(1L);
	}

	@Test
	public void testViewAll() {
		newsService.viewAll();
		verify(newsService).viewAll();
	}

	@Test
	public void testView() {
		newsService.view(1L);
		verify(newsService).view(1L);
	}

	@Test
	public void testViewAllNewsSortedByComments() {
		newsService.viewAllNewsSortedByComments();
		verify(newsService).viewAllNewsSortedByComments();
	}

	@Test
	public void testCreateNewsAuthor() {
		newsService.createNewsAuthor((long) anyInt(), (long) anyInt());
		verify(newsService).createNewsAuthor((long) anyInt(), (long) anyInt());
	}

	@Test
	public void testCreateNewsTag() {
		newsService.createNewsTag((long) anyInt(), (long) anyInt());
		;
		verify(newsService).createNewsTag((long) anyInt(), (long) anyInt());
	}

}
