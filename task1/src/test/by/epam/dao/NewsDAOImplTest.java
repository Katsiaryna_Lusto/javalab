package test.by.epam.dao;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.List;

import org.junit.Test;

import by.epam.dao.NewsDAOImpl;
import by.epam.entities.News;

public class NewsDAOImplTest {
	NewsDAOImpl newsDAO = mock(NewsDAOImpl.class);
	News news = mock(News.class);

	@Test
	public void testReadInt() throws SQLException {
		when(newsDAO.read((long) anyInt())).thenReturn(news);
	}

	@Test
	public void testDeleteNews() throws SQLException {
		when(newsDAO.delete(news)).thenReturn(true);
	}

	@Test
	public void testCreateNews() throws SQLException {
		when(newsDAO.create(news)).thenReturn(true);
	}

	@Test
	public void testUpdateNews() throws SQLException {
		when(newsDAO.update(news)).thenReturn(true);
	}

	@Test
	public void testViewNews() throws SQLException {
		List<News> list = mock(List.class);
		when(newsDAO.viewNews()).thenReturn(list);
	}

	@Test
	public void testCountNews() throws SQLException {
		when(newsDAO.countNews()).thenReturn(anyInt());
	}

}
