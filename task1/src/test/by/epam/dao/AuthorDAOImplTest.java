package test.by.epam.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.DatabaseMetaData;

import javax.sql.DataSource;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.ext.oracle.Oracle10DataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/dataSource.xml" })
public class AuthorDAOImplTest {
	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Before
	public void init() throws Exception {
		// insert data into db
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSet());
	}

	@After
	public void after() throws Exception {
		// insert data into db
		DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSet());
	}

	private IDatabaseConnection getConnection() throws Exception {
		// get connection
		Connection con = dataSource.getConnection();
		DatabaseMetaData databaseMetaData = con.getMetaData();
		// oracle schema name is the user name
		IDatabaseConnection connection = new DatabaseConnection(con, databaseMetaData.getUserName().toUpperCase());
		DatabaseConfig config = connection.getConfig();
		// oracle 10g
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new Oracle10DataTypeFactory());
		config.setFeature(DatabaseConfig.FEATURE_SKIP_ORACLE_RECYCLEBIN_TABLES, !Boolean.TRUE);
		return connection;
	}

	private IDataSet getDataSet() throws Exception {
		// get insert data
		File file = new File("test/by/epam/dao/config/dataset.xml");
		return new FlatXmlDataSet(file);
	}

	@Test
	public void testSQLUpdate() throws Exception {
		/*
		 * Connection con = authorDAOImpl.getDataSource().getConnection();
		 * Statement stmt = con.createStatement(); // get current data ResultSet
		 * rst = stmt.executeQuery("SELECT * FROM AUTHOR WHERE AUTHOR_ID = 1");
		 * if (rst.next()) {
		 * 
		 * // from dataset.xml assertEquals("Kate",
		 * rst.getString("AUTHOR_NAME")); rst.close(); // update via sql int
		 * count = stmt.executeUpdate(
		 * "UPDATE AUTHOR SET AUTHOR_NAME='Jane' WHERE AUTHOR_ID = 1");
		 * 
		 * stmt.close(); con.close();
		 * 
		 * // expect only one row to be updated assertEquals(
		 * "one row should be updated", 1, count);
		 * 
		 * // Fetch database data after executing the code QueryDataSet
		 * databaseSet = new QueryDataSet(getConnection()); // filter // data
		 * databaseSet.addTable("AUTHOR",
		 * "SELECT * FROM AUTHOR WHERE AUTHOR_ID = 1"); ITable actualTable =
		 * databaseSet.getTable("AUTHOR");
		 * 
		 * // Load expected data from an XML dataset File file = new
		 * File("test/by/epam/dao/config/expectedDataSet.xml"); IDataSet
		 * expectedDataSet = new FlatXmlDataSet(file); ITable expectedTable =
		 * databaseSet.getTables()[0];
		 * 
		 * // filter unnecessary columns of current data by xml definition
		 * actualTable = DefaultColumnFilter.includedColumnsTable(actualTable,
		 * expectedTable.getTableMetaData().getColumns());
		 * 
		 * // Assert actual database table match expected table
		 * assertEquals(expectedTable.getRowCount(), actualTable.getRowCount());
		 * assertEquals(expectedTable.getValue(0, "AUTHOR_NAME"),
		 * actualTable.getValue(0, "AUTHOR_NAME"));
		 * 
		 * } else { fail("no rows"); rst.close(); stmt.close(); con.close(); }
		 */
	}
}