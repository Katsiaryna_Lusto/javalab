<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<div class="login-menu">
	<sec:authorize access="!isAuthenticated()">
		<p>
			<a href="/news-admin/login"><fmt:message key="login.link.login" />
			</a>
		</p>
	</sec:authorize>
	<sec:authorize access="isAuthenticated()">
		<span><fmt:message key="login.message.hello" />
			<sec:authentication property="principal.username" /></span>

		<form action="logout" class="logout-button">
			<input type="submit" value="<fmt:message key="login.button.logout"/>" />
		</form>
	</sec:authorize>
</div>