<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<div class="admin-menu">
	<ul>
		<li><a href="/news-admin/news-filter"><fmt:message
					key="menu.link.news-list" /> </a></li>
		<li><a href="/news-admin/add-news-view"><fmt:message
					key="menu.link.add.news" /> </a></li>
		<li><a href="/news-admin/add-author-view"><fmt:message
					key="menu.link.add.author" /></a></li>
		<li><a href="/news-admin/add-tag-view"><fmt:message
					key="menu.link.add.tags" /> </a></li>
	</ul>
</div>
