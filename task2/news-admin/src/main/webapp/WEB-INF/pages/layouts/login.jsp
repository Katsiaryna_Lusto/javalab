<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="login-content">
	<form action="/news-admin/login" method="post">
		<h2 class="form-signin-heading">
			<fmt:message key="login.message.login" />
		</h2>
		<input type="text" name="username" required><br /> <input
			type="password" name="password" required><br />
		<button type="submit">
			<fmt:message key="login.button.login" />
		</button>
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />

		<c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
			<font color="red"> <br /> <c:out
					value="${SPRING_SECURITY_LAST_EXCEPTION.message}" />.
			</font>
		</c:if>

	</form>
</div>