<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<div id="content">
	<c:forEach var="author" items="${authorList}">
		<form id="updateForm${author.id}" action="/news-admin/update-author"
			method="post">
			<strong><fmt:message key="add.author.message.author" /> </strong> <input
				type="text" name="authorName" id="text${author.id}"
				value="${author.name}" size="30" disabled /> <a
				id="edit${author.id}" href="javascript:{}"
				onclick="switchEnableAuthorInput(${author.id})"><fmt:message
					key="link.edit" /></a> <a id="update${author.id}" href="javascript:{}"
				onclick="updateFormLink(${author.id})" class="hidden"><fmt:message
					key="link.update" /> </a> <a id="expire${author.id}"
				href="/news-admin/expire-author/${author.id}" class="hidden"><fmt:message
					key="link.expire" /></a> <a id="cancel${author.id}"
				href="/news-admin/add-author-view" class="hidden"><fmt:message
					key="link.cancel" /></a><br /> <input type="hidden" name="authorId"
				value="${author.id}" />
		</form>
	</c:forEach>
	<form action="/news-admin/add-author" method="post">
		<strong><fmt:message key="add.author.message.add.author" />
		</strong> <input type="text" required name="authorName" size="30" /> <input
			type="submit" value="<fmt:message key="button.add"/> ">
	</form>
</div>
