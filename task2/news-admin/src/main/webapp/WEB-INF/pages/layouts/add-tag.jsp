<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div id="content">
	<c:forEach var="tag" items="${tagList}">
		<form id="updateForm${tag.id}" action="/news-admin/update-tag"
			method="post">
			<strong><fmt:message key="add.tag.message.tag" /> </strong> <input
				type="text" name="tagName" id="text${tag.id}" value="${tag.name}"
				size="30" disabled /> <a id="edit${tag.id}" href="javascript:{}"
				onclick="switchEnableTagInput(${tag.id})"><fmt:message
					key="link.edit" /></a> <a id="update${tag.id}" href="javascript:{}"
				onclick="updateFormLink(${tag.id})" class="hidden"><fmt:message
					key="link.update" /> </a> <a id="delete${tag.id}"
				href="/news-admin/delete-tag/${tag.id}" class="hidden"><fmt:message
					key="link.delete" /></a> <a id="cancel${tag.id}"
				href="/news-admin/add-tag-view" class="hidden"><fmt:message
					key="link.cancel" /></a><br /> <input type="hidden" name="tagId"
				value="${tag.id}" />
		</form>
	</c:forEach>
	<form action="/news-admin/add-tag" name="addTag" method="post">
		<strong><fmt:message key="add.tag.message.add.tag" /> </strong> <input
			type="text" required name="tagName" size="30" /> <input
			type="submit" value="<fmt:message key="button.add"/> ">
	</form>
</div>
