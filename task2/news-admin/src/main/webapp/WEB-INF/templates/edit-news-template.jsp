<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<html>
<head>
<title><tiles:getAsString name="title" /></title>
<link href="<c:url value='/resources/css/style.css'/>" rel="stylesheet">
<script src="<c:url value="/resources/js/script.js" />"></script>
<script type="text/javascript"
	src="<c:url value='/resources/js/jquery-1.11.2.min.js'/>"></script>
<script type="text/javascript"
	src="<c:url value='/resources/js/jquery-ui-1.11.2.min.js'/>"></script>
<script type="text/javascript"
	src="<c:url value='/resources/js/ui.dropdownchecklist.js'/>"></script>
</head>
<body>
	<tiles:insertAttribute name="header" />
	<tiles:insertAttribute name="login-menu" />
	<tiles:insertAttribute name="locale" />
	<tiles:insertAttribute name="menu" />
	<tiles:insertAttribute name="content" />
	<tiles:insertAttribute name="footer" />
</body>
</html>
