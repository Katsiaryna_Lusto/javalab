package com.epam.news.controller;

import static com.epam.news.utils.RequestMappingNames.ADD_AUTHOR_VIEW;
import static com.epam.news.utils.RequestMappingNames.ADD_NEWS_VIEW;
import static com.epam.news.utils.RequestMappingNames.ADD_TAG_VIEW;
import static com.epam.news.utils.RequestMappingNames.EDIT_NEWS_LINK;
import static com.epam.news.utils.RequestMappingNames.EDIT_NEWS_VIEW;
import static com.epam.news.utils.RequestMappingNames.ERROR;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.entity.Author;
import com.epam.news.entity.News;
import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.IAuthorService;
import com.epam.news.service.INewsService;
import com.epam.news.service.ITagService;

/**
 * Class provides navigation routing
 */
@Controller
public class MenuController {
	@Autowired
	private ITagService tagService;
	@Autowired
	private IAuthorService authorService;
	@Autowired
	private INewsService newsService;

	@RequestMapping(value = ADD_NEWS_VIEW)
	public ModelAndView addNews(HttpSession session) throws ServiceException {
		List<Author> authorList = authorService.findAllAuthors();

		// remove expired authors
		removeExpiredAuthors(authorList);
		session.setAttribute("authorList", authorList);
		session.setAttribute("currentDate", new Date());
		return new ModelAndView(ADD_NEWS_VIEW);
	}

	@RequestMapping(value = ADD_AUTHOR_VIEW)
	public ModelAndView addAuthor(HttpSession session) throws ServiceException {
		List<Author> authorList = authorService.findAllAuthors();
		removeExpiredAuthors(authorList);
		session.setAttribute("authorList", authorList);
		return new ModelAndView(ADD_AUTHOR_VIEW);
	}

	@RequestMapping(value = ADD_TAG_VIEW)
	public ModelAndView addTags(HttpSession session) throws ServiceException {
		List<Tag> tags = tagService.findAllTags();
		session.setAttribute("tagList", tags);
		return new ModelAndView(ADD_TAG_VIEW);
	}

	@RequestMapping(value = EDIT_NEWS_LINK)
	public ModelAndView editNews(HttpSession session, @PathVariable Long newsId) throws ServiceException {

		News news = newsService.findById(newsId);
		Author author = authorService.findNewsAuthor(news.getId());// find
																	// author
																	// and tags
																	// for news
		List<Tag> tags = tagService.findTagsForNews(news.getId());
		List<Long> tagIds = new ArrayList<>();
		for (Tag tag : tags) {
			tagIds.add(tag.getId());
		}

		session.setAttribute("news", news);
		session.setAttribute("authorEditId", author.getId());
		session.setAttribute("tagEditIds", tagIds);

		return new ModelAndView(EDIT_NEWS_VIEW);
	}

	private void removeExpiredAuthors(List<Author> authorList) {
		// remove expired authors
		for (Iterator<Author> iterator = authorList.iterator(); iterator.hasNext();) {
			Author author = iterator.next();
			if (author.getExpired() != null) {
				iterator.remove();
			}
		}
	}

	@ExceptionHandler(ServiceException.class)
	public ModelAndView serviceExceptionHandler() {
		return new ModelAndView(ERROR);
	}
}
