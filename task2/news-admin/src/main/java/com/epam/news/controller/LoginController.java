package com.epam.news.controller;

import static com.epam.news.utils.RequestMappingNames.ACCESS_DENIED;
import static com.epam.news.utils.RequestMappingNames.LOGIN;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Class provides login action
 */
@Controller
public class LoginController {

	@RequestMapping(value = { LOGIN }, method = RequestMethod.GET)
	public ModelAndView loginPage() {
		return new ModelAndView(LOGIN);
	}

	@RequestMapping(value = { ACCESS_DENIED }, method = RequestMethod.GET)
	public ModelAndView accessDeniedPage() {
		return new ModelAndView(ACCESS_DENIED);
	}
}
