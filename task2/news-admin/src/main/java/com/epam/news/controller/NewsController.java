package com.epam.news.controller;

import static com.epam.news.utils.RequestMappingNames.ADD_NEWS;
import static com.epam.news.utils.RequestMappingNames.DELETE_NEWS;
import static com.epam.news.utils.RequestMappingNames.EDIT_NEWS;
import static com.epam.news.utils.RequestMappingNames.ERROR;
import static com.epam.news.utils.RequestMappingNames.NEWS_FILTER;
import static com.epam.news.utils.RequestMappingNames.NEWS_FILTER_VIEW;
import static com.epam.news.utils.RequestMappingNames.NEWS_PAGE_NUMBER_PARAM;
import static com.epam.news.utils.RequestMappingNames.NEWS_SINGLE_VIEW;
import static com.epam.news.utils.RequestMappingNames.REDIRECT;
import static com.epam.news.utils.RequestMappingNames.RESET_FILTER;
import static com.epam.news.utils.RequestMappingNames.SINGLE_NEWS;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.entity.Author;
import com.epam.news.entity.FilterVO;
import com.epam.news.entity.News;
import com.epam.news.entity.NewsVO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.IAuthorService;
import com.epam.news.service.INewsManageService;
import com.epam.news.service.INewsService;
import com.epam.news.service.ITagService;

/**
 * Class provides actions: - get news by filter ang page - reset filter - get
 * single news by id - add news - delete news - edit news
 */
@Controller
public class NewsController {
	@Autowired
	private ITagService tagService;
	@Autowired
	private IAuthorService authorService;
	@Autowired
	private INewsService newsService;
	@Autowired
	private INewsManageService newsManageService;

	private static final int NEWS_ON_PAGE = 3;

	/**
	 * Get news by filter and page
	 */
	@RequestMapping(value = NEWS_FILTER + NEWS_PAGE_NUMBER_PARAM)
	public ModelAndView newsFilter(HttpSession session, @RequestParam(value = "author", required = false) Long authorId,
			@RequestParam(value = "tags", required = false) List<Long> tagIds,
			@RequestParam(value = "fromFilter", required = false) Boolean fromFilter, @PathVariable Integer pageNumber)
					throws ServiceException {

		return newsFilterModel(session, authorId, tagIds, pageNumber, fromFilter);// general
																					// method
																					// for
																					// news
																					// by
																					// filter
	}

	/**
	 * Get news by filter, without page number parameter
	 */

	@RequestMapping(value = NEWS_FILTER)
	public ModelAndView newsFilter(HttpSession session, @RequestParam(value = "author", required = false) Long authorId,
			@RequestParam(value = "tags", required = false) List<Long> tagIds,
			@RequestParam(value = "fromFilter", required = false) Boolean fromFilter) throws ServiceException {
		return newsFilterModel(session, authorId, tagIds, 1, fromFilter);// general
																			// method
																			// for
																			// news
																			// by
																			// filter
	}

	/**
	 * Reset filter and return first page on news
	 */
	@RequestMapping(value = RESET_FILTER)
	public ModelAndView resetFilter(HttpSession session) throws ServiceException {
		session.setAttribute("authorId", null);
		session.setAttribute("tagIds", null);
		return newsFilterModel(session, null, null, 1, null);
	}

	/**
	 * Find single news by id
	 */
	@RequestMapping(value = SINGLE_NEWS)
	public ModelAndView singleNews(HttpSession session, @PathVariable Long newsId) throws ServiceException {
		FilterVO filterVO = getFilterFromSession(session, null, null);
		NewsVO newsVO = newsManageService.findNewsVO(newsId);
		int newsNumber = newsService.newsNumberFromFilter(newsId, filterVO);
		Long prevId = null;
		Long nextId = null;
		if (newsNumber > 1) {
			prevId = newsService.newsFromFilterByNumber(newsNumber - 1, filterVO).getId();
		}
		if (newsNumber < newsService.countNewsByFilter(filterVO)) {
			nextId = newsService.newsFromFilterByNumber(newsNumber + 1, filterVO).getId();
		}

		session.setAttribute("newsVO", newsVO);
		session.setAttribute("prevId", prevId);
		session.setAttribute("nextId", nextId);
		session.setAttribute("pageNumber", (newsNumber - 1) / NEWS_ON_PAGE + 1);
		return new ModelAndView(NEWS_SINGLE_VIEW);
	}

	/**
	 * Add news
	 */
	@RequestMapping(value = ADD_NEWS)
	public ModelAndView addNews(HttpSession session, @RequestParam(value = "title", required = true) String title,
			@RequestParam(value = "shortText", required = true) String shortText,
			@RequestParam(value = "fullText", required = true) String fullText,
			@RequestParam(value = "author", required = true) Long authorId,
			@RequestParam(value = "tags", required = false) List<Long> tagIds,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(value = "creationDate", required = true) Date creationDate)
					throws ServiceException {
		List<Tag> tags = null;
		if (tagIds != null) {// find tags by tagIds
			tags = new ArrayList<>();
			for (Long id : tagIds) {
				tags.add(tagService.findTag(id));
			}
		}
		Author author = authorService.findAuthor(authorId);

		News news = new News(0, title, shortText, fullText, creationDate, creationDate);
		NewsVO newsVO = new NewsVO(news, tags, null, author);
		newsManageService.add(newsVO);

		session.setAttribute("authorId", null);// to avoid errors with old
												// filter
		session.setAttribute("tagIds", null);
		session.setAttribute("newsVO", newsVO);
		session.setAttribute("prevNews", null);
		session.setAttribute("nextNews", null);
		session.setAttribute("pageNumber", 1);
		return new ModelAndView(REDIRECT + "single-news/" + news.getId());
	}

	/**
	 * Delete news by ids
	 */
	@RequestMapping(DELETE_NEWS)
	public ModelAndView deleteNews(HttpSession session,
			@RequestParam(value = "newsToDelete", required = false) List<Long> newsToDelete) throws ServiceException {
		prepareView(session);
		for (Long id : newsToDelete) {
			newsManageService.delete(id);
		}
		return new ModelAndView(REDIRECT + NEWS_FILTER);
	}

	/**
	 * Edit news
	 */
	@RequestMapping(EDIT_NEWS)
	public ModelAndView editNews(@RequestParam(value = "newsId", required = true) Long newsId,
			@RequestParam(value = "title", required = true) String title,
			@RequestParam(value = "shortText", required = true) String shortText,
			@RequestParam(value = "fullText", required = true) String fullText,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(value = "creationDate", required = true) Date creationDate,
			@RequestParam(value = "authorId", required = true) Long authorId,
			@RequestParam(value = "tags", required = false) List<Long> tagIds,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(value = "modificationDate", required = true) Date modificationDate)
					throws ServiceException {
		News news = new News(newsId, title, shortText, fullText, creationDate, modificationDate);
		List<Tag> tags = null;
		if (tagIds != null) {
			tags = new ArrayList<>();
			for (Long id : tagIds) {
				tags.add(tagService.findTag(id));
			}
		}

		newsManageService.editNewsVO(news, authorId, tags);

		return new ModelAndView(REDIRECT + "single-news/" + newsId);
	}

	/**
	 * Exception handler
	 */
	@ExceptionHandler(ServiceException.class)
	public ModelAndView serviceExceptionHandler() {
		return new ModelAndView(ERROR);
	}

	/**
	 * Set attributes for view
	 */
	private void prepareView(HttpSession session) throws ServiceException {
		List<Author> authorList = authorService.findAllAuthors();
		List<Tag> tagList = tagService.findAllTags();
		session.setAttribute("tagList", tagList);
		session.setAttribute("authorList", authorList);
	}

	/**
	 * General method for newsFilter
	 */
	private ModelAndView newsFilterModel(HttpSession session, Long authorId, List<Long> tagIds, Integer pageNumber,
			Boolean fromFilter) throws ServiceException {
		if (fromFilter != null) {// if request came not from filter button
			session.setAttribute("authorId", authorId);
			session.setAttribute("tagIds", tagIds);
		}
		if (fromFilter != null && tagIds == null) {// if we have 0 tags from
													// filter
			session.setAttribute("tagIds", null);
		}
		prepareView(session);
		FilterVO filterVO = getFilterFromSession(session, authorId, tagIds);

		List<News> newsList;
		int newsCount = newsService.countNewsByFilter(filterVO);
		if (pageNumber == null) {// if use filter
			newsList = newsService.findNewsByFilters(NEWS_ON_PAGE, pageNumber, filterVO);
		} else {// if change page
			newsList = newsService.findNewsByFilters(NEWS_ON_PAGE, pageNumber, filterVO);
		}
		List<NewsVO> newsVOList = newsManageService.buildNewsVOList(newsList);

		session.setAttribute("pageCount", newsCount / NEWS_ON_PAGE + (newsCount % NEWS_ON_PAGE == 0 ? 0 : 1));
		if (pageNumber == null) {
			session.setAttribute("pageNumber", 1);
		} else {
			session.setAttribute("pageNumber", pageNumber);
		}
		session.setAttribute("newsOnPage", NEWS_ON_PAGE);
		session.setAttribute("newsVOList", newsVOList);

		return new ModelAndView(NEWS_FILTER_VIEW);
	}

	/**
	 * Build filterVO by author and tags ids
	 */
	private FilterVO getFilterFromSession(HttpSession session, Long authorId, List<Long> tagIds)
			throws ServiceException {
		if (tagIds == null)
			tagIds = (List<Long>) session.getAttribute("tagIds");
		if (authorId == null)
			authorId = (Long) session.getAttribute("authorId");
		List<Tag> tags = null;
		if (tagIds != null) {
			tags = new ArrayList<>();
			for (Long id : tagIds) {
				tags.add(tagService.findTag(id));
			}
		}
		Author author = authorId == null ? null : authorService.findAuthor(authorId);
		return new FilterVO(tags, author);
	}
}