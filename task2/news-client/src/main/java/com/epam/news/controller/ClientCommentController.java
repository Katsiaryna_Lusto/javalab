package com.epam.news.controller;

import com.epam.news.entity.Comment;
import com.epam.news.entity.NewsVO;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.ICommentService;
import com.epam.news.service.INewsManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.Date;
import static com.epam.news.utils.ClientRequestMappingNames.*;

/**
 * Class provides actions: - add comment
 */
@Controller
public class ClientCommentController {
	@Autowired
	ICommentService commentService;
	@Autowired
	INewsManageService newsManageService;

	@RequestMapping(value = ADD_COMMENT)
	public ModelAndView newsFilter(HttpSession session, @RequestParam(value = "commentText", required = false) String commentText, @RequestParam(value = "newsId", required = false) Long newsId) throws ServiceException {

		Comment comment = new Comment(0, commentText, new Date(), newsId);
		commentService.addComment(comment);
		NewsVO newsVO = newsManageService.findNewsVO(newsId);
		session.setAttribute("newsVO", newsVO);
		return new ModelAndView(NEWS_SINGLE_VIEW);
	}

	@ExceptionHandler(ServiceException.class)
	public ModelAndView serviceExceptionHandler() {
		return new ModelAndView(ERROR);
	}
}
