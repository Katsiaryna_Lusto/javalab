<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<div id="content">
	<a href="/news-client/news-filter/${pageNumber}"><fmt:message
			key="link.back" /></a><br />
	<div class="title-author-date">
		<div class="news-title">${newsVO.news.title}</div>
		<div class="news-author-date">
			(
			<fmt:message key="main.message.by" />
			${newsVO.author.name}) <span class="news-modification-date"><ctg:dateFormat
					date="${newsVO.news.modificationDate}" /></span>
		</div>
		<div class="news-full-text">${newsVO.news.fullText}</div>
		<div class="comments">
			<c:forEach var="comment" items="${newsVO.comments}">
				<div>
					<span class="comment-date"> <ctg:dateFormat
							date="${comment.creationDate}" /></span>
					<div class="comment-text">${comment.commentText}</div>
				</div>
			</c:forEach>
			<form action="/news-client/add-comment" method="post">
				<textarea name="commentText" cols="40" rows="6"></textarea>
				<br /> <input type="hidden" name="newsId" value="${newsVO.news.id}" />
				<input type="submit"
					value="<fmt:message key="single.button.add.comment" />" />
			</form>
		</div>
		<div>
			<c:if test="${prevId != null}">
				<a href="/news-client/single-news/${prevId}"><fmt:message
						key="single.link.previous" /> </a>
			</c:if>
			<c:if test="${nextId != null}">
				<a href="/news-client/single-news/${nextId}"
					class="single-next-link"><fmt:message key="single.link.next" />
				</a>
			</c:if>
		</div>
	</div>