package com.epam.news.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.dao.ICommentDAO;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.ICommentService;

/**
 * Class that provides actions with comments
 *
 */

@Transactional(rollbackFor = { ServiceException.class, RuntimeException.class })
public class CommentServiceImpl implements ICommentService {

	private static Logger logger = Logger.getLogger(CommentServiceImpl.class);

	private ICommentDAO commentDAO;

	public void setCommentDAO(ICommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	/**
	 * Adds new comment and set inserted id into comment
	 * 
	 * @param comment
	 *            to be added
	 */
	@Override
	public void addComment(Comment comment) throws ServiceException {
		try {
			comment.setId(commentDAO.add(comment));
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}

	}

	/**
	 * Deletes given comment by comment id
	 * 
	 * @param id
	 *            of comment
	 */
	@Override
	public void deleteComment(Long id) throws ServiceException {
		try {
			commentDAO.delete(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}

	}

	/**
	 * Updates given comment by comment id
	 * 
	 * @param comment
	 *            to be updated
	 * @throws ServiceException
	 */
	@Override
	public void updateComment(Comment comment) throws ServiceException {
		try {
			commentDAO.update(comment);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	/**
	 * Finds comment by id
	 * 
	 * @param id
	 * @return Comment or null
	 * @throws ServiceException
	 */
	@Override
	public Comment findComment(Long id) throws ServiceException {
		Comment comment = null;
		try {
			comment = commentDAO.findById(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return comment;
	}

	/**
	 * Deletes all comments for news with given id
	 * 
	 * @param id
	 *            of news
	 * @return true if delete is successful, returns false otherwise
	 * @throws ServiceException
	 */
	@Override
	public void deleteCommentsForNews(Long id) throws ServiceException {
		try {
			commentDAO.deleteCommentsForNews(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	/**
	 * Finds all comments for news with given id
	 * 
	 * @param id
	 *            of news
	 * @return List of comments
	 * @throws ServiceException
	 */
	@Override
	public List<Comment> findCommentsForNews(Long id) throws ServiceException {
		List<Comment> result = null;
		try {
			result = commentDAO.findCommentsForNews(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return result;
	}

}
