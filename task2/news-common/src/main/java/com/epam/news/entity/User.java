package com.epam.news.entity;

import java.io.Serializable;

/**
 * Class to store user info
 *
 */
public class User implements Serializable {

	private static final long serialVersionUID = -3641995940725268144L;
	/**
	 * User id
	 */
	private Long id;
	/**
	 * User name
	 */
	private String name;
	/**
	 * User login
	 */
	private String login;
	/**
	 * User password
	 */
	private String password;

	public User(Long id, String name, String login, String password) {
		this.id = id;
		this.name = name;
		this.login = login;
		this.password = password;
	}

	public User() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		User user = (User) o;

		if (id != null ? !id.equals(user.id) : user.id != null)
			return false;
		if (login != null ? !login.equals(user.login) : user.login != null)
			return false;
		if (name != null ? !name.equals(user.name) : user.name != null)
			return false;
		if (password != null ? !password.equals(user.password) : user.password != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (login != null ? login.hashCode() : 0);
		result = 31 * result + (password != null ? password.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "User{" + "id=" + id + ", name='" + name + '\'' + ", login='" + login + '\'' + ", password='" + password + '}';
	}
}
