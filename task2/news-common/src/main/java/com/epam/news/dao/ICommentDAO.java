package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;

/**
 * Interface that provides operations with Comments
 *
 */
public interface ICommentDAO extends IGenericDAO<Comment, Long> {
	/**
	 * Finds all comments
	 * 
	 * @return List of comments
	 * @throws DAOException
	 */
	List<Comment> findAll() throws DAOException;

	/**
	 * Deletes all comments for news with given id
	 * 
	 * @param id
	 *            of news
	 * @return true if delete is successful, returns false otherwise
	 * @throws DAOException
	 */
	boolean deleteCommentsForNews(Long id) throws DAOException;

	/**
	 * Finds all comments for news with given id
	 * 
	 * @param id
	 *            of news
	 * @return List of comments
	 * @throws DAOException
	 */
	List<Comment> findCommentsForNews(Long id) throws DAOException;

}
