package com.epam.news.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.epam.news.entity.Comment;
import com.epam.news.entity.News;
import com.epam.news.entity.NewsVO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.IAuthorService;
import com.epam.news.service.ICommentService;
import com.epam.news.service.INewsManageService;
import com.epam.news.service.INewsService;
import com.epam.news.service.ITagService;

/**
 * Class that provides complex actions with news
 *
 */

@Transactional(rollbackFor = { ServiceException.class, RuntimeException.class })
public class NewsManageServiceImpl implements INewsManageService {

	private INewsService newsService;
	private ITagService tagService;
	private ICommentService commentService;
	private IAuthorService authorService;

	public void setNewsService(INewsService newsService) {
		this.newsService = newsService;
	}

	public void setTagService(ITagService tagService) {
		this.tagService = tagService;
	}

	public void setCommentService(ICommentService commentService) {
		this.commentService = commentService;
	}

	public void setAuthorService(IAuthorService authorService) {
		this.authorService = authorService;
	}

	/**
	 * Adds news with news info, author info, tags and comments
	 * 
	 * @param newsVO
	 *            to add
	 * @throws Exception
	 */
	@Override
	public void add(NewsVO newsVO) throws ServiceException {
		newsService.addNews(newsVO.getNews());// create news

		if (newsVO.getComments() != null) {// add comments if exist
			for (Comment comment : newsVO.getComments()) {
				commentService.addComment(comment);
			}
		}
		newsService.insertNewsAuthor(newsVO.getNews().getId(), newsVO.getAuthor().getId());// insert
																							// news~author
																							// info
		if (newsVO.getTags() != null) {
			newsService.insertNewsTags(newsVO.getNews().getId(), newsVO.getTags());// insert
																					// news~tags
																					// //
																					// info
		}

	}

	/**
	 * Deletes news according news id and all information about
	 * it(tags,comments)
	 * 
	 * @param id
	 *            of newsVO
	 * @throws ServiceException
	 */
	@Override

	public void delete(Long id) throws ServiceException {
		newsService.deleteNewsAuthor(id); // delete news author
		newsService.deleteNewsTags(id); // delete news tags

		commentService.deleteCommentsForNews(id); // delete comments for news
		newsService.deleteNews(id);// delete news
	}

	/**
	 * Collects all the data about the news with the given id
	 * 
	 * @param id
	 *            of news
	 * @return NewsVO(news,author,tags,comments) object with given id
	 * @throws ServiceException
	 */
	@Override
	public NewsVO findNewsVO(Long id) throws ServiceException {
		NewsVO result = new NewsVO();
		result.setNews(newsService.findById(id));
		result.setAuthor(authorService.findNewsAuthor(id));
		result.setComments(commentService.findCommentsForNews(id));
		result.setTags(tagService.findTagsForNews(id));
		return result;
	}

	/**
	 * Build NewsVO list by News list
	 * 
	 * @param newsList
	 * @return NewsVO list
	 * @throws ServiceException
	 */
	@Override
	public List<NewsVO> buildNewsVOList(List<News> newsList) throws ServiceException {
		List<NewsVO> resultList = null;
		if (newsList != null) {
			resultList = new ArrayList<>();

			for (News news : newsList) {
				NewsVO newsVO = findNewsVO(news.getId());
				resultList.add(newsVO);
			}
		}
		return resultList;
	}

	/**
	 * Edits newsVO(news, news author, news tags)
	 * 
	 * @param news
	 * @param authorId
	 * @param tags
	 * @throws ServiceException
	 */
	@Override
	public void editNewsVO(News news, Long authorId, List<Tag> tags) throws ServiceException {
		newsService.updateNews(news);
		newsService.deleteNewsAuthor(news.getId());
		newsService.insertNewsAuthor(news.getId(), authorId);
		newsService.deleteNewsTags(news.getId());
		newsService.insertNewsTags(news.getId(), tags);
	}

}
