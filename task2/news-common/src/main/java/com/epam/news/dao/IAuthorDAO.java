package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;

/**
 * Interface that provides operations with Author
 *
 */
public interface IAuthorDAO extends IGenericDAO<Author, Long> {
	/**
	 * Finds all authors
	 * 
	 * @return List of authors
	 * @throws DAOException
	 */
	List<Author> findAll() throws DAOException;

	/**
	 * Finds author by id
	 * 
	 * @param id
	 * @return author if find is successful, returns null otherwise
	 * @throws DAOException
	 */
	Author findById(Long id) throws DAOException;

	/**
	 * Finds news author
	 * 
	 * @param id
	 *            of news
	 * @return
	 * @throws DAOException
	 */
	Author findNewsAuthor(Long id) throws DAOException;
}
