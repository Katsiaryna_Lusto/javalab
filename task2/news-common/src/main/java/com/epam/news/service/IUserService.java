package com.epam.news.service;

import com.epam.news.dao.IUserDAO;
import com.epam.news.entity.User;
import com.epam.news.exception.ServiceException;

/**
 * Interface that provides orepations with User
 */
public interface IUserService {
	/**
	 * Finds user by login
	 * 
	 * @param login
	 * @return
	 * @throws ServiceException
	 */
	User findUser(String login) throws ServiceException;

	/**
	 * Finds user role by id
	 * 
	 * @param id
	 * @return
	 * @throws ServiceException
	 */
	String findUserRole(Long id) throws ServiceException;

	public void setUserDAO(IUserDAO userDAO);
}
