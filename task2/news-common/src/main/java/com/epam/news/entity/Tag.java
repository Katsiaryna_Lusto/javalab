package com.epam.news.entity;

import java.io.Serializable;
/**
 * Class to store tag info
 *
 */
public class Tag implements Serializable{
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -5494264259688759151L;
    /**
     * Tag id
     */
    private long id;
    /**
     * Tag name
     */
    private String name;

    public Tag() {}

    public Tag(long id, String name) {
        super();
        this.id = id;
        this.name = name;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("Tag [id=");
        str.append(id);
        str.append(", name=");
        str.append(name);
        str.append("]");
        return str.toString();
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Tag other = (Tag) obj;
        if (id != other.id)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }
}
