package com.epam.news.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;

/**
 * Interface that provides operations with Tags
 *
 */
@Transactional
public interface ITagDAO extends IGenericDAO<Tag, Long> {
	/**
	 * Finds all tags
	 * 
	 * @return List of tags
	 * @throws DAOException
	 */
	List<Tag> findAll() throws DAOException;

	/**
	 * Finds all tags for news with given id
	 * 
	 * @param id
	 *            of news
	 * @return list of tags
	 * @throws DAOException
	 */
	List<Tag> findTagsForNews(Long id) throws DAOException;

	/**
	 * Deletes tag with given id from all news
	 * 
	 * @param id
	 *            of tag
	 * @throws DAOException
	 */
	void deleteTagFromNews(Long id) throws DAOException;

}