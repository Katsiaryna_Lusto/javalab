package com.epam.news.util;

public class ColumnNames {
	private ColumnNames() {
	};

	public static final String AUTHOR_ID = "AUTHOR_ID";
	public static final String AUTHOR_NAME = "AUTHOR_NAME";
	public static final String EXPIRED = "EXPIRED";

	public static final String COMMENT_ID = "COMMENT_ID";
	public static final String COMMENT_TEXT = "COMMENT_TEXT";
	public static final String CREATION_DATE = "CREATION_DATE";
	public static final String COMMENT_NEWS_ID = "NEWS_ID";

	public static final String NEWS_ID = "newsId";
	public static final String NEWS_SHORT_TEXT = "newsShortText";
	public static final String NEWS_FULL_TEXT = "newsFullText";
	public static final String NEWS_TITLE = "newsTitle";
	public static final String NEWS_CREATION_DATE = "newsCreationDate";
	public static final String NEWS_MODIFICATION_DATE = "newsModificationDate";

	public static final String TAG_ID = "TAG_ID";
	public static final String TAG_NAME = "TAG_NAME";

	public static final String USER_ID = "USER_ID";
	public static final String USER_NAME = "USER_NAME";
	public static final String LOGIN = "LOGIN";
	public static final String PASSWORD = "PASSWORD";

	public static final String ROLE_NAME = "ROLE_NAME";
}
