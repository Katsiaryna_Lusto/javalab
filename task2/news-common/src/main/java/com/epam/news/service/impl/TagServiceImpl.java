package com.epam.news.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.dao.ITagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.ITagService;

/**
 * Class that provides actions with tags
 *
 */

@Transactional(rollbackFor = { ServiceException.class, RuntimeException.class })
public class TagServiceImpl implements ITagService {

	static private Logger logger = Logger.getLogger(TagServiceImpl.class);
	private ITagDAO tagDAO;

	public void setTagDAO(ITagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}

	/**
	 * Adds new tag and set inserted id into tag
	 * 
	 * @param tag
	 *            to be added
	 */
	@Override
	public void addTag(Tag tag) throws ServiceException {
		try {
			tag.setId(tagDAO.add(tag));
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}

	}

	/**
	 * Deletes tag with given id from all news
	 * 
	 * @param id
	 *            of tag
	 * @throws ServiceException
	 */
	@Override
	public void deleteTagFromNews(Long id) throws ServiceException {
		try {
			tagDAO.deleteTagFromNews(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	/**
	 * Deletes given tag by comment id
	 * 
	 * @param id
	 *            of tag
	 */
	@Override
	public void deleteTag(Long id) throws ServiceException {
		try {
			tagDAO.delete(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}

	}

	/**
	 * Updates given tag by comment id
	 * 
	 * @param tag
	 *            to be updated
	 * @throws ServiceException
	 */
	@Override
	public void updateTag(Tag tag) throws ServiceException {
		try {
			tagDAO.update(tag);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}

	}

	/**
	 * Finds tag by id
	 * 
	 * @param id
	 * @return tag or null
	 * @throws ServiceException
	 */
	@Override
	public Tag findTag(Long id) throws ServiceException {
		Tag tag = null;
		try {
			tag = tagDAO.findById(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return tag;
	}

	/**
	 * Finds all tags for news with given id
	 * 
	 * @param id
	 *            of news
	 * @return list of tags
	 * @throws ServiceException
	 */
	@Override
	public List<Tag> findTagsForNews(Long id) throws ServiceException {
		List<Tag> result = null;
		try {
			result = tagDAO.findTagsForNews(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return result;
	}

	/**
	 * Finds all tags
	 * 
	 * @return list of tags
	 * @throws ServiceException
	 */
	@Override
	public List<Tag> findAllTags() throws ServiceException {
		List<Tag> result = null;
		try {
			result = tagDAO.findAll();
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return result;
	}

}
