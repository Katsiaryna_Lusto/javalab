package com.epam.news.entity;

import java.io.Serializable;
import java.util.Date;
/**
 * CLass to store author info
 *
 */
public class Author implements Serializable{
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 7471930922830752352L;

    /**
     * Author id
     */
    private Long id;
    /**
     * Author name
     */
    private String name;
    /**
     * if author expired or not (null if not)
     */
    private Date expired;

    public Author() {}
    public Author(Long id, String name, Date expired) {
        super();
        this.id = id;
        this.name = name;
        this.expired = expired;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Date getExpired() {
        return expired;
    }
    public void setExpired(Date expired) {
        this.expired = expired;
    }
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("Author [id=");
        str.append(id);
        str.append(", name=" );
        str.append(name);
        str.append(", expired=");
        str.append(expired);
        str.append("]");
        return str.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Author author = (Author) o;

        if (expired != null ? !expired.equals(author.expired) : author.expired != null) return false;
        if (id != null ? !id.equals(author.id) : author.id != null) return false;
        if (name != null ? !name.equals(author.name) : author.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (expired != null ? expired.hashCode() : 0);
        return result;
    }
}
