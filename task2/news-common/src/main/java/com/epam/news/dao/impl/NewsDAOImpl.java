package com.epam.news.dao.impl;

import static com.epam.news.util.ColumnNames.NEWS_CREATION_DATE;
import static com.epam.news.util.ColumnNames.NEWS_FULL_TEXT;
import static com.epam.news.util.ColumnNames.NEWS_ID;
import static com.epam.news.util.ColumnNames.NEWS_MODIFICATION_DATE;
import static com.epam.news.util.ColumnNames.NEWS_SHORT_TEXT;
import static com.epam.news.util.ColumnNames.NEWS_TITLE;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.news.dao.INewsDAO;
import com.epam.news.entity.Author;
import com.epam.news.entity.FilterVO;
import com.epam.news.entity.News;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;


/**
 * Class that provides C.R.U.D. operations with News and related information
 * using JDBC
 *
 */

public class NewsDAOImpl implements INewsDAO {

	private DataSource dataSource;

	private final static String FIND_BY_ID = "SELECT NEWS.NEWS_ID newsId," + "NEWS.TITLE newsTitle,"
			+ "NEWS.SHORT_TEXT newsShortText," + "NEWS.FULL_TEXT newsFullText," + "NEWS.CREATION_DATE newsCreationDate,"
			+ "NEWS.MODIFICATION_DATE newsModificationDate " + "FROM News WHERE NEWS_ID=?";
	private final static String IS_TAG_FOR_NEWS = "SELECT TAG_ID FROM NEWS_TAG WHERE NEWS_ID=? AND TAG_ID=?";
	private static final String DELETE_BY_ID = "DELETE FROM NEWS WHERE NEWS_ID=?";
	private static final String INSERT_BY_ENTITY = "INSERT INTO NEWS(NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) VALUES (NEWS_SEQ.nextVal,?,?,?,?,?)";
	private static final String INSERT_NEWS_AUTHOR = "INSERT INTO NEWS_AUTHOR(NEWS_ID,AUTHOR_ID) VALUES(?,?)";
	private static final String INSERT_NEWS_TAG = "INSERT INTO NEWS_TAG(NEWS_ID,TAG_ID) VALUES(?,?)";
	private static final String DELETE_NEWS_TAGS = "DELETE FROM NEWS_TAG WHERE NEWS_ID=?";
	private static final String DELETE_NEWS_AUTHOR = "DELETE FROM NEWS_AUTHOR WHERE NEWS_ID=?";
	private static final String UPDATE_BY_ID = "UPDATE NEWS SET TITLE=?, SHORT_TEXT=?,FULL_TEXT=?"
			+ ",CREATION_DATE=?,MODIFICATION_DATE=? WHERE NEWS_ID=?";
	private final static String FIND_NEWS_BY_AUTHOR = "SELECT NEWS.NEWS_ID newsId," + "NEWS.TITLE newsTitle,"
			+ "NEWS.SHORT_TEXT newsShortText," + "NEWS.FULL_TEXT newsFullText," + "NEWS.CREATION_DATE newsCreationDate,"
			+ "NEWS.MODIFICATION_DATE newsModificationDate " + "FROM NEWS "
			+ "JOIN NEWS_AUTHOR ON NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID " + "WHERE NEWS_AUTHOR.AUTHOR_ID=?";

	/**
	 * Finds news by id
	 * 
	 * @param id
	 *            of news
	 * @return NewsEntity if method is successful, returns null otherwise
	 * @throws DAOException
	 */
	@Override
	public News findById(Long id) throws DAOException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection connection = null;
		News result = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			ps = connection.prepareStatement(FIND_BY_ID);
			ps.setLong(1, id);
			rs = ps.executeQuery();

			if (rs.next()) {
				result = buildNews(rs);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return result;
	}

	/**
	 * Deletes news by id
	 * 
	 * @param id
	 *            of news
	 * @return true if delete is successful, returns false otherwise
	 * @throws DAOException
	 */
	@Override
	public boolean delete(Long id) throws DAOException {
		PreparedStatement ps = null;
		Connection connection = null;
		int deleteFlag = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			ps = connection.prepareStatement(DELETE_BY_ID);
			ps.setLong(1, id);
			deleteFlag = ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return deleteFlag == 1;
	}

	/**
	 * Creates news with given info
	 * 
	 * @param entity
	 * @return id of inserted comment, 0 if insert was unsuccessful
	 * @throws DAOException
	 */
	@Override
	public Long add(News entity) throws DAOException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection connection = null;
		Long insertId = 0L;

		if (entity == null)
			return -1L;
		try {
			String[] id = { "NEWS_ID" };
			connection = DataSourceUtils.getConnection(dataSource);
			ps = connection.prepareStatement(INSERT_BY_ENTITY, id);
			ps.setString(1, entity.getTitle());
			ps.setString(2, entity.getShortText());
			ps.setString(3, entity.getFullText());
			Timestamp creationDate = dateToTimeStamp(entity.getCreationDate());
			Date modificationDate = new Date(entity.getModificationDate().getTime());
			ps.setTimestamp(4, creationDate);
			ps.setDate(5, modificationDate);
			ps.execute();
			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				insertId = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return insertId;
	}

	/**
	 * Updates news info by id
	 * 
	 * @param entity
	 * @return true if update is successful, returns false otherwise
	 * @throws DAOException
	 */
	@Override
	public boolean update(News entity) throws DAOException {
		int updateFlag = 0;
		PreparedStatement ps = null;
		Connection connection = null;

		if (entity == null)
			return false;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			ps = connection.prepareStatement(UPDATE_BY_ID);
			ps.setString(1, entity.getTitle());
			ps.setString(2, entity.getShortText());
			ps.setString(3, entity.getFullText());
			Timestamp creationDate = dateToTimeStamp(entity.getCreationDate());
			Date modificationDate = new Date(entity.getModificationDate().getTime());
			ps.setTimestamp(4, creationDate);
			ps.setDate(5, modificationDate);
			ps.setLong(6, entity.getId());
			updateFlag = ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return updateFlag == 1;
	}

	/**
	 * Inserts info about news author
	 * 
	 * @param NEWS_ID
	 * @param authorId
	 * @return true if insert is successful, returns false otherwise
	 * @throws DAOException
	 */
	@Override
	public boolean insertNewsAuthor(Long NEWS_ID, Long authorId) throws DAOException {
		PreparedStatement ps = null;
		Connection connection = null;
		boolean insertFlag = false;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			ps = connection.prepareStatement(INSERT_NEWS_AUTHOR);
			ps.setLong(1, NEWS_ID);
			ps.setLong(2, authorId);
			insertFlag = ps.execute();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return insertFlag;
	}

	/**
	 * Checks if there is already such tag(tags) for news and then insert it if
	 * it needed
	 * 
	 * @param NEWS_ID
	 *            news id
	 * @param tags
	 *            ArrayList of tags to be added
	 * @throws DAOException
	 */
	@Override
	public void insertNewsTags(Long NEWS_ID, List<Tag> tags) throws DAOException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		PreparedStatement check = null;
		Connection connection = null;
		if (tags == null)
			return;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			check = connection.prepareStatement(IS_TAG_FOR_NEWS);
			check.setLong(1, NEWS_ID);
			ps = connection.prepareStatement(INSERT_NEWS_TAG);
			for (Tag tag : tags) {
				ps.setLong(1, NEWS_ID);
				ps.setLong(2, tag.getId());
				ps.execute();
				closeResultSet(rs);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
			closeConnection(connection);
		}

	}

	/**
	 * Finds all news from given author
	 * 
	 * @param author
	 * @return List of all news by author
	 * @throws DAOException
	 */
	@Override
	public List<News> findByAuthor(Author author) throws DAOException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection connection = null;
		List<News> resultList = new ArrayList<News>();
		if (author == null)
			return null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			ps = connection.prepareStatement(FIND_NEWS_BY_AUTHOR);
			ps.setLong(1, author.getId());
			rs = ps.executeQuery();
			while (rs.next()) {
				News news = buildNews(rs);
				resultList.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
			closeConnection(connection);
		}

		return resultList;
	}

	/**
	 * Deletes all tags for news
	 * 
	 * @param id
	 *            of news
	 * @return true if delete is successful, returns false otherwise
	 * @throws DAOException
	 */
	@Override
	public boolean deleteNewsTags(Long id) throws DAOException {
		PreparedStatement ps = null;
		Connection connection = null;
		int deleteFlag = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			ps = connection.prepareStatement(DELETE_NEWS_TAGS);
			ps.setLong(1, id);
			deleteFlag = ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return deleteFlag == 1;
	}

	/**
	 * Deletes news author by news id
	 * 
	 * @param id
	 *            of news
	 * @return true if delete is successful, returns false otherwise
	 * @throws DAOException
	 */
	@Override
	public boolean deleteNewsAuthor(Long id) throws DAOException {
		PreparedStatement ps = null;
		Connection connection = null;
		int deleteFlag = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			ps = connection.prepareStatement(DELETE_NEWS_AUTHOR);
			ps.setLong(1, id);
			deleteFlag = ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return deleteFlag == 1;
	}

	/**
	 * Finds count of news by given tags, author, tags, page
	 * 
	 * @return list of news
	 * @throws DAOException
	 */
	@Override
	public int countNewsByFilter(FilterVO filterVO) throws DAOException {
		int infinite = 1_000_000;
		PreparedStatement ps = null;
		Connection connection = null;
		ResultSet rs = null;
		int result = 0;
		String sqlSelect = buildFilterSQL(filterVO);
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			String sqlCount = "SELECT COUNT(*) FROM(" + sqlSelect + ")";
			ps = connection.prepareStatement(sqlCount);

			int insertParam = insertPreparedStatementParams(ps, filterVO);
			ps.setInt(insertParam, 1);
			insertParam++;
			ps.setInt(insertParam, infinite);
			rs = ps.executeQuery();
			if (rs.next())
				result = rs.getInt(1);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return result;
	}

	/**
	 * Closes prepared statement
	 * 
	 * @param ps
	 * @throws DAOException
	 */
	private void closePreparedStatement(PreparedStatement ps) throws DAOException {
		try {
			if (ps != null) {
				ps.close();
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	/**
	 * Closes result set
	 * 
	 * @param rs
	 * @throws DAOException
	 */
	private void closeResultSet(ResultSet rs) throws DAOException {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	/**
	 * Closes connection
	 * 
	 * @param cn
	 * @throws DAOException
	 */
	private void closeConnection(Connection cn) {
		if (cn != null) {
			DataSourceUtils.releaseConnection(cn, dataSource);
		}
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * Builds news from ResultSet
	 * 
	 * @throws SQLException
	 */
	private News buildNews(ResultSet rs) throws SQLException {
		Long id = rs.getLong(NEWS_ID);
		String title = rs.getString(NEWS_TITLE);
		String shortText = rs.getString(NEWS_SHORT_TEXT);
		String fullText = rs.getString(NEWS_FULL_TEXT);
		Timestamp creationDate = rs.getTimestamp(NEWS_CREATION_DATE);
		Date modificationDate = rs.getDate(NEWS_MODIFICATION_DATE);
		News news = new News(id, title, shortText, fullText, creationDate, modificationDate);
		return news;
	}

	/**
	 * Returns list of news from filter on given page number. Page numbers begin
	 * with 1. News ordered by comments number and modification date
	 * 
	 * @param newsOnPage
	 *            how many news will be on page
	 * @param pageNumber
	 *            determine number of page which should be return
	 * @return list of news on given page
	 */
	public List<News> findNewsByFilters(int newsOnPage, int pageNumber, FilterVO filterVO) throws DAOException {
		PreparedStatement ps = null;
		Connection connection = null;
		ResultSet rs = null;
		List<News> news = new ArrayList<>();
		int startIndex, endIndex;
		if (pageNumber != 1) {
			startIndex = (pageNumber - 1) * newsOnPage + 1;
			endIndex = startIndex + newsOnPage - 1;
		} else {
			startIndex = 1;
			endIndex = newsOnPage;
		}
		try {

			String sql = buildFilterSQL(filterVO);
			connection = DataSourceUtils.getConnection(dataSource);
			ps = connection.prepareStatement(sql);
			int insertParam = insertPreparedStatementParams(ps, filterVO);
			ps.setLong(insertParam, startIndex);
			insertParam++;
			ps.setLong(insertParam, endIndex);
			rs = ps.executeQuery();
			while (rs.next()) {
				news.add(buildNews(rs));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		if (news.size() == 0)
			news = null;// for correct view work
		return news;
	}

	/**
	 * Finds news number in list of filtered news
	 * 
	 * @param NEWS_ID
	 * @return news number
	 * @throws DAOException
	 */
	public Integer newsNumberFromFilter(Long NEWS_ID, FilterVO filterVO) throws DAOException {
		int infinite = 1_000_000;
		String findNumberSQL = "SELECT rn FROM( SELECT newsId, newsTitle, newsShortText, newsFullText,  newsCreationDate,"
				+ " newsModificationDate, rownum rn FROM ( " + buildFilterSQL(filterVO) + ")" + ")" + "WHERE newsId=?";
		Integer result = null;
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			ps = connection.prepareStatement(findNumberSQL);

			int insertParam = insertPreparedStatementParams(ps, filterVO);
			ps.setLong(insertParam, 1);
			insertParam++;
			ps.setLong(insertParam, infinite);
			insertParam++;
			ps.setLong(insertParam, NEWS_ID);
			rs = ps.executeQuery();
			if (rs.next()) {
				result = rs.getInt(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return result;
	}

	/**
	 * Finds news from filter by news number
	 * 
	 * @param newsNumber
	 * @return news
	 * @throws DAOException
	 */
	@Override
	public News newsFromFilterByNumber(int newsNumber, FilterVO filterVO) throws DAOException {
		int infinite = 1_000_000;
		String findNewsSQL = "SELECT newsId, newsTitle, newsShortText, newsFullText, newsCreationDate,"
				+ " newsModificationDate FROM( SELECT newsId, newsTitle, newsShortText, newsFullText, newsCreationDate,"
				+ " newsModificationDate, rownum rn FROM ( " + buildFilterSQL(filterVO) + ")" + ")" + " WHERE rn=?";
		News result = null;
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			ps = connection.prepareStatement(findNewsSQL);

			int insertParam = insertPreparedStatementParams(ps, filterVO);
			ps.setLong(insertParam, 1);
			insertParam++;
			ps.setInt(insertParam, infinite);
			insertParam++;
			ps.setInt(insertParam, newsNumber);
			rs = ps.executeQuery();
			if (rs.next()) {
				result = buildNews(rs);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return result;
	}

	private String buildFilterSQL(FilterVO filterVO) {
		StringBuilder findNews = new StringBuilder(
				"SELECT newsId, newsTitle, newsShortText, newsFullText, newsCreationDate, newsModificationDate FROM ( "
						+ "SELECT newsId, newsTitle, total_message, newsShortText, newsFullText,  newsCreationDate, newsModificationDate, rownum rn FROM ( "
						+ "SELECT newsId, newsTitle, newsShortText, newsFullText, newsCreationDate, newsModificationDate, COUNT (COMMENTS.COMMENT_ID) as total_message FROM ( "
						+ "SELECT newsId, newsTitle, newsShortText, newsFullText, newsCreationDate, newsModificationDate FROM ( "
						+ "SELECT NEWS.NEWS_ID newsId, NEWS.TITLE newsTitle, NEWS.SHORT_TEXT newsShortText, NEWS.FULL_TEXT newsFullText,  NEWS.CREATION_DATE newsCreationDate, NEWS.MODIFICATION_DATE newsModificationDate FROM NEWS ");

		if (filterVO.getAuthorSearch() != null) {
			findNews.append(
					"INNER JOIN NEWS_AUTHOR ON NEWS.NEWS_ID=NEWS_AUTHOR.NEWS_ID WHERE NEWS_AUTHOR.AUTHOR_ID=? ");

		}

		findNews.append(" ) ");

		if (filterVO.getTagsSearch() != null && !filterVO.getTagsSearch().isEmpty()) {

			findNews.append(" JOIN NEWS_TAG on NEWS_TAG.NEWS_ID = newsId WHERE NEWS_TAG.TAG_ID IN( ");
			for (int i = 0; i < filterVO.getTagsSearch().size() - 1; i++) {
				findNews.append("?, ");
			}
			findNews.append(
					"? ) GROUP BY newsId, newsTitle, newsShortText, newsFullText, newsCreationDate, newsModificationDate ");
		}
		findNews.append(" ) ");
		findNews.append("LEFT JOIN COMMENTS ON COMMENTS.NEWS_ID= newsId "
				+ "GROUP BY newsId, newsTitle, newsShortText, newsFullText, newsCreationDate, newsModificationDate "
				+ "ORDER BY COUNT(COMMENTS.COMMENT_ID) DESC, newsModificationDate DESC ) ) "
				+ "WHERE rn BETWEEN ? AND ? ");

		return findNews.toString();
	}

	/**
	 * Inserts parameters into PreparedStatement for filter
	 */
	int insertPreparedStatementParams(PreparedStatement ps, FilterVO filterVO) throws SQLException {
		int insertParam = 1;
		if (filterVO.getAuthorSearch() != null) {
			ps.setLong(insertParam, filterVO.getAuthorSearch().getId());
			insertParam++;
		}
		if (filterVO.getTagsSearch() != null && !filterVO.getTagsSearch().isEmpty()) {
			for (int i = 0; i < filterVO.getTagsSearch().size(); i++) {
				ps.setLong(insertParam, filterVO.getTagsSearch().get(i).getId());
				insertParam++;
			}
		}
		return insertParam;
	}

	/**
	 * Converts date to Timestamp
	 * 
	 * @param date
	 * @return
	 */
	private Timestamp dateToTimeStamp(java.util.Date date) {
		return date == null ? null : new Timestamp(date.getTime());
	}
}
