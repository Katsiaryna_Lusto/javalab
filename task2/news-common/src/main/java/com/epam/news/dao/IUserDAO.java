package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.User;
import com.epam.news.exception.DAOException;

/**
 * Interface that provides operations with User
 *
 */
public interface IUserDAO extends IGenericDAO<User, Long> {
	/**
	 * Finds all users
	 * 
	 * @return list of users
	 * @throws DAOException
	 */
	List<User> findAll() throws DAOException;

	/**
	 * Finds user by login
	 * 
	 * @param login
	 * @return
	 * @throws DAOException
	 */
	User findByLogin(String login) throws DAOException;

	/**
	 * Finds user role
	 * 
	 * @param id
	 * @return
	 * @throws DAOException
	 */
	String findUserRole(Long id) throws DAOException;

}