package com.epam.news.service.impl;

import com.epam.news.dao.impl.UserDAOImpl;
import com.epam.news.entity.User;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

/**
 * Tests for UserServiceImpl
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
    @Mock
    User user;
    @Mock
    UserDAOImpl userDAO;
    @InjectMocks
    UserServiceImpl userService;

    /**
     * Test find user by login
     */
    @Test
    public void testFIndUserByLogin() throws ServiceException,DAOException
    {
        userService.findUser("login");
        verify(userDAO).findByLogin("login");
    }

    /**
     * Test find user role
     */
    @Test
    public void testFindUserRole() throws ServiceException,DAOException
    {
        userService.findUserRole(1L);
        verify(userDAO).findUserRole(1L);
    }
}
