package com.epam.news.service.impl;

import com.epam.news.dao.impl.TagDAOImpl;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

/**
 * Tests for TagServiceImpl
 */
@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {
    @Mock
    TagDAOImpl tagDAO;
    @Mock
    Tag tag;
    @InjectMocks
    TagServiceImpl tagService;

    /**
     * Test add tag
     */
    @Test
    public void testAddTad() throws ServiceException,DAOException
    {
        tagService.addTag(tag);
        verify(tagDAO).add(tag);
    }
    /**
     * Test delete tags for news
     */
    @Test
    public void testDeleteTagsForNews() throws ServiceException,DAOException
    {
        tagService.deleteTagFromNews(1L);
        verify(tagDAO).deleteTagFromNews(1L);
    }
    /**
     * Test delete tag
     */
    @Test
    public void setDeleteTag() throws ServiceException,DAOException
    {
        tagService.deleteTag(1L);
        verify(tagDAO).delete(1L);
    }
    /**
     * Test update tag
     */
    @Test
    public void testUpdateTag() throws ServiceException,DAOException
    {
        tagService.updateTag(tag);
        verify(tagDAO).update(tag);
    }
    /**
     * Test find by id
     */
    @Test
    public void testFindById() throws ServiceException,DAOException
    {
        tagService.findTag(1L);
        verify(tagDAO).findById(1L);
    }

    /**
     * Test find tags for news
     */
    @Test
    public void testFindTagsForNews() throws ServiceException,DAOException
    {
        tagService.findTagsForNews(1L);
        verify(tagDAO).findTagsForNews(1L);
    }

    /**
     * Test find all tags
     */
    @Test
    public void testFindAllTags() throws ServiceException,DAOException
    {
        tagService.findAllTags();
        verify(tagDAO).findAll();
    }
}
