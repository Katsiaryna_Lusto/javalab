package com.epam.news.dao.impl;

import java.sql.Timestamp;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.dao.ICommentDAO;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * Test C.R.U.D operations from CommentDAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-configuration-test.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, TransactionDbUnitTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class, DbUnitTestExecutionListener.class })
@Transactional
@DatabaseSetup("classpath:fullDB.xml")
public class CommentDAOTest {
	@Autowired
	ICommentDAO commentDAO;

	public CommentDAOTest() {
		super();
	}

	/**
	 * Test add method
	 */
	@Test
	public void testAdd() throws DAOException {
		Comment comment = new Comment(1, "qwerty", Timestamp.valueOf("2015-03-23 08:13:51.84"), 1);
		long id = commentDAO.add(comment);
		comment.setId(id);
		Assert.assertEquals(comment, commentDAO.findById(id));

	}

	/**
	 * Test edit
	 */
	@Test
	public void testEdit() throws DAOException {
		Comment comment = new Comment(1, "qwerty", Timestamp.valueOf("2015-03-23 08:13:51.84"), 1);
		boolean updateFlag = commentDAO.update(comment);
		Assert.assertTrue(updateFlag);
	}

	/**
	 * Test delete
	 */
	@Test
	public void testDelete() throws DAOException {
		Comment comment = new Comment(1, "qwerty", Timestamp.valueOf("2015-03-23 08:13:51.84"), 1);
		long id = commentDAO.add(comment);
		boolean deleteFlag = commentDAO.delete(id);
		Assert.assertTrue(deleteFlag);
	}

	/**
	 * Test find method
	 */
	@Test
	public void testFind() throws DAOException {
		Comment comment = commentDAO.findById(1L);
		Assert.assertNotNull(comment);
	}

	/**
	 * Test delete comments for news
	 */
	@Test
	public void testDeleteCommentsForNews() throws DAOException {
		commentDAO.deleteCommentsForNews(9L);
		List<Comment> comments = commentDAO.findAll();
		Assert.assertEquals(16, comments.size());
	}
}
