package com.epam.news.service.impl;

import com.epam.news.entity.Author;
import com.epam.news.entity.News;
import com.epam.news.entity.NewsVO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.ICommentService;
import com.epam.news.service.INewsManageService;
import com.epam.news.service.INewsService;
import com.epam.news.service.ITagService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NewsManageServiceTest {
    @Mock
    NewsServiceImpl newsService;
    @Mock
    CommentServiceImpl commentService;
    @Mock
    TagServiceImpl tagService;
    @Mock
    AuthorServiceImpl authorService;
    @InjectMocks
    NewsManageServiceImpl newsManageService;
    /**
     * Test delete news
     */
    @Test
    public void testDeleteNews() throws DAOException, ServiceException
    {

        NewsVO newsVO = mock(NewsVO.class);
        News news = mock(News.class);
        when(newsVO.getNews()).thenReturn(news);//determine the behavior of mocks
        when(news.getId()).thenReturn(1L);

        newsManageService.delete(newsVO.getNews().getId());

        verify(newsService).deleteNewsAuthor(1L);//check methods calls
        verify(newsService).deleteNewsTags(1L);
        verify(commentService).deleteCommentsForNews(1L);
        verify(newsService).deleteNews(news.getId());


    }

    /**
     * Test add news
     * @throws Exception
     */
    @Test
    public void testAddNews() throws Exception
    {

        NewsVO newsVO = mock(NewsVO.class);
        News news = mock(News.class);
        Author author = mock(Author.class);
        when(newsVO.getNews()).thenReturn(news);//determine the behavior of mocks

        when(newsVO.getTags()).thenReturn(new ArrayList<Tag>());

        when(newsVO.getAuthor()).thenReturn(author);
        when(author.getId()).thenReturn(1L);
        when(news.getId()).thenReturn(1L);

        newsManageService.add(newsVO);// call method from service

        verify(newsService).insertNewsAuthor(1L, 1L);//check methods calls
        verify(newsService).insertNewsTags(1L, newsVO.getTags());

    }

    /**
     * Test find NewsVO
     */
    @Test
    public void testFindNewsVO() throws Exception
    {
        newsManageService.findNewsVO(1L);

        verify(newsService).findById(1L);
        verify(commentService).findCommentsForNews(1L);
        verify(authorService).findNewsAuthor(1L);
        verify(tagService).findTagsForNews(1L);
    }
}
