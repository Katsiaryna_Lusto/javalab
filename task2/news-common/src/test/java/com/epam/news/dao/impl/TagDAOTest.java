package com.epam.news.dao.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.dao.ITagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * Test C.R.U.D operations from TagDAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-configuration-test.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, TransactionDbUnitTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class, DbUnitTestExecutionListener.class })
@Transactional
@DatabaseSetup("classpath:fullDB.xml")
public class TagDAOTest {

	@Autowired
	ITagDAO tagDAO;

	public TagDAOTest() {
		super();
	}

	/**
	 * Test add method
	 */
	@Test
	public void testAdd() throws DAOException {
		Tag tag = new Tag(1L, "qwerty");
		long id = tagDAO.add(tag);
		tag.setId(id);
		Assert.assertEquals(tag, tagDAO.findById(id));

	}

	/**
	 * Test edit
	
	@Test
	public void testEdit() throws DAOException {
		Tag tag = new Tag(1, "qwerty");
		boolean updateFlag = tagDAO.update(tag);
		Assert.assertTrue(updateFlag);
	}
 */
	/**
	 * Test delete
	
	@Test
	public void testDelete() throws DAOException {
		Tag tag = new Tag(1, "qwerty");
		boolean deleteFlag = tagDAO.delete(tag.getId());
		Assert.assertTrue(deleteFlag);
	}
 */
	/**
	 * Test find method
	
	@Test
	public void testFind() throws DAOException {
		Tag tag = tagDAO.findById(1L);
		Assert.assertNotNull(tag);
	} */
}
