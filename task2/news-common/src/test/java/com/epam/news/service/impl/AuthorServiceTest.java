package com.epam.news.service.impl;

import com.epam.news.dao.impl.AuthorDAOImpl;
import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

/**
 * Tests methods from AuthorServiceImpl
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {
    @Mock
    Author author;
    @Mock
    AuthorDAOImpl authorDAO;
    @InjectMocks
    AuthorServiceImpl authorService;

    /**
     * Test add author
     */
    @Test
    public void testAuthorAdd() throws ServiceException,DAOException
    {
        when(authorDAO.add(author)).thenReturn(1L);
        authorService.addAuthor(author);
        verify(authorDAO).add(author);
        verify(author).setId(1L);
    }

    /**
     * Test delete author
     */
    @Test
    public void testDeleteAuthor() throws ServiceException,DAOException
    {
        authorService.deleteAuthor(1L);
        verify(authorDAO).delete(1L);
    }
    /**
     * Test update author
     */
    @Test
    public void testUpdateAuthor() throws ServiceException,DAOException
    {
        authorService.updateAuthor(author);
        verify(authorDAO).update(author);
    }
    /**
     * Test find author
     */
    @Test
    public void testFindAuthor() throws ServiceException,DAOException
    {
        authorService.findAuthor(1L);
        verify(authorDAO).findById(1L);
    }
    /**
     * Test find all authors
     */
    @Test
    public void testFindAllAuthors() throws  ServiceException,DAOException
    {
        authorService.findAllAuthors();
        verify(authorDAO).findAll();
    }

    /**
     * Test find news author
     */
    @Test
    public void testFIndNewsAuthor() throws ServiceException,DAOException
    {
        authorService.findNewsAuthor(1L);
        verify(authorDAO).findNewsAuthor(1L);
    }

}
