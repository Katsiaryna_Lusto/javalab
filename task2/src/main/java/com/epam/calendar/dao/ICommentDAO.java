package com.epam.calendar.dao;

import java.util.List;

import com.epam.calendar.exception.DAOException;
import com.epam.calendar.model.Comment;

/**
 * Interface that provides operations with Comment
 *
 */
public interface ICommentDAO extends IGenericDAO<Comment, Long>{
	/**
	 * Finds all comments by event ID
	 * @throws DAOException
	 * @return List of events
	 */
	List<Comment> readCommentsByEventID(Long idEvent) throws DAOException;

}
