package com.epam.calendar.dao.impl;

import static com.epam.calendar.util.ColumnNames.date;
import static com.epam.calendar.util.ColumnNames.description;
import static com.epam.calendar.util.ColumnNames.idEvent;
import static com.epam.calendar.util.ColumnNames.idUser;
import static com.epam.calendar.util.ColumnNames.place;
import static com.epam.calendar.util.ColumnNames.timeFrom;
import static com.epam.calendar.util.ColumnNames.timeTo;
import static com.epam.calendar.util.ColumnNames.title;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.calendar.dao.IEventDAO;
import com.epam.calendar.exception.DAOException;
import com.epam.calendar.model.Event;
/**
 * Class that realize operations with event using JDBC
 *
 */
public class EventDAOImpl implements IEventDAO{

	private DataSource dataSource;
	
	private static final String CREATE_EVENT = "INSERT INTO event (title, date, time_from, time_to, place, description, id_user) VALUES  (?,?,?,?,?,?,?);";
	private static final String READ_EVENT = "SELECT id_event,title,date,time_from,time_to,place,description,id_user FROM  event  WHERE id_event=?;";
	private static final String UPDATE_EVENT = "UPDATE  event SET title=?, date=?,time_from=?,time_to=?,place=?,description=?,id_user=? WHERE id_event=?;";
	private static final String DELETE_EVENT = "DELETE FROM  event WHERE id_event=?;";
	private static final String READ_ALL_EVENTS = "SELECT id_event,title,date,time_from,time_to,place,description,id_user FROM  event;";
	/**
	 * Creates dataSource bean
	 * 
	 * @param dataSource
	 */
	@Override
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	/**
	 * Creates event with given info
	 * 
	 * @param entity
	 * @return id of inserted event, 0 if insert was unsuccessful
	 * @throws DAOException
	 */
	@Override
	public Long create(Event entity) throws DAOException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection connection = null;
		Long insertId = 0L;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			ps = connection.prepareStatement(CREATE_EVENT,PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setString(1, entity.getTitle());
			ps.setDate(2, entity.getDate());
			ps.setTime(3, entity.getTimeFrom());
			ps.setTime(4, entity.getTimeTo());
			ps.setString(5, entity.getPlace());
			ps.setString(6, entity.getDescription());
			ps.setLong(7, entity.getIdUser());
			ps.execute();
			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				insertId = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return insertId;
	}
	/**
	 * Deletes event by id
	 * 
	 * @param id
	 * @return true if delete is successful, returns false otherwise
	 * @throws DAOException
	 */
	@Override
	public boolean delete(Long id) throws DAOException {
		PreparedStatement ps = null;
		Connection connection = null;
		int deleteFlag = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			ps = connection.prepareStatement(DELETE_EVENT);
			ps.setLong(1, id);
			deleteFlag = ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return deleteFlag == 1;
	}
	
	/**
	 * Finds event by id
	 * 
	 * @param id
	 * @return event if find is successful, returns null otherwise
	 * @throws DAOException
	 */
	@Override
	public Event read(Long id) throws DAOException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Event result = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			ps = connection.prepareStatement(READ_EVENT);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			if (rs.next()) {
				result = buildEvent(rs);
			}
		} catch (SQLException e) {
			throw new DAOException(e.getMessage());
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return result;
	}
	/**
	 * Updates event info by id
	 * 
	 * @param entity
	 * @return true if update is successful, returns false otherwise
	 * @throws DAOException
	 */
	@Override
	public boolean update(Event entity) throws DAOException {
		int updateFlag = 0;
		PreparedStatement ps = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			ps = connection.prepareStatement(UPDATE_EVENT);
			ps.setString(1, entity.getTitle());
			ps.setDate(2, entity.getDate());
			ps.setTime(3, entity.getTimeFrom());
			ps.setTime(4, entity.getTimeTo());
			ps.setString(5, entity.getPlace());
			ps.setString(6, entity.getDescription());
			ps.setLong(7, entity.getIdUser());
			ps.setLong(8, entity.getIdEvent());
			updateFlag = ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return updateFlag == 1;
	}
	/**
	 * Returns list of events
	 * 
	 * @return list of events
	 * @throws DAOException
	 */
	@Override
	public List<Event> readAll() throws DAOException  {
		Statement st = null;
		ResultSet rs = null;
		Connection connection = null;
		List<Event> resultList = new ArrayList<Event>();
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			st = connection.createStatement();
			rs = st.executeQuery(READ_ALL_EVENTS);
			while (rs.next()) {
				Event news = buildEvent(rs);
				resultList.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeResultSet(rs);
			closeStatement(st);
			closeConnection(connection);
		}

		return resultList;
	}
	
	/**
	 * Closes prepared statement
	 * 
	 * @param ps
	 * @throws DAOException
	 */
	private void closePreparedStatement(PreparedStatement ps) throws DAOException {
		try {
			if (ps != null) {
				ps.close();
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	/**
	 * Closes result set
	 * 
	 * @param rs
	 * @throws DAOException
	 */
	private void closeResultSet(ResultSet rs) throws DAOException {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	/**
	 * Closes connection
	 * 
	 * @param cn
	 * @throws DAOException
	 */
	private void closeConnection(Connection cn) {
		if (cn != null) {
			DataSourceUtils.releaseConnection(cn, dataSource);
		}
	}
	/**
	 * Closes statement
	 * 
	 * @param st
	 * @throws DAOException
	 */
	private void closeStatement(Statement st) throws DAOException {
		try {
			if (st != null) {
				st.close();
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}
	/**
	 * Builds event from ResultSet
	 * 
	 * @throws SQLException
	 */
	private Event buildEvent(ResultSet rs) throws SQLException {
		return new Event(rs.getLong(idEvent), rs.getString(title), rs.getDate(date),rs.getTime(timeFrom),rs.getTime(timeTo),rs.getString(place),rs.getString(description),rs.getLong(idUser));
	}
}
