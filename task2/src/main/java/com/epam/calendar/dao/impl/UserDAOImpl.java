package com.epam.calendar.dao.impl;
import static com.epam.calendar.util.ColumnNames.idUser;
import static com.epam.calendar.util.ColumnNames.imgUrl;
import static com.epam.calendar.util.ColumnNames.name;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.calendar.dao.IUserDAO;
import com.epam.calendar.exception.DAOException;
import com.epam.calendar.model.User;
/**
 * Class that realize operations with user using JDBC
 *
 */
public class UserDAOImpl implements IUserDAO{
	
	private DataSource dataSource;
	private static final String READ_USER = "SELECT id_user,name,img_url FROM calendar.user WHERE id_user = ?";
	
	/**
	 * Creates dataSource bean
	 * 
	 * @param dataSource
	 */
	@Override
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	@Override
	public User read(Long id) throws DAOException {
		PreparedStatement ps = null;
		Connection connection = null;
		ResultSet rs = null;
		User result = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			ps = connection.prepareStatement(READ_USER);
			ps.setLong(1, id);
			rs = ps.executeQuery();

			if (rs.next()) {
				result = buildUser(rs);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return result;
	}
	private User buildUser(ResultSet rs) throws SQLException {
		return new User(rs.getLong(idUser), rs.getString(name), rs.getString(imgUrl));
	}

	/**
	 * Closes prepared statement
	 * 
	 * @param ps
	 * @throws DAOException
	 */
	private void closePreparedStatement(PreparedStatement ps) throws DAOException {
		try {
			if (ps != null) {
				ps.close();
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	/**
	 * Closes result set
	 * 
	 * @param rs
	 * @throws DAOException
	 */
	private void closeResultSet(ResultSet rs) throws DAOException {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	/**
	 * Closes connection
	 * 
	 * @param cn
	 * @throws DAOException
	 */
	private void closeConnection(Connection cn) {
		if (cn != null) {
			DataSourceUtils.releaseConnection(cn, dataSource);
		}
	}
}
