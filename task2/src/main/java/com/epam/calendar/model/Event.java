package com.epam.calendar.model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

/**
 * CLass to store event info
 *
 */
public class Event implements Serializable {

	/**
     * serialVersionUID
     */

	private static final long serialVersionUID = -9186218410857212074L;

	  /**
     * Event ID
     */
	private Long idEvent;
	  /**
     * Event title
     */
	private String title;
	  /**
     * Event date
     */
	private Date date;
	  /**
     * Time of beginning of an event
     */
	private Time timeFrom;	
	  /**
     * Time of ending of an event
     */
	private Time timeTo;
	  /**
     * Event place
     */
	private String place;
	  /**
     * Event description
     */
	private String description;
	  /**
     * User ID
     */
	private Long idUser;
	
	public Event() {
	}

	public Event(Long idEvent, String title, Date date, Time timeFrom, Time timeTo, String place, String description,
			Long idUser) {
		super();
		this.idEvent = idEvent;
		this.title = title;
		this.date = date;
		this.timeFrom = timeFrom;
		this.timeTo = timeTo;
		this.place = place;
		this.description = description;
		this.idUser = idUser;
	}

	public Long getIdEvent() {
		return idEvent;
	}

	public void setIdEvent(Long idEvent) {
		this.idEvent = idEvent;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Time getTimeFrom() {
		return timeFrom;
	}

	public void setTimeFrom(Time timeFrom) {
		this.timeFrom = timeFrom;
	}

	public Time getTimeTo() {
		return timeTo;
	}

	public void setTimeTo(Time timeTo) {
		this.timeTo = timeTo;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((idEvent == null) ? 0 : idEvent.hashCode());
		result = prime * result + ((idUser == null) ? 0 : idUser.hashCode());
		result = prime * result + ((place == null) ? 0 : place.hashCode());
		result = prime * result + ((timeFrom == null) ? 0 : timeFrom.hashCode());
		result = prime * result + ((timeTo == null) ? 0 : timeTo.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Event other = (Event) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (idEvent == null) {
			if (other.idEvent != null)
				return false;
		} else if (!idEvent.equals(other.idEvent))
			return false;
		if (idUser == null) {
			if (other.idUser != null)
				return false;
		} else if (!idUser.equals(other.idUser))
			return false;
		if (place == null) {
			if (other.place != null)
				return false;
		} else if (!place.equals(other.place))
			return false;
		if (timeFrom == null) {
			if (other.timeFrom != null)
				return false;
		} else if (!timeFrom.equals(other.timeFrom))
			return false;
		if (timeTo == null) {
			if (other.timeTo != null)
				return false;
		} else if (!timeTo.equals(other.timeTo))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	


}
