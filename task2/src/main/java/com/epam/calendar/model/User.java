package com.epam.calendar.model;

import java.io.Serializable;

/**
 * CLass to store user info
 *
 */
public class User implements Serializable {
	
	  /**
     * serialVersionUID
     */
	private static final long serialVersionUID = -5644125834885497470L;
	  /**
     * User ID
     */
	private Long idUser;
	  /**
     * User name
     */
	private String name;
	  /**
     * User image URL
     */
	private String imgUrl;

	public User() {
	}

	public User(Long idUser, String name, String imgUrl) {
		super();
		this.idUser = idUser;
		this.name = name;
		this.imgUrl = imgUrl;
	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idUser == null) ? 0 : idUser.hashCode());
		result = prime * result + ((imgUrl == null) ? 0 : imgUrl.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (idUser == null) {
			if (other.idUser != null)
				return false;
		} else if (!idUser.equals(other.idUser))
			return false;
		if (imgUrl == null) {
			if (other.imgUrl != null)
				return false;
		} else if (!imgUrl.equals(other.imgUrl))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
}
