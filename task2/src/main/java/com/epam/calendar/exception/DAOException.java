package com.epam.calendar.exception;

public class DAOException extends Exception{
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -3222366123960167703L;
    public DAOException(String message) {
        super(message);
    }
    public DAOException(Throwable e) {
        super(e.getMessage(),e);
    }
    public DAOException(String message,Throwable e) {
        super(message,e);
    }
}
