package com.epam.calendar.util;

public class ColumnNames {
	private ColumnNames() {
	};

	public static final String idComment = "id_comment";
	public static final String text = "text";
	public static final String dateTime = "date_time";
	
	public static final String idEvent = "id_event";
	public static final String title = "title";
	public static final String date = "date";
	public static final String timeFrom = "time_from";
	public static final String timeTo = "time_to";
	public static final String place = "place";
	public static final String description = "description";
	
	public static final String idUser = "id_user";
	public static final String name = "name";
	public static final String imgUrl = "img_url";
}
