package com.epam.calendar.dao.impl;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.calendar.dao.IEventDAO;
import com.epam.calendar.exception.DAOException;
import com.epam.calendar.model.Comment;
import com.epam.calendar.model.Event;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;


/**
 * Test  operations from EventDAO
 */
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/spring-configuration-test.xml" })
@DatabaseSetup(value = "classpath:/fullDB.xml")
public class EventDAOTest {
	@Autowired
	IEventDAO eventDAO;
	
	/**
	 * Test create method
	 * 
	 * @throws DAOException
	 */
	@Test
	public void testCreate() throws DAOException {
		Event event = new Event(7L,"title",Date.valueOf("1999-10-31"),null,null,null,"description",1L);
		Long id = 0L;
		id = eventDAO.create(event);
		event.setIdEvent(id);
		Assert.assertEquals(event, eventDAO.read(id));
	}
	
	/**
	 * Test update method
	 * 
	 * @throws DAOException
	 */
	@Test
	public void testUpdate() throws DAOException {
		Event event = new Event(7L,"title",Date.valueOf("1999-10-31"),null,null,null,"description",1L);
		Long id  = eventDAO.create(event);
		boolean updateFlag = eventDAO.update(new Event(id,"title",Date.valueOf("1999-10-31"),null,null,null,"updated",1L));
		Assert.assertTrue(updateFlag);
	}
	
	/**
	 * Test delete method
	 * 
	 * @throws DAOException
	 */
	@Test
	public void testDelete() throws DAOException {
		Event event = new Event(7L,"title",Date.valueOf("1999-10-31"),null,null,null,"description",1L);
		Long id = eventDAO.create(event);
		boolean deleteFlag = eventDAO.delete(id);
		Assert.assertTrue(deleteFlag);
	}
	
	/**
	 * Test readAll method
	 * 
	 * @throws DAOException
	 */
	@Test
	public void testReadAll() throws DAOException {
		List<Event> resultList = eventDAO.readAll();
		Assert.assertEquals(6,resultList.size());
	}
}
