package com.epam.calendar.dao.JDBC;

import java.sql.Timestamp;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.calendar.dao.IUserDAO;
import com.epam.calendar.exception.DAOException;
import com.epam.calendar.model.User;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * Test  operations from UserDAO
 */
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/spring-configuration-test.xml" })
@DatabaseSetup(value = "classpath:/fullDB.xml")
public class UserDAOTest {
	@Autowired
	IUserDAO userDAO;
	/**
	 * Test read method
	 * 
	 * @throws DAOException
	 */
	@Test
	public void testRead() throws DAOException {
		User user = userDAO.read(1L);
		Assert.assertEquals(user.getName(),"user1");
	}
	
	/**
	 * Test readByName method
	 * 
	 * @throws DAOException
	 */
	@Test
	public void readByName() throws DAOException {
		User user = userDAO.readByName("user1");
		Assert.assertEquals(user.getName(),"user1");
	}
}
