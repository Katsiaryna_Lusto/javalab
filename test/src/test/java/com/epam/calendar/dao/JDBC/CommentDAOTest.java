package com.epam.calendar.dao.JDBC;

import java.sql.Timestamp;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.calendar.dao.ICommentDAO;
import com.epam.calendar.exception.DAOException;
import com.epam.calendar.model.Comment;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
/**
 * Test  operations from CommentDAO
 */
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/spring-configuration-test.xml" })
@DatabaseSetup(value = "classpath:/fullDB.xml")
public class CommentDAOTest {
	@Autowired
	ICommentDAO commentDAO;

	public CommentDAOTest() {
		super();
	}
	
	/**
	 * Test create method
	 * 
	 * @throws DAOException
	 */
	@Test
	public void testCreate() throws DAOException {
		Comment comment = new Comment(12L, "qwerty", Timestamp.valueOf("1999-01-01 00:00:00.000000"), 1L, 1L);
		Long id = 0L;
		id = commentDAO.create(comment);
		comment.setIdComment(id);
		Assert.assertEquals(comment, commentDAO.read(id));
	}
	
	/**
	 * Test update method
	 * 
	 * @throws DAOException
	 */
	@Test
	public void testUpdate() throws DAOException {
		Comment comment = new Comment(122L, "qwerty", Timestamp.valueOf("1999-01-01 00:00:00.000000"), 1L, 1L);
		Long id  = commentDAO.create(comment);
		boolean updateFlag = commentDAO.update(new Comment(id, "qwerty UPDATED", Timestamp.valueOf("1999-01-01 00:00:00.000000"), 1L, 1L));
		Assert.assertTrue(updateFlag);
	}
	
	/**
	 * Test delete method
	 * 
	 * @throws DAOException
	 */
	@Test
	public void testDelete() throws DAOException {
		Comment comment = new Comment(12L, "qwerty", Timestamp.valueOf("1999-01-01 00:00:00.000000"), 1L, 1L);
		Long id = commentDAO.create(comment);
		boolean deleteFlag = commentDAO.delete(id);
		Assert.assertTrue(deleteFlag);
	}
	
	/**
	 * Test readCommentsByEventID method
	 * 
	 * @throws DAOException
	 */
	@Test
	public void testReadCommentsByEventID() throws DAOException {
		List<Comment> resultList = commentDAO.readCommentsByEventID(1L);
		Assert.assertEquals(1,resultList.size());
	}
	
}
