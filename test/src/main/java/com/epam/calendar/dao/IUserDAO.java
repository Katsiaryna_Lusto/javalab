package com.epam.calendar.dao;

import javax.sql.DataSource;

import com.epam.calendar.exception.DAOException;
import com.epam.calendar.model.User;

/**
 * Interface that provides operations with User
 *
 */
public interface IUserDAO {
	/**
	 * Finds User by name
	 * 
	 * @param name
	 * @throws DAOException
	 * @return User if find is successful, returns null otherwise
	 */

	User readByName(String name) throws DAOException;
	/**
	 * Finds User by id
	 * 
	 * @param id
	 * @throws DAOException
	 * @return User if find is successful, returns null otherwise
	 */
	User read(Long id)  throws DAOException ;

}
