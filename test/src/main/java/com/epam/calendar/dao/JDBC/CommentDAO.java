package com.epam.calendar.dao.JDBC;

import static com.epam.calendar.util.ConstantsUtil.dateTime;
import static com.epam.calendar.util.ConstantsUtil.idComment;
import static com.epam.calendar.util.ConstantsUtil.idEvent;
import static com.epam.calendar.util.ConstantsUtil.text;
import static com.epam.calendar.util.ConstantsUtil.idCommentUser;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.calendar.dao.ICommentDAO;
import com.epam.calendar.exception.DAOException;
import com.epam.calendar.model.Comment;
import static com.epam.calendar.util.ConstantsUtil.CREATE_COMMENT;
import static com.epam.calendar.util.ConstantsUtil.READ_COMMENT;
import static com.epam.calendar.util.ConstantsUtil.UPDATE_COMMENT;
import static com.epam.calendar.util.ConstantsUtil.DELETE_COMMENT;
import static com.epam.calendar.util.ConstantsUtil.READ_COMMENTS_BY_EVENT_ID;

/**
 * Class that realize operations with comment using JDBC
 *
 */
public class CommentDAO implements ICommentDAO {

    private DataSource dataSource;

    /**
     * Creates dataSource bean
     * 
     * @param dataSource
     */
    public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
    }

    /**
     * Creates comment with given info
     * 
     * @param entity
     * @return id of inserted comment, 0 if insert was unsuccessful
     * @throws DAOException
     */
    public Long create(Comment entity) throws DAOException {
	PreparedStatement ps = null;
	ResultSet rs = null;
	Connection connection = null;
	Long insertId = 0L;
	try {
	    connection = DataSourceUtils.getConnection(dataSource);
	    ps = connection.prepareStatement(CREATE_COMMENT, PreparedStatement.RETURN_GENERATED_KEYS);
	    ps.setString(1, entity.getText());
	    ps.setTimestamp(2, entity.getDateTime());
	    ps.setLong(3, entity.getIdEvent());
	    ps.setLong(4, entity.getIdUser());
	    ps.execute();
	    rs = ps.getGeneratedKeys();
	    if (rs.next()) {
		insertId = rs.getLong(1);
	    }
	} catch (SQLException e) {
	    throw new DAOException(e);
	} finally {
	    closeResultSet(rs);
	    closePreparedStatement(ps);
	    closeConnection(connection);
	}
	return insertId;
    }

    /**
     * Deletes comment by id
     * 
     * @param id
     * @return true if delete is successful, returns false otherwise
     * @throws DAOException
     */
    public boolean delete(Long id) throws DAOException {
	PreparedStatement ps = null;
	Connection connection = null;
	int deleteFlag = 0;
	try {
	    connection = DataSourceUtils.getConnection(dataSource);
	    ps = connection.prepareStatement(DELETE_COMMENT);
	    ps.setLong(1, id);
	    deleteFlag = ps.executeUpdate();
	} catch (SQLException e) {
	    throw new DAOException(e);
	} finally {
	    closePreparedStatement(ps);
	    closeConnection(connection);
	}
	return deleteFlag == 1;
    }

    /**
     * Updates comment info by id
     * 
     * @param entity
     * @return true if update is successful, returns false otherwise
     * @throws DAOException
     */
    public boolean update(Comment entity) throws DAOException {
	int updateFlag = 0;
	PreparedStatement ps = null;
	Connection connection = null;
	try {
	    connection = DataSourceUtils.getConnection(dataSource);
	    ps = connection.prepareStatement(UPDATE_COMMENT);
	    ps.setString(1, entity.getText());
	    ps.setTimestamp(2, entity.getDateTime());
	    ps.setLong(3, entity.getIdEvent());
	    ps.setLong(4, entity.getIdUser());
	    ps.setLong(5, entity.getIdComment());
	    updateFlag = ps.executeUpdate();
	} catch (SQLException e) {
	    throw new DAOException(e);
	} finally {
	    closePreparedStatement(ps);
	    closeConnection(connection);
	}
	return updateFlag == 1;
    }

    /**
     * Finds comment by id
     * 
     * @param id
     * @return comment if find is successful, returns null otherwise
     * @throws DAOException
     */
    public Comment read(Long id) throws DAOException {
	PreparedStatement ps = null;
	ResultSet rs = null;
	Comment result = null;
	Connection connection = null;
	try {
	    connection = DataSourceUtils.getConnection(dataSource);
	    ps = connection.prepareStatement(READ_COMMENT);
	    ps.setLong(1, id);
	    rs = ps.executeQuery();
	    if (rs.next()) {
		result = buildComment(rs);
	    }
	} catch (SQLException e) {
	    throw new DAOException(e);
	} finally {
	    closeResultSet(rs);
	    closePreparedStatement(ps);
	    closeConnection(connection);
	}
	return result;
    }

    /**
     * Finds all comments by event id
     * 
     * @return List of comments
     * @throws DAOException
     */

    public List<Comment> readCommentsByEventID(Long idEvent) throws DAOException {
	PreparedStatement ps = null;
	ResultSet rs = null;
	Connection connection = null;
	List<Comment> resultList = new ArrayList<Comment>();
	try {
	    connection = DataSourceUtils.getConnection(dataSource);
	    ps = connection.prepareStatement(READ_COMMENTS_BY_EVENT_ID);
	    ps.setLong(1, idEvent);
	    rs = ps.executeQuery();
	    while (rs.next()) {
		Comment news = buildComment(rs);
		resultList.add(news);
	    }
	} catch (SQLException e) {
	    throw new DAOException("Couldn't read comments by event id, \n" + e.getMessage(), e);
	} finally {
	    closeResultSet(rs);
	    closePreparedStatement(ps);
	    closeConnection(connection);
	}

	return resultList;
    }

    /**
     * Closes prepared statement
     * 
     * @param ps
     * @throws DAOException
     */
    private void closePreparedStatement(PreparedStatement ps) throws DAOException {
	try {
	    if (ps != null) {
		ps.close();
	    }
	} catch (SQLException e) {
	    throw new DAOException(e);
	}
    }

    /**
     * Closes result set
     * 
     * @param rs
     * @throws DAOException
     */
    private void closeResultSet(ResultSet rs) throws DAOException {
	try {
	    if (rs != null) {
		rs.close();
	    }
	} catch (SQLException e) {
	    throw new DAOException(e);
	}
    }

    /**
     * Closes connection
     * 
     * @param cn
     * @throws DAOException
     */
    private void closeConnection(Connection cn) {
	if (cn != null) {
	    DataSourceUtils.releaseConnection(cn, dataSource);
	}
    }

    /**
     * Builds comment from ResultSet
     * 
     * @throws SQLException
     */
    private Comment buildComment(ResultSet rs) throws SQLException {
	return new Comment(rs.getLong(idComment), rs.getString(text), rs.getTimestamp(dateTime),
		rs.getLong(idEvent), rs.getLong(idCommentUser));
    }

}
