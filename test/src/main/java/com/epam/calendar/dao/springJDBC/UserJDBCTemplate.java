package com.epam.calendar.dao.springJDBC;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.epam.calendar.dao.IUserDAO;
import com.epam.calendar.dao.springJDBC.mappers.UserMapper;
import com.epam.calendar.exception.DAOException;
import com.epam.calendar.model.User;

import static com.epam.calendar.util.ConstantsUtil.READ_USER_BY_ID;
import static com.epam.calendar.util.ConstantsUtil.READ_USER_BY_NAME;

public class UserJDBCTemplate implements IUserDAO {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
	this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    @Override
    public User readByName(String name) throws DAOException {
	User user;
	try {
	    user = jdbcTemplateObject.queryForObject(READ_USER_BY_NAME, new Object[] { name }, new UserMapper());
	} catch (DataAccessException e) {
	    throw new DAOException(e);
	}
	return user;
    }

    @Override
    public User read(Long id) throws DAOException {
	User user;
	try {
	    user = jdbcTemplateObject.queryForObject(READ_USER_BY_ID, new Object[] { id }, new UserMapper());
	} catch (DataAccessException e) {
	    throw new DAOException(e);
	}
	return user;
    }
}
