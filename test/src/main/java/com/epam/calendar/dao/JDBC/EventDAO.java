package com.epam.calendar.dao.JDBC;

import static com.epam.calendar.util.ConstantsUtil.date;
import static com.epam.calendar.util.ConstantsUtil.description;
import static com.epam.calendar.util.ConstantsUtil.idEvent;
import static com.epam.calendar.util.ConstantsUtil.idUser;
import static com.epam.calendar.util.ConstantsUtil.place;
import static com.epam.calendar.util.ConstantsUtil.timeFrom;
import static com.epam.calendar.util.ConstantsUtil.timeTo;
import static com.epam.calendar.util.ConstantsUtil.title;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.calendar.dao.IEventDAO;
import com.epam.calendar.exception.DAOException;
import com.epam.calendar.model.Event;

import static com.epam.calendar.util.ConstantsUtil.COUNT_EVENTS;
import static com.epam.calendar.util.ConstantsUtil.CREATE_EVENT;
import static com.epam.calendar.util.ConstantsUtil.DELETE_EVENT;
import static com.epam.calendar.util.ConstantsUtil.READ_EVENT;
import static com.epam.calendar.util.ConstantsUtil.READ_EVENTS;
import static com.epam.calendar.util.ConstantsUtil.UPDATE_EVENT;

/**
 * Class that realize operations with event using JDBC
 *
 */
public class EventDAO implements IEventDAO {

    private DataSource dataSource;

    /**
     * Creates dataSource bean
     * 
     * @param dataSource
     */

    public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
    }

    /**
     * Creates event with given info
     * 
     * @param entity
     * @return id of inserted event, 0 if insert was unsuccessful
     * @throws DAOException
     */

    public Long create(Event entity) throws DAOException {
	PreparedStatement ps = null;
	ResultSet rs = null;
	Connection connection = null;
	Long insertId = 0L;
	try {
	    connection = DataSourceUtils.getConnection(dataSource);
	    ps = connection.prepareStatement(CREATE_EVENT, PreparedStatement.RETURN_GENERATED_KEYS);
	    ps.setString(1, entity.getTitle());
	    ps.setDate(2, (Date) entity.getDate());
	    ps.setTime(3, entity.getTimeFrom());
	    ps.setTime(4, entity.getTimeTo());
	    ps.setString(5, entity.getPlace());
	    ps.setString(6, entity.getDescription());
	    ps.setLong(7, entity.getIdUser());
	    ps.execute();
	    rs = ps.getGeneratedKeys();
	    if (rs.next()) {
		insertId = rs.getLong(1);
	    }
	} catch (SQLException e) {
	    throw new DAOException(e);
	} finally {
	    closeResultSet(rs);
	    closePreparedStatement(ps);
	    closeConnection(connection);
	}
	return insertId;
    }

    /**
     * Deletes event by id
     * 
     * @param id
     * @return true if delete is successful, returns false otherwise
     * @throws DAOException
     */

    public boolean delete(Long id) throws DAOException {
	PreparedStatement ps = null;
	Connection connection = null;
	int deleteFlag = 0;
	try {
	    connection = DataSourceUtils.getConnection(dataSource);
	    ps = connection.prepareStatement(DELETE_EVENT);
	    ps.setLong(1, id);
	    deleteFlag = ps.executeUpdate();
	} catch (SQLException e) {
	    throw new DAOException(e);
	} finally {
	    closePreparedStatement(ps);
	    closeConnection(connection);
	}
	return deleteFlag == 1;
    }

    /**
     * Finds event by id
     * 
     * @param id
     * @return event if find is successful, returns null otherwise
     * @throws DAOException
     */

    public Event read(Long id) throws DAOException {
	PreparedStatement ps = null;
	ResultSet rs = null;
	Event result = null;
	Connection connection = null;
	try {
	    connection = DataSourceUtils.getConnection(dataSource);
	    ps = connection.prepareStatement(READ_EVENT);
	    ps.setLong(1, id);
	    rs = ps.executeQuery();
	    if (rs.next()) {
		result = buildEvent(rs);
	    }
	} catch (SQLException e) {
	    throw new DAOException(e.getMessage());
	} finally {
	    closeResultSet(rs);
	    closePreparedStatement(ps);
	    closeConnection(connection);
	}
	return result;
    }

    /**
     * Updates event info by id
     * 
     * @param entity
     * @return true if update is successful, returns false otherwise
     * @throws DAOException
     */

    public boolean update(Event entity) throws DAOException {
	int updateFlag = 0;
	PreparedStatement ps = null;
	Connection connection = null;
	try {
	    connection = DataSourceUtils.getConnection(dataSource);
	    ps = connection.prepareStatement(UPDATE_EVENT);
	    ps.setString(1, entity.getTitle());
	    ps.setDate(2, (Date) entity.getDate());
	    ps.setTime(3, entity.getTimeFrom());
	    ps.setTime(4, entity.getTimeTo());
	    ps.setString(5, entity.getPlace());
	    ps.setString(6, entity.getDescription());
	    ps.setLong(7, entity.getIdUser());
	    ps.setLong(8, entity.getIdEvent());
	    updateFlag = ps.executeUpdate();
	} catch (SQLException e) {
	    throw new DAOException(e);
	} finally {
	    closePreparedStatement(ps);
	    closeConnection(connection);
	}
	return updateFlag == 1;
    }

    /**
     * Returns list of events
     * 
     * @param amount
     *            of events on page, page number
     * @return list of events
     * @throws DAOException
     */

    public List<Event> readEvents(int eventsOnPage, int pageNumber) throws DAOException {
	PreparedStatement ps = null;
	ResultSet rs = null;
	Connection connection = null;
	List<Event> resultList = new ArrayList<Event>();
	int startIndex, endIndex;
	if (pageNumber != 1) {
	    startIndex = (pageNumber - 1) * eventsOnPage;
	    endIndex = eventsOnPage;
	} else {
	    startIndex = 0;
	    endIndex = eventsOnPage;
	}
	try {
	    connection = DataSourceUtils.getConnection(dataSource);
	    ps = connection.prepareStatement(READ_EVENTS);
	    ps.setInt(1, startIndex);
	    ps.setInt(2, endIndex);
	    rs = ps.executeQuery();
	    while (rs.next()) {
		Event news = buildEvent(rs);
		resultList.add(news);
	    }
	} catch (SQLException e) {
	    throw new DAOException(e);
	} finally {
	    closeResultSet(rs);
	    closePreparedStatement(ps);
	    closeConnection(connection);
	}

	return resultList;
    }

    public int countEvents() throws DAOException {
	Statement st = null;
	ResultSet rs = null;
	Connection connection = null;
	int result = 0;
	try {
	    connection = DataSourceUtils.getConnection(dataSource);
	    st = connection.createStatement();
	    rs = st.executeQuery(COUNT_EVENTS);
	    if (rs.next()) {
		result = rs.getInt("count");
	    }
	} catch (SQLException e) {
	    throw new DAOException(e);
	} finally {
	    closeResultSet(rs);
	    closeStatement(st);
	    closeConnection(connection);
	}
	return result;
    }

    /**
     * Closes prepared statement
     * 
     * @param ps
     * @throws DAOException
     */
    private void closePreparedStatement(PreparedStatement ps) throws DAOException {
	try {
	    if (ps != null) {
		ps.close();
	    }
	} catch (SQLException e) {
	    throw new DAOException(e);
	}
    }

    /**
     * Closes result set
     * 
     * @param rs
     * @throws DAOException
     */
    private void closeResultSet(ResultSet rs) throws DAOException {
	try {
	    if (rs != null) {
		rs.close();
	    }
	} catch (SQLException e) {
	    throw new DAOException(e);
	}
    }

    /**
     * Closes connection
     * 
     * @param cn
     * @throws DAOException
     */
    private void closeConnection(Connection cn) {
	if (cn != null) {
	    DataSourceUtils.releaseConnection(cn, dataSource);
	}
    }

    /**
     * Closes statement
     * 
     * @param st
     * @throws DAOException
     */
    private void closeStatement(Statement st) throws DAOException {
	try {
	    if (st != null) {
		st.close();
	    }
	} catch (SQLException e) {
	    throw new DAOException(e);
	}
    }

    /**
     * Builds event from ResultSet
     * 
     * @throws SQLException
     */
    private Event buildEvent(ResultSet rs) throws SQLException {
	return new Event(rs.getLong(idEvent), rs.getString(title), (java.util.Date) rs.getDate(date),
		rs.getTime(timeFrom), rs.getTime(timeTo), rs.getString(place), rs.getString(description),
		rs.getLong(idUser));
    }
}
