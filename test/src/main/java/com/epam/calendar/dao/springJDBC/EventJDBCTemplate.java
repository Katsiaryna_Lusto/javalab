package com.epam.calendar.dao.springJDBC;

import static com.epam.calendar.util.ConstantsUtil.COUNT_EVENTS;
import static com.epam.calendar.util.ConstantsUtil.CREATE_EVENT;
import static com.epam.calendar.util.ConstantsUtil.DELETE_EVENT;
import static com.epam.calendar.util.ConstantsUtil.READ_EVENT;
import static com.epam.calendar.util.ConstantsUtil.READ_EVENTS;
import static com.epam.calendar.util.ConstantsUtil.UPDATE_EVENT;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.epam.calendar.dao.IEventDAO;
import com.epam.calendar.dao.springJDBC.mappers.EventMapper;
import com.epam.calendar.exception.DAOException;
import com.epam.calendar.model.Event;

public class EventJDBCTemplate implements IEventDAO {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
	this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    @Override
    public Long create(Event entity) throws DAOException {
	try {
	    KeyHolder holder = new GeneratedKeyHolder();
	    jdbcTemplateObject.update(new PreparedStatementCreator() {
		@Override
		public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
		    PreparedStatement ps = connection.prepareStatement(CREATE_EVENT, Statement.RETURN_GENERATED_KEYS);
		    ps.setString(1, entity.getTitle());
		    ps.setDate(2, (Date) entity.getDate());
		    ps.setTime(3, entity.getTimeFrom());
		    ps.setTime(4, entity.getTimeTo());
		    ps.setString(5, entity.getPlace());
		    ps.setString(6, entity.getDescription());
		    ps.setLong(7, entity.getIdUser());
		    return ps;
		}
	    }, holder);
	    Long id = holder.getKey().longValue();
	    return id;
	} catch (DataAccessException e) {
	    throw new DAOException(e);
	}
    }

    @Override
    public Event read(Long id) throws DAOException {
	try {
	    Event event = jdbcTemplateObject.queryForObject(READ_EVENT, new Object[] { id }, new EventMapper());
	    return event;
	} catch (DataAccessException e) {
	    throw new DAOException(e);
	}
    }

    @Override
    public boolean update(Event entity) throws DAOException {
	try {
	    int flag = jdbcTemplateObject
		    .update(UPDATE_EVENT,
			    new Object[] { entity.getTitle(), entity.getDate(), entity.getTimeFrom(),
				    entity.getTimeTo(), entity.getPlace(), entity.getDescription(), entity.getIdUser(),
				    entity.getIdEvent() });
	    return flag > 0;
	} catch (DataAccessException e) {
	    throw new DAOException(e);
	}
    }

    @Override
    public boolean delete(Long id) throws DAOException {
	try {
	    int flag = jdbcTemplateObject.update(DELETE_EVENT, new Object[] { id });
	    return flag > 0;
	} catch (DataAccessException e) {
	    throw new DAOException(e);
	}
    }

    @Override
    public List<Event> readEvents(int eventsOnPage, int pageNumber) throws DAOException {
	try {
	    int startIndex, endIndex;
	    if (pageNumber != 1) {
		startIndex = (pageNumber - 1) * eventsOnPage;
		endIndex = eventsOnPage;
	    } else {
		startIndex = 0;
		endIndex = eventsOnPage;
	    }
	    return jdbcTemplateObject.query(READ_EVENTS, new EventMapper(), new Object[] { startIndex, endIndex });
	} catch (DataAccessException e) {
	    throw new DAOException(e);
	}
    }

    @Override
    public int countEvents() throws DAOException {
	try {
	    return jdbcTemplateObject.queryForObject(COUNT_EVENTS, Integer.class);
	} catch (DataAccessException e) {
	    throw new DAOException(e);
	}
    }

}
