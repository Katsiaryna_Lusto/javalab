package com.epam.calendar.dao.springJDBC.mappers;
import java.sql.ResultSet;
import java.sql.SQLException;
import static com.epam.calendar.util.ConstantsUtil.idUser;
import static com.epam.calendar.util.ConstantsUtil.imgUrl;
import static com.epam.calendar.util.ConstantsUtil.name;
import org.springframework.jdbc.core.RowMapper;

import com.epam.calendar.model.User;

public class UserMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
	return new User(rs.getLong(idUser), rs.getString(name), rs.getString(imgUrl));
    }
}
