package com.epam.calendar.dao.springJDBC;

import static com.epam.calendar.util.ConstantsUtil.READ_USER_BY_NAME;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.epam.calendar.dao.ICommentDAO;
import com.epam.calendar.dao.springJDBC.mappers.CommentMapper;
import com.epam.calendar.exception.DAOException;
import com.epam.calendar.model.Comment;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;

import static com.epam.calendar.util.ConstantsUtil.CREATE_COMMENT;
import static com.epam.calendar.util.ConstantsUtil.READ_COMMENT;
import static com.epam.calendar.util.ConstantsUtil.UPDATE_COMMENT;
import static com.epam.calendar.util.ConstantsUtil.DELETE_COMMENT;
import static com.epam.calendar.util.ConstantsUtil.READ_COMMENTS_BY_EVENT_ID;

public class CommentJDBCTemplate implements ICommentDAO {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
	this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    @Override
    public Long create(Comment entity) throws DAOException {
	try {
	    KeyHolder holder = new GeneratedKeyHolder();
	    jdbcTemplateObject.update(new PreparedStatementCreator() {
		@Override
		public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
		    PreparedStatement ps = connection.prepareStatement(CREATE_COMMENT, Statement.RETURN_GENERATED_KEYS);
		    ps.setString(1, entity.getText());
		    ps.setTimestamp(2, entity.getDateTime());
		    ps.setLong(3, entity.getIdEvent());
		    ps.setLong(4, entity.getIdUser());
		    return ps;
		}
	    }, holder);

	    Long id = holder.getKey().longValue();
	    return id;
	} catch (DataAccessException e) {
	    throw new DAOException(e);
	}
    }

    @Override
    public Comment read(Long id) throws DAOException {
	Comment comment;
	try {
	    comment = jdbcTemplateObject.queryForObject(READ_COMMENT, new Object[] { id }, new CommentMapper());
	} catch (DataAccessException e) {
	    throw new DAOException(e);
	}
	return comment;
    }

    @Override
    public boolean update(Comment entity) throws DAOException {
	try {
	    int flag = jdbcTemplateObject.update(UPDATE_COMMENT, new Object[] { entity.getText(), entity.getDateTime(),
		    entity.getIdEvent(), entity.getIdUser(), entity.getIdComment() });
	    return flag > 0;
	} catch (DataAccessException e) {
	    throw new DAOException(e);
	}
    }

    @Override
    public boolean delete(Long id) throws DAOException {
	try {
	    int flag = jdbcTemplateObject.update(DELETE_COMMENT, new Object[] { id });
	    return flag > 0;
	} catch (DataAccessException e) {
	    throw new DAOException(e);
	}
    }

    @Override
    public List<Comment> readCommentsByEventID(Long idEvent) throws DAOException {
	try {
	    return jdbcTemplateObject.query(READ_COMMENTS_BY_EVENT_ID, new CommentMapper(), idEvent);
	} catch (DataAccessException e) {
	    throw new DAOException(e);
	}
    }

}
