package com.epam.calendar.dao.springJDBC.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import static com.epam.calendar.util.ConstantsUtil.dateTime;
import static com.epam.calendar.util.ConstantsUtil.idComment;
import static com.epam.calendar.util.ConstantsUtil.idEvent;
import static com.epam.calendar.util.ConstantsUtil.text;
import static com.epam.calendar.util.ConstantsUtil.idCommentUser;
import org.springframework.jdbc.core.RowMapper;

import com.epam.calendar.model.Comment;


public class CommentMapper implements RowMapper<Comment>{

    @Override
    public Comment mapRow(ResultSet rs, int rowNum) throws SQLException {
	return new Comment(rs.getLong(idComment), rs.getString(text), rs.getTimestamp(dateTime),
		rs.getLong(idEvent), rs.getLong(idCommentUser));
    }

}
