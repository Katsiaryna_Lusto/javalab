package com.epam.calendar.dao;

import java.util.List;

import com.epam.calendar.exception.DAOException;
import com.epam.calendar.model.Event;
import com.epam.calendar.model.User;

/**
 * Interface that provides operations with Event
 *
 */
public interface IEventDAO extends IGenericDAO<Event, Long>{
	/**
	 * Finds all events
	 * @throws DAOException
	 * @return List of events
	 */
	List<Event> readEvents(int eventsOnPage, int pageNumber) throws DAOException ;
	/**
	 * Count of events
	 * @throws DAOException
	 * @return Count of events
	 */
	public int countEvents() throws DAOException;
}
