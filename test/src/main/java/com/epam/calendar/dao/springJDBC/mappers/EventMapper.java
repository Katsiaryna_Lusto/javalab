package com.epam.calendar.dao.springJDBC.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import static com.epam.calendar.util.ConstantsUtil.date;
import static com.epam.calendar.util.ConstantsUtil.description;
import static com.epam.calendar.util.ConstantsUtil.idEvent;
import static com.epam.calendar.util.ConstantsUtil.idUser;
import static com.epam.calendar.util.ConstantsUtil.place;
import static com.epam.calendar.util.ConstantsUtil.timeFrom;
import static com.epam.calendar.util.ConstantsUtil.timeTo;
import static com.epam.calendar.util.ConstantsUtil.title;
import org.springframework.jdbc.core.RowMapper;

import com.epam.calendar.model.Event;

public class EventMapper implements RowMapper<Event>{

    @Override
    public Event mapRow(ResultSet rs, int rowNum) throws SQLException {
	return new Event(rs.getLong(idEvent), rs.getString(title), (java.util.Date) rs.getDate(date),
		rs.getTime(timeFrom), rs.getTime(timeTo), rs.getString(place), rs.getString(description),
		rs.getLong(idUser));
    }

}
