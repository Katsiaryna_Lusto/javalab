package com.epam.calendar.util;

public class ConstantsUtil {
    private ConstantsUtil() {
    };
    public static final int eventsOnPage = 3;
//columns names
    public static final String idComment = "id_comment";
    public static final String text = "text";
    public static final String dateTime = "date_time";
    public static final String idCommentUser = "id_comment_user";

    public static final String idEvent = "id_event";
    public static final String title = "title";
    public static final String date = "date";
    public static final String timeFrom = "time_from";
    public static final String timeTo = "time_to";
    public static final String place = "place";
    public static final String description = "description";

    public static final String idUser = "id_user";
    public static final String name = "name";
    public static final String imgUrl = "img_url";
    
//sql scripts
    public static final String READ_USER_BY_ID = "SELECT id_user,name,img_url FROM calendar.user WHERE id_user = ?";
    public static final String READ_USER_BY_NAME = "SELECT id_user,name,img_url FROM calendar.user WHERE name = ?";

    public static final String CREATE_EVENT = "INSERT INTO event (title, date, time_from, time_to, place, description, id_user) VALUES  (?,?,?,?,?,?,?);";
    public static final String READ_EVENT = "SELECT id_event,title,date,time_from,time_to,place,description,id_user FROM  event  WHERE id_event=?;";
    public static final String UPDATE_EVENT = "UPDATE  event SET title=?, date=?,time_from=?,time_to=?,place=?,description=?,id_user=? WHERE id_event=?;";
    public static final String DELETE_EVENT = "DELETE FROM  event WHERE id_event=?;";
    public static final String READ_EVENTS = "SELECT id_event,title,date,time_from,time_to,place,description,id_user FROM  event limit ?,?;";
    public static final String COUNT_EVENTS = "SELECT COUNT(*) as count FROM  event;";

    public static final String CREATE_COMMENT = "INSERT INTO comment (text,date_time,id_event,id_comment_user) VALUES (?,?,?,?);";
    public static final String READ_COMMENT = "SELECT id_comment,text,date_time,id_event,id_comment_user FROM  comment WHERE id_comment=?;";
    public static final String UPDATE_COMMENT = "UPDATE  comment SET text=?, date_time=?,id_event=?,id_comment_user=? WHERE id_comment=?;";
    public static final String DELETE_COMMENT = "DELETE FROM  comment WHERE id_comment=?;";
    public static final String READ_COMMENTS_BY_EVENT_ID = "SELECT id_comment,text,date_time,id_event,id_comment_user FROM  comment WHERE id_event=?;";

}
