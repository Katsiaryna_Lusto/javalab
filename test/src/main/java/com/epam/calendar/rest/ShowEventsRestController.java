package com.epam.calendar.rest;

import java.util.ArrayList;
import java.util.List;
import static com.epam.calendar.util.ConstantsUtil.eventsOnPage;
import org.apache.log4j.Logger;

import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;
import com.epam.calendar.dao.IEventDAO;
import com.epam.calendar.dao.IUserDAO;
import com.epam.calendar.exception.DAOException;
import com.epam.calendar.model.Event;
import com.epam.calendar.model.EventVO;
import com.opensymphony.xwork2.ModelDriven;

import net.sf.json.JSONObject;

public class ShowEventsRestController implements ModelDriven<Object> {
	static Logger logger = Logger.getLogger(ShowEventsRestController.class.getName());
	private IEventDAO eventDAO;
	private IUserDAO userDAO;
	private Object events;
	private String id;
	private int pageCount;

	public HttpHeaders show() {
		try {
			JSONObject obj = new JSONObject();

			int eventSize = eventDAO.countEvents();
			pageCount = eventSize / eventsOnPage + (eventSize % eventsOnPage == 0 ? 0 : 1);
			List<Event> list = eventDAO.readEvents(eventsOnPage, Integer.parseInt(getId()));
			List<EventVO> listVO = new ArrayList<>();
			for (Event event : list) {
				event.setUser(userDAO.read(event.getIdUser()));
				listVO.add(new EventVO(event.getIdEvent(), event.getTitle(), event.getDate().toString(),
						event.getTimeFrom().toString(), event.getTimeTo().toString(), event.getPlace(), 
						event.getDescription(), event.getIdUser(), event.getUser()));
			}	
			obj.put("events", listVO);
			obj.put("pageCount", pageCount);
			events = obj;
		} catch (DAOException e) {
			logger.error(e);
		}
		return new DefaultHttpHeaders("show");
	}

	public Object getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public IEventDAO getEventDAO() {
		return eventDAO;
	}

	public void setEventDAO(IEventDAO eventDAO) {
		this.eventDAO = eventDAO;
	}

	public IUserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(IUserDAO userDAO) {
		this.userDAO = userDAO;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public Object getModel() {
		if (events == null) {
			return events;
		} else {
			return events;
		}
	}

}
