package com.epam.calendar.actions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.epam.calendar.dao.ICommentDAO;
import com.epam.calendar.dao.IEventDAO;
import com.epam.calendar.dao.IUserDAO;
import com.epam.calendar.exception.DAOException;
import com.epam.calendar.model.Comment;
import com.epam.calendar.model.Event;
import com.opensymphony.xwork2.ActionSupport;

public class EventAction extends ActionSupport implements SessionAware {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -8048784484520746711L;
    static Logger logger = Logger.getLogger(EventAction.class.getName());
    private Map<String, Object> sessionMap;
    private IEventDAO eventDAO;
    private IUserDAO userDAO;
    private ICommentDAO commentDAO;
    private List<Event> events = new ArrayList<Event>();
    private Event event;
    private List<Comment> comments;
    private Long idEvent;
    private String title;
    private String description;
    private String place;
    private String date;
    private Time timeFrom;
    private Time timeTo;

    private static final String DELIMITER = ";";
    private static final String NEW_LINE_SEPARATOR = "\n";
    private static final String FILE_HEADER = "idEvent; title; date; timeFrom; timeTo; place; description; idUser";

    public String openEvent() {
	try {
	    event = eventDAO.read(idEvent);
	    comments = commentDAO.readCommentsByEventID(idEvent);
	    for (Comment comment : comments) {
		comment.setUser(userDAO.read(comment.getIdUser()));
	    }
	} catch (DAOException e) {
	    logger.error(e);
	}
	return SUCCESS;
    }

    public String openAddEventForm() {
	return SUCCESS;
    }

    public String openUpdateEventForm() {
	if (!idEvent.equals(null)) {
	    try {
		event = eventDAO.read(idEvent);
	    } catch (DAOException e) {
		logger.error(e);
	    }
	}
	return SUCCESS;
    }

    public String addEvent() {
	Event event = new Event();
	DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	Date dateFormated = null;
	try {
	    dateFormated = new Date(format.parse(date).getTime());
	} catch (ParseException e1) {
	    logger.error(e1);
	}
	event.setTitle(title);
	event.setDescription(description);
	event.setPlace(place);
	event.setDate(dateFormated);
	event.setTimeFrom(timeFrom);
	event.setTimeTo(timeTo);
	event.setIdUser((Long) sessionMap.get("userID"));
	try {
	    eventDAO.create(event);
	} catch (DAOException e) {
	    logger.error(e);
	}
	return SUCCESS;
    }

    public String updateEvent() {
	DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	Date dateFormated = null;
	try {
	    dateFormated = new Date(format.parse(date).getTime());
	} catch (ParseException e) {
	    logger.error(e);
	}
	Event event = new Event();
	event.setIdEvent(idEvent);
	event.setTitle(title);
	event.setDescription(description);
	event.setPlace(place);
	event.setDate(dateFormated);
	event.setTimeFrom(timeFrom);
	event.setTimeTo(timeTo);
	event.setIdUser((Long) sessionMap.get("userID"));
	try {
	    eventDAO.update(event);
	} catch (DAOException e) {
	    logger.error(e);
	}
	return SUCCESS;
    }

    public String deleteEvent() {
	try {
	    eventDAO.delete(idEvent);
	} catch (DAOException e) {
	    logger.error(e);
	}
	return SUCCESS;
    }

    public String exportEventsToCSV() {
	String fileName = "D:\\events.csv";
	File file = new File(fileName);
	List<Event> events = new ArrayList<>();
	FileWriter fileWriter = null;
	try {
	    events = eventDAO.readEvents(eventDAO.countEvents(), 1);
	} catch (DAOException e) {
	    logger.error(e);
	}
	try {
	    file.createNewFile();
	    fileWriter = new FileWriter(fileName);
	    fileWriter.append(FILE_HEADER.toString());
	    fileWriter.append(NEW_LINE_SEPARATOR);
	    for (Event event : events) {
		fileWriter.append(String.valueOf(event.getIdEvent()));
		fileWriter.append(DELIMITER);
		fileWriter.append(event.getTitle());
		fileWriter.append(DELIMITER);
		fileWriter.append(String.valueOf(event.getDate()));
		fileWriter.append(DELIMITER);
		fileWriter.append(String.valueOf(event.getTimeFrom()));
		fileWriter.append(DELIMITER);
		fileWriter.append(String.valueOf(event.getTimeTo()));
		fileWriter.append(DELIMITER);
		fileWriter.append(event.getPlace());
		fileWriter.append(DELIMITER);
		fileWriter.append(event.getDescription());
		fileWriter.append(DELIMITER);
		fileWriter.append(String.valueOf(event.getIdUser()));
		fileWriter.append(NEW_LINE_SEPARATOR);
	    }
	} catch (IOException e) {
	    logger.error(e);
	} finally {
	    try {
		fileWriter.flush();
		fileWriter.close();
	    } catch (IOException e) {
		logger.error(e);
	    }
	}
	return SUCCESS;
    }

    public String exportEventToCSV() {
	String fileName = "D:\\event.csv";
	File file = new File(fileName);
	Event event = new Event();
	FileWriter fileWriter = null;
	try {
	    event = eventDAO.read(idEvent);
	} catch (DAOException e) {
	    logger.error(e);
	}
	try {
	    file.createNewFile();
	    fileWriter = new FileWriter(fileName);
	    fileWriter.append(FILE_HEADER.toString());
	    fileWriter.append(NEW_LINE_SEPARATOR);
	    fileWriter.append(String.valueOf(event.getIdEvent()));
	    fileWriter.append(DELIMITER);
	    fileWriter.append(event.getTitle());
	    fileWriter.append(DELIMITER);
	    fileWriter.append(String.valueOf(event.getDate()));
	    fileWriter.append(DELIMITER);
	    fileWriter.append(String.valueOf(event.getTimeFrom()));
	    fileWriter.append(DELIMITER);
	    fileWriter.append(String.valueOf(event.getTimeTo()));
	    fileWriter.append(DELIMITER);
	    fileWriter.append(event.getPlace());
	    fileWriter.append(DELIMITER);
	    fileWriter.append(event.getDescription());
	    fileWriter.append(DELIMITER);
	    fileWriter.append(String.valueOf(event.getIdUser()));
	    fileWriter.append(NEW_LINE_SEPARATOR);
	} catch (IOException e) {
	    logger.error(e);
	} finally {
	    try {
		fileWriter.flush();
		fileWriter.close();
	    } catch (IOException e) {
		logger.error(e);
	    }
	}

	return SUCCESS;
    }

    public void setUserDAO(IUserDAO userDAO) {
	this.userDAO = userDAO;
    }

    public void setEventDAO(IEventDAO eventDAO) {
	this.eventDAO = eventDAO;
    }

    public void setCommentDAO(ICommentDAO commentDAO) {
	this.commentDAO = commentDAO;
    }

    public Long getIdEvent() {
	return idEvent;
    }

    public void setIdEvent(Long idEvent) {
	this.idEvent = idEvent;
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public String getPlace() {
	return place;
    }

    public void setPlace(String place) {
	this.place = place;
    }

    public Time getTimeFrom() {
	return timeFrom;
    }

    public void setTimeFrom(Time timeFrom) {
	this.timeFrom = timeFrom;
    }

    public Time getTimeTo() {
	return timeTo;
    }

    public void setTimeTo(Time timeTo) {
	this.timeTo = timeTo;
    }

    public String getDate() {
	return date;
    }

    public void setDate(String date) {
	this.date = date;
    }

    public Event getEvent() {
	return event;
    }

    public void setEvent(Event event) {
	this.event = event;
    }

    public List<Comment> getComments() {
	return comments;
    }

    public void setComments(List<Comment> comments) {
	this.comments = comments;
    }

    public List<Event> getEvents() {
	return events;
    }

    public void setEvents(List<Event> events) {
	this.events = events;
    }

    @Override
    public void setSession(Map<String, Object> sessionMap) {
	this.sessionMap = sessionMap;
    }

}
