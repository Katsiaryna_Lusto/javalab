package com.epam.calendar.actions;

import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.epam.calendar.dao.IUserDAO;
import com.epam.calendar.model.User;
import com.opensymphony.xwork2.ActionSupport;

public class AuthenticationAction extends ActionSupport implements SessionAware {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 6636231049550720617L;
    static Logger logger = Logger.getLogger(AuthenticationAction.class.getName());
    private IUserDAO userDAO;
    private String name;
    private Map<String, Object> sessionMap;

    public void setUserDAO(IUserDAO userDAO) {
	this.userDAO = userDAO;
    }

    public IUserDAO getUserDAO() {
	return userDAO;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String login() {
	User user = null;
	try {
	    if (name != null) {
		user = userDAO.readByName(name);
	    }
	} catch (Exception e) {
	    logger.error(e);
	}

	if (user != null) {
	    sessionMap.put("userName", user.getName());
	    sessionMap.put("userID", user.getIdUser());
	    return SUCCESS;
	}
	return INPUT;
    }

    public String logout() {
	if (sessionMap.containsKey("userID") && sessionMap.containsKey("userName")) {
	    sessionMap.remove("userID");
	    sessionMap.remove("userName");
	}
	return SUCCESS;
    }

    public void setSession(Map<String, Object> sessionMap) {
	this.sessionMap = sessionMap;
    }
}