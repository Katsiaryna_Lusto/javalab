package com.epam.calendar.actions;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.epam.calendar.dao.ICommentDAO;
import com.epam.calendar.dao.IUserDAO;
import com.epam.calendar.exception.DAOException;
import com.epam.calendar.model.Comment;
import com.opensymphony.xwork2.ActionSupport;

public class CommentAction extends ActionSupport implements SessionAware {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 7369938501053994447L;
    static Logger logger = Logger.getLogger(CommentAction.class.getName());
    private Map<String, Object> sessionMap;
    private ICommentDAO commentDAO;
    private IUserDAO userDAO;
    private String text;
    private Long idEvent;
    private Long idDeleteComment;
    private Long idUpdateComment;
    private Comment comment = new Comment();

    /**
     * public String addComment() { Comment comment = new Comment(); Long
     * idUser; if (!text.equals(null)) { idUser = (Long)
     * sessionMap.get("userID"); comment.setIdEvent(idEvent);
     * comment.setText(text); comment.setIdUser(idUser); Timestamp dateTime =
     * new Timestamp((new Date()).getTime()); comment.setDateTime(dateTime); try
     * { commentDAO.create(comment); } catch (DAOException e) { logger.error(e);
     * } } return SUCCESS; }
     **/

    public String addJsonComment() {
	Long idUser;
	Long idComment;
	if (!text.equals(null)) {
	    idUser = (Long) sessionMap.get("userID");
	    comment.setIdEvent(idEvent);
	    comment.setText(text);
	    comment.setIdUser(idUser);
	    Timestamp dateTime = new Timestamp((new Date()).getTime());
	    comment.setDateTime(dateTime);
	    try {
		idComment = commentDAO.create(comment);
		comment.setIdComment(idComment);
		comment.setUser(userDAO.read(idUser));
	    } catch (DAOException e) {
		logger.error(e);
	    }
	}
	return SUCCESS;
    }

    public String deleteComment() {
	if (!idDeleteComment.equals(null)) {
	    try {
		commentDAO.delete(idDeleteComment);
	    } catch (DAOException e) {
		logger.error(e);
	    }
	}
	return SUCCESS;
    }

    public String updateComment() {
	if (!idUpdateComment.equals(null)) {
	    try {
		comment = commentDAO.read(idUpdateComment);
		comment.setText(text);
		commentDAO.update(comment);
	    } catch (DAOException e) {
		logger.error(e);
	    }
	}
	return SUCCESS;
    }

    public IUserDAO getUserDAO() {
	return userDAO;
    }

    public void setUserDAO(IUserDAO userDAO) {
	this.userDAO = userDAO;
    }

    public void setCommentDAO(ICommentDAO commentDAO) {
	this.commentDAO = commentDAO;
    }

    public String getText() {
	return text;
    }

    public void setText(String text) {
	this.text = text;
    }

    public Long getIdEvent() {
	return idEvent;
    }

    public void setIdEvent(Long idEvent) {
	this.idEvent = idEvent;
    }

    public Long getIdDeleteComment() {
	return idDeleteComment;
    }

    public void setIdDeleteComment(Long idDeleteComment) {
	this.idDeleteComment = idDeleteComment;
    }

    public Long getIdUpdateComment() {
	return idUpdateComment;
    }

    public void setIdUpdateComment(Long idUpdateComment) {
	this.idUpdateComment = idUpdateComment;
    }

    public Comment getComment() {
	return comment;
    }

    public void setComment(Comment comment) {
	this.comment = comment;
    }

    @Override
    public void setSession(Map<String, Object> sessionMap) {
	this.sessionMap = sessionMap;
    }

}
