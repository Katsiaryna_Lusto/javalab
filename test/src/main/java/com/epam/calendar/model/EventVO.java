package com.epam.calendar.model;

public class EventVO {

	  /**
   * Event ID
   */
	private Long idEvent;
	  /**
   * Event title
   */
	private String title;
	  /**
   * Event date
   */
	private String date;
	  /**
   * Time of beginning of an event
   */

	private String timeFrom;	
	  /**
   * Time of ending of an event
   */

	private String timeTo;
	  /**
   * Event place
   */
	private String place;
	  /**
   * Event description
   */
	private String description;
	  /**
   * User ID
   */
	private Long idUser;
	  /**
   * User
   */
	private User user;
	
	public EventVO(Long idEvent, String title, String date, String timeFrom, String timeTo, String place,
			String description, Long idUser, User user) {
		super();
		this.idEvent = idEvent;
		this.title = title;
		this.date = date;
		this.timeFrom = timeFrom;
		this.timeTo = timeTo;
		this.place = place;
		this.description = description;
		this.idUser = idUser;
		this.user = user;
	}
	public Long getIdEvent() {
		return idEvent;
	}
	public void setIdEvent(Long idEvent) {
		this.idEvent = idEvent;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTimeFrom() {
		return timeFrom;
	}
	public void setTimeFrom(String timeFrom) {
		this.timeFrom = timeFrom;
	}
	public String getTimeTo() {
		return timeTo;
	}
	public void setTimeTo(String timeTo) {
		this.timeTo = timeTo;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getIdUser() {
		return idUser;
	}
	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
}
