$(document)
		.ready(
				function() {
					$("#updateSubmit")
							.click(
									function() {
										var title = $("#title").val();
										var description = $("#description")
												.val();
										var place = $("#place").val();
										var date = $("#date").val();
										var timeFrom = $("#timeFrom").val();
										var timeTo = $("#timeTo").val();
										var errorMsg = "";
										var counter=0;
										
										// Check title
										if (title.length == 0 || title.length > 50) {
											errorMsg = "Title is requared,it must be < 50 symbols";
											$('#validationTitle').text(errorMsg);
											$(".updateTitle").addClass("error");
											counter++;
										} else {
											if ($(".updateTitle").hasClass("error"))
												$(".updateTitle").removeClass("error");
										}
										
										// Check description
										if (description.length == 0
												|| description.length > 65536) {
											errorMsg = "Description is requared, it must be < 65536 symbols";
											$('#validationDescription').text(errorMsg);
											$(".updateDescription").addClass("error");
											counter++;
										} else {
											if ($(".updateDescription").hasClass("error"))
												$(".updateDescription").removeClass("error");
										}
										
										// Check place
										if (place.length > 50) {
											errorMsg = "Place must be < 50 symbols";
											$('#validationPlace').text(errorMsg);
											$(".updatePlace").addClass("error");
											counter++;
										} else {
											if ($(".updatePlace").hasClass("error"))
												$(".updatePlace").removeClass("error");
										}
										// Check date
										if ((errorMsg = checkDate(date)) != "") {
											$('#validationDate').text(errorMsg);
											$(".updateDate").addClass("error");
											counter++;
										} else if ((errorMsg = checkIfDateAfterToday(date)) != "") {
											$('#validationDate').text(errorMsg);
											$(".updateDate").addClass("error");
											counter++;
										} else {
											if ($(".updateDate").hasClass("error"))
												$(".updateDate").removeClass("error");
										}
										
										// Check timeFrom
											if ((errorMsg = checkTime(timeFrom)) != "") {
											$('#validationTimeFrom').text(errorMsg);
											$(".updateTimeFrom").addClass("error");
											counter++;
										} else {
											if ($(".updateTimeFrom").hasClass("error"))
												$(".updateTimeFrom").removeClass("error");
										}
										
										// Check timeTo
										if ((errorMsg = checkTime(timeTo)) != "") {
											$('#validationTimeTo').text(errorMsg);
											$(".updateTimeTo").addClass("error");
											counter++;
										} else if ((errorMsg = checkIfTimeFromAfterTimeTo(timeFrom,timeTo)) != "") {
											$('#validationTimeTo').text(errorMsg);
											$(".updateTimeTo").addClass("error");
											counter++;
										} else {
											if ($(".updateTimeTo").hasClass("error"))
												$(".updateTimeTo").removeClass("error");
										}
										if (counter != 0) {
											return false;
										}
									});

					function checkDate(field) {
						var minYear = (new Date()).getFullYear();
						var msg = "";

						re = /^([0-9]{4})-([0-9]{2})-([0-9]{2})$/;

						if (field != '') {
							var regs = field.match(re);
							if (regs) {
								if (regs[3] < 1 || regs[3] > 31) {
									msg = "Day must be from 1 to 31!";
								} else if (regs[2] < 1 || regs[2] > 12) {
									errorMsg = "Month must be from 1 to 12!";
								} else if (regs[1] < minYear) {
									msg = "The year must be >= " + minYear;
								}
							} else {
								msg = "Wrong date format: " + field;
							}
						} else if (field == '')
							msg = "Type a date";
						return msg;
					}

					function checkIfDateAfterToday(field) {
						var today = new Date();
						var date = new Date(field);
						var ddDate = date.getDate();
						var mmDate = date.getMonth() + 1;
						var yyyyDate = date.getFullYear();
						var dd = today.getDate();
						var mm = today.getMonth() + 1;
						var yyyy = today.getFullYear();
						var msg = "Date must be after today";
						if (yyyyDate == yyyy) {
							if (mmDate < mm) {
								return msg;
							} else if (mmDate == mm) {
								if (ddDate < dd) {
									return msg;
								}
							}
						} else if (yyyyDate < yyyy) {
							return msg;
						}
						return "";
					}

					function checkTime(checkTime) {
						msg = "";
						var time;
						var hours;
						var minutes;
						var seconds;
						if (checkTime != '') {
							time = new Date("1/1/1900 " + checkTime);
							if (isNaN(time.getHours())
									|| isNaN(time.getMinutes())
									|| isNaN(time.getSeconds())) {
								msg = "Time is wrong!"
							}
						} else if (checkTime == '') {
							msg = "Type time";
						}
						return msg;
					}

					function checkIfTimeFromAfterTimeTo(timeFrom, timeTo) {
						time1 = new Date("1/1/1900 " + timeFrom);
						time2 = new Date("1/1/1900 " + timeTo);
						msg = "Time from must be < time to";
						if (time1.getHours() > time2.getHours()) {
							return msg;
						} 
						if (time1.getHours() == time2.getHours()) {
							if (time1.getMinutes() > time2.getMinutes()) {
								return msg;
							}
							if (time1.getMinutes() == time2.getMinutes()) {
								if (time1.getSeconds() >= time2.getSeconds()) {
									return msg;
								}
							}
						}
						return "";

					}

				});