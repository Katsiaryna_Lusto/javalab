 var comment = {info: []};
function addCommentAjax() {
                var text = $('#text').val();
                var idEvent = $('#idEvent').val();
                $.ajax({
                    type: "POST",
                    url: "http://localhost:8080/test/addJsonComment.action",
                    data: { text: text, idEvent: idEvent},
                    dataType: 'json',
                    success: function(response)
                    {
                    	if(response.comment.text!=null){
                        	var tmpl = $.templates("{^{for info}}" +
                        			"<div class='commentbox'>" +
                        				"<img class='imgCommentList' data-link='userImgUrl' src='{{:userImgUrl}}'>" +
                        				"<div class='commentHeader'>Posted by {^{:userName}} on {^{:commentDateTime}}</div>"+
                        				"<div class='commentText'>{^{:commentText}}</div>"                     				
                        				+
            							"<form name='deleteComment' action='deleteComment' method='post'>" +
            								"<input type='hidden' data-link='commentId' name='idDeleteComment'/>" +
            								"<input type='submit' class='submit deleteSubmit' value='delete comment' data-comment-id='{{:commentId}}' style='display: inline;'/>" +
            							'</form>' +
            						  	"<div class='updateCommentHiddenBlock' style='display: none;' data-link='commentId' data-comment-id='{{:commentId}}'>" +
            								"<form name='updateComment' action='updateComment' method='post'>" +
            									"<input type='hidden' data-link='commentId' value='{{:commentId}}'' name='idUpdateComment'/>" +
            									"<textarea class='textarea addCommentTextarea' name='text'>{^{:commentText}}</textarea><br/>" +
            									"<input type='submit' class='submit updateSubmit'  value='update comment' />" +
            									"<input type='button' onclick = 'javascript:location.href='openEvent.action';' class='submit' value='cancel'>" +
            				  				"</form>" +
            							"</div>"+
            							"<input type='submit' class='submit showUpdateFormSubmit' data-comment-id='{{:commentId}}' value='update comment' style='display: inline;'/>" +	
                        			"</div>" +
                        		"{{/for}}<br/>");
                			$.observable(comment.info).insert({
                				commentText: response.comment.text, 
                				commentId:response.comment.idComment,
                				userImgUrl:response.comment.user.imgUrl,
                				userName:response.comment.user.name,
                				userId:response.comment.user.idUser,
                				commentDateTime:$.format.date(response.comment.dateTime, "yyyy-MM-dd HH:mm:ss"),
                				})
                			tmpl.link("#placeholder",comment);
                    	}
                    }
                });
            }
  
 
  /**function addCommentAjax(){
      var text = $('#text').val();
      var idEvent = $('#idEvent').val();
      var body = 'text=' + encodeURIComponent(text) +
      '&idEvent=' + encodeURIComponent(idEvent);

	  var xhr = new XMLHttpRequest();
	  xhr.open('POST', 'http://localhost:8080/test/addComment.action', true);
	  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	  xhr.send(body);

	  if (xhr.status != 200) {
		  location.reload(true);
	  }
	  
  } **/
 