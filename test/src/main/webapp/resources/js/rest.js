 	function showEvents(pageNumber) {
 		$.ajax({
		type: 'GET',
		url: "http://localhost:8080/test/api/events/"+pageNumber,
		dataType: "json",
		success: function (data) {
			$( ".event-list" ).empty();
			$( ".paginationDiv" ).empty();
		    		$.each(data.events, function(index, event) {
		    			$('.event-list').append(
		    				"<li>" +
		    					"<div class='userInfo'>" +
			    					"<img class='imgEventList' src='"+event.user.imgUrl+"' />" +
			    					event.user.name+
			    					"<br/>" +
		    					"</div>" +
		    					"<div class='dateTime'>" +
		    					event.timeFrom+"-"+event.timeTo+"<br/>"+event.date+
		    					"</div>" +
		    					"<div class='info'>" +
			    					"<form name='openEvent' action='openEvent' method='post'>" +
				    					"<input type='hidden' value="+event.idEvent+" name='idEvent' />" +
				    					"<h2 class='title'>"+event.title+"</h2>"+
				    					"<p class='place'>"+event.place+"</p>"+
				    					"<input type='submit' class='submit'  value='view more'></input>"+
			    					"</form>" +
		    					"</div>"+					    					
		    					"<form name='exportEventToCSV' action='exportEventToCSV' method='post'>" +
		    					"<input type='hidden' value="+event.idEvent+" name='idEvent'/>" +
		    					"<input type='submit' class='submit' value='export event to CSV file' />" +
		    				"</form>"+		
		    					"<div class='deleteUpdateEventSubmit'>" +
			    					"<c:if test='${sessionScope.userID}=="+event.user.idUser+"'>" +
				    					"<form name='deleteEvent' action='deleteEvent' method='post'>" +
					    					"<input type='hidden' value="+event.idEvent+" name='idEvent'/>" +
					    					"<input type='submit' class='submit' value='delete event' />" +
					    				"</form><br/>"+
			    					"<form name='openUpdateEventForm' action='openUpdateEventForm' method='post'>" +
			    					"<input type='hidden' value="+event.idEvent+" name='idEvent'/>" +
			    					"<input type='submit' class='submit' value='update event' />" +
			    					"</form>" +
			    					"</c:if>" +
		    					"</div>"+
		    				"</li><br/>");
		    		})
		    		if(data.pageCount!=1){
		    			$.each([1, data.pageCount], function( index, value ) {
		    				$('.paginationDiv').append(
		    					"<form name='showEvents' action='JavaScript:showEvents("+value+")' method='post' class='pagination'>" +
		    	    				"<input type='hidden' name='pageNumber' class='pageNumber' value='"+value+"'>" +
		    	    				"<input type='submit' value='"+value+"'/> " +
		    					"</form>");
		    			});
		    		}
		     }
	})
	}