<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="resources/css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="resources/js/jquery-2.2.2.min.js"></script>
<script type="text/javascript"
	src="resources/js/updateEventValidation.js"></script>
</head>
<body>
	<%@ include file="Header.jsp"%>
	<div class="Logout">
		<input type="button"
			onclick="javascript:location.href='login.action';" class="submit"
			value="Back">
	</div>
	<div class="eventInfo">
		<h3>Enter event information:</h3>
		<s:form name="updateEvent" action="updateEvent" method="post">
			<div class="updateTitle">
				Title:<br /> <input type="text" id="title" name="title"
					value="${event.title}"> <label id="validationTitle"
					class="validationLabel"></label>
			</div>
			<div class="updateDescription">
				Description:<br />
				<textarea id="description" name="description"
					class="textarea eventDescriptionTextarea">${event.description}</textarea>
				<label id="validationDescription" class="validationLabel"></label>
			</div>
			<div class="updatePlace">
				Place:<br /> <input type="text" id="place" name="place"
					value="${event.place}" /> <label id="validationPlace"
					class="validationLabel"></label>
			</div>
			<div class="updateDate">
				Date:<br /> <input type="text" id="date" name="date"
					placeholder="yyyy-mm-dd" value="${event.date}" /> <label
					id="validationDate" class="validationLabel"></label>
			</div>
			<div class="updateTimeFrom">
				Time from:<br /> <input type="text" id="timeFrom" name="timeFrom"
					placeholder="00:00:00" value="${event.timeFrom}" /> <label
					id="validationTimeFrom" class="validationLabel"></label>
			</div>
			<div class="updateTimeTo">
				Time to:<br /> <input type="text" id="timeTo" name="timeTo"
					placeholder="00:00:00" value="${event.timeTo}" /> <label
					id="validationTimeTo" class="label"></label>
			</div>
			<s:submit cssClass="submit" value="Update" id="updateSubmit" />
		</s:form>
	</div>
</body>
</html>