<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome</title>
<script type="text/javascript" src="resources/js/jquery-2.2.2.min.js"></script>
<script type="text/javascript" src="resources/js/rest.js"></script>
<script type="text/javascript" src="resources/js/exportCSV.js"></script>
<link href="resources/css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
	showEvents(1);
</script>
</head>
<body class="welcome">
	<%@ include file="Header.jsp"%>
	<input type="button" onclick="javascript:location.href='openAddEventForm.action';" class="submit" value="add event">
	<input type="button" onclick="javascript:location.href='exportEventsToCSV.action';" class="submit" value="export all events to CSV file">
	<div id="events">
		<ul class="event-list"></ul>
	</div>
	<div class="paginationDiv"></div>


	<!--  
		<div class="events">
		<c:if test="${events != null}">	
			<c:forEach items="${events}" var="event">
				<ul class="event-list">
					<li>
						<div class="userInfo">
							<img class="imgEventList" src="${event.user.imgUrl}"/>
							${event.user.name}<br/>
						</div>
						
						<div class="dateTime">${event.timeFrom}-${event.timeTo}<br/>${event.date}</div>
						
						<div class="info">
							<s:form name="openEvent" action="openEvent" method="post">
								<input type="hidden" value="${event.idEvent}" name="idEvent" />
								<h2 class="title">${event.title}</h2>
								<p class="place">${event.place}</p>
								<input type="submit" class="submit"  value="view more"></input>
							</s:form>
						</div>
						
						<div class="deleteUpdateEventSubmit">
							<c:if test="${sessionScope.userID==event.user.idUser}">
								<s:form name="deleteEvent" action="deleteEvent" method="post">
									<input type="hidden" value="${event.idEvent}" name="idEvent"/>
									<input type="submit" class="submit" value="delete event" />
								</s:form><br/>
								<s:form name="openUpdateEventForm" action="openUpdateEventForm" method="post">
									<input type="hidden" value="${event.idEvent}" name="idEvent"/>
									<input type="submit" class="submit" value="update event" />
								</s:form>
							</c:if>
						</div>
					</li>
				</ul>
			</c:forEach>
		</c:if>
		</div>
		<div class="paginationDiv">
		<c:if test="${pageCount!=1}">
				<c:forEach var="i" begin="1" end="${pageCount}">
						<s:form name="showEvents" action="showEvents" method="post" cssClass="pagination">
							<input type="hidden" name="pageNumber" class="pageNumber" value="${i}">
							<input type="submit" value="${i}" />
						</s:form>
				</c:forEach>
			</c:if>
		</div>-->

</body>
</html>