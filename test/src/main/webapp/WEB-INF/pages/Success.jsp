<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Success</title>
<link href="resources/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<%@ include file="Header.jsp"%>
	<div class="Logout">
		<input type="button"
			onclick="javascript:location.href='login.action';" class="submit"
			value="Back">
	</div>
	<s:form action="login" method="post" cssClass="login">
		<h1 align="center">Export to CSV file completed successfully!</h1>
	</s:form>
</body>
</html>