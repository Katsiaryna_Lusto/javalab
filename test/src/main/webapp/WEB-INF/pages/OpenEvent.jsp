<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
	
	<link href="resources/css/style.css" rel="stylesheet" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Welcome</title>
    <script type="text/javascript" src="resources/js/jquery-2.2.2.min.js"></script>
	<script type="text/javascript" src="resources/js/jsviews.js"></script>
	<script type="text/javascript" src="resources/js/jquery-dateFormat.js"></script>
	<script type="text/javascript" src="resources/js/updateCommentForm.js"></script>
	<script type="text/javascript" src="resources/js/addingComment.js"></script>
</head>
<body>
		<%@ include file="Header.jsp" %>
		<div class="Logout">
			<input type="button" onclick = "javascript:location.href='login.action';" class="submit" value="Back">
		</div>
		<div class="openEvent">
			<h2>${event.title}</h2>
			<h3>${event.date}<br/>
			${event.timeFrom}-${event.timeTo}<br/>
			${event.place}</h3>
			<div class="eventDescription">
				${event.description}
			</div><br/>
			<div id="comments">
				<c:forEach items="${comments}" var="comment" >
					<div class="commentbox">
						<img class="imgCommentList" src=" ${comment.user.imgUrl}">
						<div class="commentHeader">Posted by ${comment.user.name} on ${comment.dateTime}</div>
						<div class="commentText">${comment.text}</div>
						
						<c:if test="${sessionScope.userID==comment.user.idUser}">
							<s:form name="deleteComment" action="deleteComment" method="post">
								<input type="hidden" value="${comment.idComment}" name="idDeleteComment"/>
								<input type="submit" class="submit deleteSubmit" value="delete comment" data-comment-id="${comment.idComment}" style="display: inline;"/>
							</s:form>
								
						  	<div class="updateCommentHiddenBlock" style="display: none;" data-comment-id="${comment.idComment}">	
								<s:form name="updateComment" action="updateComment" method="post">
									<input type="hidden" value="${comment.idComment}" name="idUpdateComment"/>
									<textarea class="textarea addCommentTextarea" name="text" >${comment.text}</textarea><br/>
					  				<input type="submit" class="submit updateSubmit"  value="update comment" />
					  				<input type="button" onclick = "javascript:location.href='openEvent.action';" class="submit" value="cancel">
				  				</s:form>
							</div>		
										
							<input type="submit" class="submit showUpdateFormSubmit" data-comment-id="${comment.idComment}" value="update comment" style="display: inline;"/>		
						</c:if>
					</div><br/>
				</c:forEach>
				<div id="placeholder"></div>				
			</div>
				
			<!--
			<s:form name="addComment" action="addComment" method="post">
				<textarea name="text" class="textarea addCommentTextarea"></textarea><br/>
				<input type="hidden" value="${event.idEvent}" name="idEvent"/><br/>
	  			<input type="submit" class="submit" value="add comment" />  	
			</s:form>
			-->
			
				<textarea name="text" required id="text" class="textarea addCommentTextarea"></textarea><br/>				
				<input type="hidden" id="idEvent" value="${event.idEvent}" name="idEvent"/><br/>
				<input type="submit" class="submit" value="add comment" onclick="addCommentAjax()"/>
		</div>
</body>
</html>