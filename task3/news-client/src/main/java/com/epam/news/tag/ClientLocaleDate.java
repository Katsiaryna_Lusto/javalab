package com.epam.news.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ClientLocaleDate extends TagSupport {
	private Date date;

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public int doStartTag() throws JspException {

		Locale locale = pageContext.getResponse().getLocale();
		SimpleDateFormat dateFormat = null;
		if ("en".equals(locale.getLanguage())) {
			dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		} else {
			dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		}
		try {
			pageContext.getOut().write(dateFormat.format(date));
		} catch (IOException e) {
			throw new JspException(e);
		}
		return SKIP_BODY;
	}
}
