function switchEnableAuthorInput(inputId) {
        document.getElementById("text"+inputId).disabled = false;
        document.getElementById("edit"+inputId).style.visibility = "hidden";
        document.getElementById("update"+inputId).style.visibility = "visible";
        document.getElementById("expire"+inputId).style.visibility = "visible";
}

function switchEnableTagInput(inputId) {
    document.getElementById("text"+inputId).disabled = false;
    document.getElementById("edit"+inputId).style.visibility = "hidden";
    document.getElementById("update"+inputId).style.visibility = "visible";
    document.getElementById("delete"+inputId).style.visibility = "visible";
}

function updateFormLink(inputId){
    document.forms["updateForm"+inputId].submit();
    return false;
}


