<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<div id="content">
	<div class="filters-div">
		<form action="/news-client/news-filter" class="filter">
			<select name="author">
				<c:if test="${authorId != null}">
					<option value="-1"><fmt:message
							key="main.message.select.author" /></option>
				</c:if>
				<c:if test="${authorId == null}">
					<option selected="selected" value="-1"><fmt:message
							key="main.message.select.author" /></option>
				</c:if>
				<c:forEach items="${authorList}" var="author">
					<c:if test="${authorId != author.id}">
						<option value="<c:out value="${author.id}"/>"><c:out
								value="${author.name}" /></option>
					</c:if>
					<c:if test="${authorId == author.id}">
						<option selected="selected" value="<c:out value="${author.id}"/>"><c:out
								value="${author.name}" /></option>
					</c:if>
				</c:forEach>
			</select> <select name="tags" id="multi-dropbox" class="multi-dropbox"
				multiple="multiple">
				<c:forEach items="${tagsList}" var="tag">
					<c:set var="contains" value="false" />
					<c:forEach var="item" items="${tagIds}">
						<c:if test="${item == tag.id}">
							<c:set var="contains" value="true" />
						</c:if>
					</c:forEach>
					<c:if test="${contains == true}">
						<option selected="selected" value="${tag.id}">${tag.name}</option>
					</c:if>
					<c:if test="${contains != true}">
						<option value="${tag.id}">${tag.name}</option>
					</c:if>
				</c:forEach>
				<input type="submit" value="<fmt:message key="main.button.filter"/>" />
				<input type="hidden" name="fromFilter" value="true" />
			</select>
		</form>
		<form action="/news-client/reset-filter" class="filter">
			<input type="submit" value="<fmt:message key="main.button.reset"/>" />
		</form>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".multi-dropbox").dropdownchecklist();
		});
	</script>
	<c:if test="${newsVOList == null}">
		<fmt:message key="main.message.no.news" />
	</c:if>
	<c:if test="${newsVOList != null}">

		<c:forEach items="${newsVOList}" var="newsVO" varStatus="iter">
			<div class="title-author-date">
				<div class="news-title">${newsVO.news.title}</div>
				<div class="news-author-date">
					(
					<fmt:message key="main.message.by" />
					${newsVO.author.name}) <span class="news-modification-date"><ctg:dateFormat
							date="${newsVO.news.modificationDate}" /></span>
				</div>
			</div>
			<div class="news-short-text">${newsVO.news.shortText}</div>
			<div class="tags-comment-view">
				<c:forEach items="${newsVO.tags}" var="tag" varStatus="i">
					<div class="tag">${tag.name}</div>
				</c:forEach>
				<div class="comments-number">
					<fmt:message key="main.message.comments" />
					(${fn:length(newsVO.comments)})
				</div>
				<a class="view-link"
					href="/news-client/single-news/${newsVO.news.id}"><fmt:message
						key="main.link.view" /></a><br />
			</div>

		</c:forEach>

		<div class="main-page-links">
			<c:if test="${pageCount != 1}">
				<c:forEach var="i" begin="1" end="${pageCount}">
					<c:if test="${pageNumber == null || pageNumber == i}">
						<a class="pagination-link current-page-link"
							href="/news-client/news-filter/${i}">${i} </a>
					</c:if>
					<c:if test="${pageNumber != i}">
						<a class="pagination-link" href="/news-client/news-filter/${i}">${i}
						</a>
					</c:if>
				</c:forEach>
			</c:if>
		</div>
	</c:if>
</div>