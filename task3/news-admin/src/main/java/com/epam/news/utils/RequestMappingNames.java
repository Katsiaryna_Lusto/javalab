package com.epam.news.utils;

public class RequestMappingNames {
	public static final String LOGIN = "login";
	public static final String RESET_FILTER = "reset-filter";
	public static final String NEWS_FILTER = "news-filter";
	public static final String NEWS_PAGE_NUMBER_PARAM = "/{pageNumber}";
	public static final String NEWS_FILTER_VIEW = "news-filter-view";
	public static final String SINGLE_NEWS = "single-news/{newsId}";
	public static final String NEWS_SINGLE_VIEW = "single-news-view";
	public static final String DELETE_NEWS = "delete-news";
	public static final String EDIT_NEWS = "edit-news";
	public static final String EDIT_NEWS_LINK = "edit-news/{newsId}";
	public static final String EDIT_NEWS_VIEW = "edit-news-view";

	public static final String ADD_COMMENT = "add-comment";
	public static final String DELETE_COMMENT = "delete-comment";

	public static final String ADD_NEWS_VIEW = "add-news-view";
	public static final String ADD_AUTHOR_VIEW = "add-author-view";
	public static final String ADD_TAG_VIEW = "add-tag-view";
	public static final String ADD_NEWS = "add-news";

	public static final String ADD_AUTHOR = "add-author";
	public static final String UPDATE_AUTHOR = "update-author";
	public static final String EXPIRE_AUTHOR = "expire-author/{authorId}";

	public static final String ADD_TAG = "add-tag";
	public static final String UPDATE_TAG = "update-tag";
	public static final String DELETE_TAG = "delete-tag/{tagId}";

	public static final String ERROR = "error";
	public static final String ACCESS_DENIED = "access-denied";

	public static final String REDIRECT = "redirect:/";

}
