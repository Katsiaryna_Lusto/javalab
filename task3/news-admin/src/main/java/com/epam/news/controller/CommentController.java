package com.epam.news.controller;

import static com.epam.news.utils.RequestMappingNames.ADD_COMMENT;
import static com.epam.news.utils.RequestMappingNames.DELETE_COMMENT;
import static com.epam.news.utils.RequestMappingNames.ERROR;
import static com.epam.news.utils.RequestMappingNames.NEWS_SINGLE_VIEW;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.entity.Comment;
import com.epam.news.entity.NewsVO;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.ICommentService;
import com.epam.news.service.INewsManageService;

/**
 * Class provides actions: - add comment - delete comment
 */
@Controller
public class CommentController {
	private final static String REDIRECT_SINGLE_NEWS = "redirect:/single-news/";
	@Autowired
	ICommentService commentService;
	@Autowired
	INewsManageService newsManageService;

	@RequestMapping(value = ADD_COMMENT)
	public ModelAndView addComment(HttpSession session,
			@RequestParam(value = "commentText", required = false) String commentText,
			@RequestParam(value = "newsId", required = false) Long newsId) throws ServiceException {
		Comment comment = new Comment(0, commentText, new Date(), newsId);
		commentService.addComment(comment);
		NewsVO newsVO = newsManageService.findNewsVO(newsId);
		session.setAttribute("newsVO", newsVO);
		System.out.println(commentText);
		return new ModelAndView(NEWS_SINGLE_VIEW);
	}

	@RequestMapping(value = DELETE_COMMENT)
	public ModelAndView deleteComment(HttpSession session,
			@RequestParam(value = "commentId", required = true) Long commentId,
			@RequestParam(value = "newsId", required = true) Long newsId) throws ServiceException {
		commentService.deleteComment(commentId);
		return new ModelAndView(REDIRECT_SINGLE_NEWS + newsId);
	}

	@ExceptionHandler(ServiceException.class)
	public ModelAndView serviceExceptionHandler() {
		return new ModelAndView(ERROR);
	}

}
