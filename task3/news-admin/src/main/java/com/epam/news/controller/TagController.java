package com.epam.news.controller;

import static com.epam.news.utils.RequestMappingNames.ADD_TAG;
import static com.epam.news.utils.RequestMappingNames.ADD_TAG_VIEW;
import static com.epam.news.utils.RequestMappingNames.DELETE_TAG;
import static com.epam.news.utils.RequestMappingNames.ERROR;
import static com.epam.news.utils.RequestMappingNames.REDIRECT;
import static com.epam.news.utils.RequestMappingNames.UPDATE_TAG;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.INewsService;
import com.epam.news.service.ITagService;

/**
 * Class provides actions: - add tag - edit tag - delete tag
 */
@Controller
public class TagController {
	@Autowired
	private ITagService tagService;
	@Autowired
	private INewsService newsService;

	@RequestMapping(value = ADD_TAG)
	public ModelAndView addTag(HttpSession session, @RequestParam(value = "tagName", required = true) String tagName)
			throws ServiceException {
		Tag tag = new Tag(0, tagName);
		tagService.addTag(tag);
		return new ModelAndView(REDIRECT + ADD_TAG_VIEW);
	}

	@RequestMapping(value = UPDATE_TAG)
	public ModelAndView updateTag(HttpSession session, @RequestParam(value = "tagId", required = true) Long tagId,
			@RequestParam(value = "tagName", required = true) String tagName) throws ServiceException {
		Tag tag = new Tag(tagId, tagName);
		tagService.updateTag(tag);
		return new ModelAndView(REDIRECT + ADD_TAG_VIEW);
	}

	@RequestMapping(value = DELETE_TAG)
	public ModelAndView deleteTag(HttpSession session, @PathVariable Long tagId) throws ServiceException {

		List<Long> tagIds = (List<Long>) session.getAttribute("tagIds");
		if (tagIds != null) {
			if (tagIds.contains(tagId)) {// remove tag from search criteria
				tagIds.remove(tagId);
			}
		}
		session.setAttribute("tagIds", tagIds);
		tagService.deleteTagFromNews(tagId);
		tagService.deleteTag(tagId);
		return new ModelAndView(REDIRECT + ADD_TAG_VIEW);
	}

	@ExceptionHandler(ServiceException.class)
	public ModelAndView serviceExceptionHandler() {
		return new ModelAndView(ERROR);
	}
}
