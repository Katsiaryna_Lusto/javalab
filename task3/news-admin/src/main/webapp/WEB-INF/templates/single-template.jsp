<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<html>
<head>
    <link href="<c:url value='/resources/css/style.css'/>" rel="stylesheet">
    <title>${newsVO.news.title}</title>
    <meta charset="utf-8">
</head>
<body>

<tiles:insertAttribute name="header" />
<tiles:insertAttribute name="login-menu" />
<tiles:insertAttribute name="locale" />
<tiles:insertAttribute name="menu" />
<tiles:insertAttribute name="content"/>
<tiles:insertAttribute name="footer" />
</body>
</html>
