<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<title>Access is denied</title>
</head>
<body>
HTTP Status 403 - Access is denied</br>
<fmt:message key="login.message.hello"/><sec:authentication property="principal.username" /> <fmt:message key="login.message.access.denied"/>

</body>
</html>