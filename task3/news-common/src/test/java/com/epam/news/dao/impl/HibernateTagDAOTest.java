package com.epam.news.dao.impl;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import com.epam.news.dao.hibernate.HibernateTagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * Test C.R.U.D operations from HibernateTagDAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-configuration-test.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, TransactionDbUnitTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class HibernateTagDAOTest {

	private final static String FULL_DB_PATH = "classpath:fullDB.xml";

	@Autowired
	HibernateTagDAO hibernateTagDAO;

	/**
	 * Test find by id
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void findByIdTest() throws DAOException {
		Tag actual = hibernateTagDAO.findById(1L);

		Tag expected = new Tag(1L, "cinema");
		assertEquals(expected, actual);
	}

	/**
	 * Test find all
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void findAllTest() throws DAOException {
		List<Tag> result = hibernateTagDAO.findAll();
		assertEquals(23, result.size());
	}

	/**
	 * Test delete
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void deleteTest() throws DAOException {
		hibernateTagDAO.delete(21L);
		List<Tag> tags = hibernateTagDAO.findAll();
		assertEquals(22, tags.size());
	}

	/**
	 * Test add
	 * 
	 * @throws DAOException
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void addTest() throws DAOException {
		Tag tag = new Tag();
		tag.setName("qwerty");
		hibernateTagDAO.add(tag);
	}

	/**
	 * Test update
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void updateTest() throws DAOException {
		Tag expected = hibernateTagDAO.findById(7L);
		expected.setName("qwerty");
		hibernateTagDAO.update(expected);
		Tag actual = hibernateTagDAO.findById(7L);
		assertEquals(expected, actual);
	}
}