package com.epam.news.dao.impl;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import com.epam.news.dao.hibernate.HibernateUserDAO;
import com.epam.news.entity.User;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * Test C.R.U.D operations from HibernateUserDAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-configuration-test.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, TransactionDbUnitTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class HibernateUserDAOTest {
	private final static String FULL_DB_PATH = "classpath:fullDB.xml";

	@Autowired
	HibernateUserDAO hibernateUserDAO;

	/**
	 * Find by id test
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void findByIdTest() throws DAOException {
		User expected = new User(1L, "admin", "admin", "d033e22ae348aeb5660fc2140aec35850c4da997", null);
		User actual = hibernateUserDAO.findById(1L);
		assertEquals(expected, actual);

	}

	/**
	 * Add test
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void addTest() throws DAOException {
		User expected = new User(1L, "admin1", "admin1", "d033e22ae348aeb5660fc2140aec35850c4da997", new Date());
		hibernateUserDAO.add(expected);
		User actual = hibernateUserDAO.findById(expected.getId());
		assertEquals(expected, actual);
	}

	/**
	 * Update test
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void updateTest() throws DAOException {
		User expected = new User(1L, "admin1", "admin1", "qwerty", new Date());
		hibernateUserDAO.update(expected);
		User actual = hibernateUserDAO.findById(expected.getId());
		assertEquals(expected, actual);
	}

	/**
	 * Delete test
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void deleteTest() throws DAOException {
		hibernateUserDAO.delete(1L);
		assertEquals(4, hibernateUserDAO.findAll().size());
	}

	/**
	 * Find by login
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void findByLoginTest() throws DAOException {
		User expected = new User(1L, "admin", "admin", "d033e22ae348aeb5660fc2140aec35850c4da997", null);
		User actual = hibernateUserDAO.findByLogin(expected.getLogin());
		assertEquals(expected, actual);
	}

	/**
	 * Find user role
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void findUserRoleTest() throws DAOException {
		String expected = "ROLE_ADMIN";
		String actual = hibernateUserDAO.findUserRole(1L);
		assertEquals(expected, actual);
	}
}