package com.epam.news.dao.impl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import com.epam.news.dao.hibernate.HibernateAuthorDAO;
import com.epam.news.dao.hibernate.HibernateCommentDAO;
import com.epam.news.dao.hibernate.HibernateNewsDAO;
import com.epam.news.dao.hibernate.HibernateTagDAO;
import com.epam.news.entity.Author;
import com.epam.news.entity.Comment;
import com.epam.news.entity.FilterVO;
import com.epam.news.entity.News;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * Test C.R.U.D operations from HibernateNewsDAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-configuration-test.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, TransactionDbUnitTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class HibernateNewsDAOTest {
	private final static String FULL_DB_PATH = "classpath:fullDB.xml";

	@Autowired
	HibernateNewsDAO hibernateNewsDAO;
	@Autowired
	HibernateCommentDAO hibernateCommentDAO;
	@Autowired
	HibernateAuthorDAO hibernateAuthorDAO;
	@Autowired
	HibernateTagDAO hibernateTagDAO;
	private FilterVO filterVO = new FilterVO(null, null);

	/**
	 * Test findNewsByFilters by author
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void testFindNewsByAuthor() throws DAOException {
		filterVO.setAuthorSearch(new Author(1L, "", null));
		List<News> newsFilter = hibernateNewsDAO.findNewsByFilters(10, 1, filterVO);
		assertEquals(3, newsFilter.size());
	}

	/**
	 * Test findNewsByFilters by tag/tags
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void testFindNewsByTags() throws DAOException {
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(new Tag(21L, "NATO"));
		filterVO.setTagsSearch(tags);
		List<News> newsFilter = hibernateNewsDAO.findNewsByFilters(10, 1, filterVO);
		assertEquals(3, newsFilter.size());

		tags.add(new Tag(22L, "Europe"));
		filterVO.setTagsSearch(tags);
		newsFilter = hibernateNewsDAO.findNewsByFilters(10, 1, filterVO);
		assertEquals(3, newsFilter.size());
	}

	/**
	 * Test findNewsByFilters by tags and author
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void testFindNewsByAuthorTags() throws DAOException {
		Author author = new Author(1L, "", null);
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(new Tag(15L, "military"));

		filterVO.setTagsSearch(tags);
		filterVO.setAuthorSearch(author);

		List<News> newsFilter = hibernateNewsDAO.findNewsByFilters(10, 1, filterVO);
		assertEquals(2, newsFilter.size());

		tags.add(new Tag(14L, "protest"));
		newsFilter = hibernateNewsDAO.findNewsByFilters(10, 1, filterVO);
		assertEquals(2, newsFilter.size());
	}

	/**
	 * Test findNewsByFilters without searchCriteria
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void testFindNews() throws DAOException {
		List<News> newsFilter = hibernateNewsDAO.findNewsByFilters(10, 1, filterVO);
		assertEquals(9, newsFilter.size());
	}

	/**
	 * Test delete
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void deleteTest() throws DAOException {

		hibernateNewsDAO.delete(9L);

		assertEquals(8, hibernateNewsDAO.countNewsByFilter(filterVO));
	}

	/**
	 * Count news from filter test
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void countNewsFromFilter() throws DAOException {
		Author author = new Author(1L, "", null);
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(new Tag(15L, "military"));

		filterVO.setTagsSearch(tags);
		filterVO.setAuthorSearch(author);
		assertEquals(2, hibernateNewsDAO.countNewsByFilter(filterVO));

		resetFilter();
		filterVO.setAuthorSearch(new Author(1L, "", null));

		assertEquals(3, hibernateNewsDAO.countNewsByFilter(filterVO));
	}

	/**
	 * News from filter by number test
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void newsFromFilterByNumberTest() throws DAOException {
		Author author = new Author(1L, "", null);
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(new Tag(15L, "military"));

		filterVO.setTagsSearch(tags);
		filterVO.setAuthorSearch(author);

		assertEquals((Long) 2L, hibernateNewsDAO.newsFromFilterByNumber(1, filterVO).getId());
		assertEquals((Long) 3L, hibernateNewsDAO.newsFromFilterByNumber(2, filterVO).getId());

	}

	/**
	 * add, update test
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void addUpdateTest() throws DAOException {
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(hibernateTagDAO.findById(1L));
		tags.add(hibernateTagDAO.findById(2L));
		tags.add(hibernateTagDAO.findById(3L));
		Author author = hibernateAuthorDAO.findById(1L);
		News news = new News();

		news.setTitle("qwerty");
		news.setShortText("qwerty");
		news.setFullText("qwerty");
		news.setAuthor(author);
		news.setTags(tags);
		hibernateNewsDAO.add(news);
		assertEquals(10, hibernateNewsDAO.countNewsByFilter(filterVO));// test
																		// add

		Comment comment = new Comment(null, "qwerty", new Date(), news);
		hibernateCommentDAO.add(comment);
		comment = new Comment(null, "qwertyqwerty", new Date(), news);
		hibernateCommentDAO.add(comment);
		news = hibernateNewsDAO.findById(news.getId());

		tags.add(hibernateTagDAO.findById(4L));
		author = hibernateAuthorDAO.findById(2L);
		news.setTags(tags);
		news.setAuthor(author);
		hibernateNewsDAO.update(news);
		News newsActual = hibernateNewsDAO.findById(news.getId());
		assertEquals(news.getTitle(), newsActual.getTitle());
		assertEquals(news.getAuthor(), newsActual.getAuthor());// test update
	}

	@Before
	public void resetFilter() {
		filterVO.setTagsSearch(null);
		filterVO.setAuthorSearch(null);
	}
}