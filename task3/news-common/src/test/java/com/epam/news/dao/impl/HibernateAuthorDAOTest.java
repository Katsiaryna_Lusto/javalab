package com.epam.news.dao.impl;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import com.epam.news.dao.hibernate.HibernateAuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * Test C.R.U.D operations from HibernateAuthorDAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-configuration-test.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, TransactionDbUnitTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class HibernateAuthorDAOTest {
	private final static String FULL_DB_PATH = "classpath:fullDB.xml";

	@Autowired
	HibernateAuthorDAO hibernateAuthorDAO;

	/**
	 * Test find all
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void findAllTest() throws DAOException {
		List<Author> authors = hibernateAuthorDAO.findAll();
		assertEquals(20, authors.size());
	}

	/**
	 * Test find by id
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void testFindById() throws DAOException {
		Author expected = new Author(1L, "Ernest Lombardo", null);
		Author actual = hibernateAuthorDAO.findById(1L);
		assertEquals(expected, actual);
	}

	/**
	 * Test add
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void testAdd() throws DAOException {
		Author expected = new Author(1L, "qwerty", new Date());
		hibernateAuthorDAO.add(expected);
		Author actual = hibernateAuthorDAO.findById(expected.getId());
		assertEquals(expected, actual);
	}

	/**
	 * Test update
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void testUpdate() throws DAOException {
		Author excepted = hibernateAuthorDAO.findById(1L);
		excepted.setName("qwerty");
		excepted.setExpired(new Date());
		hibernateAuthorDAO.update(excepted);
		Author actual = hibernateAuthorDAO.findById(excepted.getId());
		assertEquals(excepted, actual);
	}

	/**
	 * Test delete
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void testDelete() throws DAOException {
		Author author = new Author(null, "qwerty", null);

		hibernateAuthorDAO.add(author);
		hibernateAuthorDAO.delete(author.getId());
		List<Author> authorList = hibernateAuthorDAO.findAll();
		assertEquals(20, authorList.size());
	}
}