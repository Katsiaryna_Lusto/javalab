package com.epam.news.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import com.epam.news.dao.hibernate.HibernateCommentDAO;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * Test C.R.U.D operations from HibernateCommentDAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-configuration-test.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, TransactionDbUnitTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class HibernateCommentDAOTest {
	private final static String FULL_DB_PATH = "classpath:fullDB.xml";

	@Autowired
	HibernateCommentDAO hibernateCommentDAO;

	/**
	 * Test find by id
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void findByIdTest() throws DAOException {
		Comment expected = new Comment(1L, "Oleg", null, null);
		Comment actual = hibernateCommentDAO.findById(1L);
		actual.setCreationDate(null);
		assertEquals(expected, actual);
	}

	/**
	 * Test add
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void addTest() throws DAOException {
		Comment expected = new Comment(1L, "qwerty", new Date(), null);
		hibernateCommentDAO.add(expected);
		Comment actual = hibernateCommentDAO.findById(expected.getId());
		assertEquals(expected, actual);
	}

	/**
	 * Test update
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void updateTest() throws DAOException {
		Comment expected = hibernateCommentDAO.findById(1L);
		expected.setCommentText("asdfgh");
		expected.setCreationDate(new Date());
		hibernateCommentDAO.update(expected);
		Comment actual = hibernateCommentDAO.findById(1L);
		assertEquals(expected, actual);
	}

	/**
	 * Test delete
	 */
	@Test
	@DatabaseSetup(FULL_DB_PATH)
	public void deleteTest() throws DAOException {
		hibernateCommentDAO.delete(1L);
		Comment comment = hibernateCommentDAO.findById(1L);
		assertNull(comment);
	}
}