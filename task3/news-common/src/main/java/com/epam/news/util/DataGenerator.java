package com.epam.news.util;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * Class to generate Set of random Strings
 */
public class DataGenerator {
	private DataGenerator() {

	}

	public static Set<String> generateNames(int number) {
		Set<String> namesSet = new HashSet<String>();
		for (int i = 0; i < number; i++) {
			String firstLetter = RandomStringUtils.random(1, 65, 91, true, false);
			StringBuilder generatedString = new StringBuilder(firstLetter);
			Random random = new Random();
			int length = random.nextInt(9) + 2;
			String nextLetters = RandomStringUtils.random(length, 97, 123, true, false);
			generatedString.append(nextLetters);
			boolean isAdd = namesSet.add(generatedString.toString());
			if (!isAdd) {
				i--;
			}
		}
		return namesSet;
	}
}