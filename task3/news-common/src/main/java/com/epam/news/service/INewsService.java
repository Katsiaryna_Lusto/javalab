package com.epam.news.service;

import java.util.List;

import com.epam.news.dao.INewsDAO;
import com.epam.news.entity.FilterVO;
import com.epam.news.entity.News;
import com.epam.news.exception.ServiceException;

/**
 * Interface that provides actions with news
 *
 */
public interface INewsService {
	/**
	 * Adds news and set inserted id into news
	 * 
	 * @param news
	 *            to be added
	 */
	void add(News news) throws ServiceException;

	/**
	 * Deletes given news by news id
	 * 
	 * @param id
	 *            of news
	 */
	void delete(Long id) throws ServiceException;

	/**
	 * Updates news information by news id
	 * 
	 * @param news
	 *            to be edited
	 */
	void update(News news) throws ServiceException;

	/**
	 * Returns list of news from filter on given page number. Page numbers begin
	 * with 1. News ordered by comments number and modification date
	 * 
	 * @param newsOnPage
	 *            how many news will be on page
	 * @param pageNumber
	 *            determine number of page which should be return
	 * @return list of news on given page
	 */
	List<News> findNewsByFilters(int newsOnPage, int pageNumber, FilterVO filterVO) throws ServiceException;

	/**
	 * Find news by id
	 * 
	 * @param id
	 *            of news
	 * @return news by given id
	 */
	News findById(Long id) throws ServiceException;

	/**
	 * Finds count of news by given tags, author, tags, pag
	 * 
	 * @return
	 * @throws ServiceException
	 */
	int countNewsByFilter(FilterVO filterVO) throws ServiceException;

	/**
	 * Finds news from filter by news number
	 * 
	 * @param newsNumber
	 * @return news
	 * @throws ServiceException
	 */
	News newsFromFilterByNumber(int newsNumber, FilterVO filterVO) throws ServiceException;

	/**
	 * Finds news number in list of filtered news
	 * 
	 * @param newsId
	 * @return news number
	 * @throws ServiceException
	 */
	Integer newsNumberFromFilter(Long newsId, FilterVO filterVO) throws ServiceException;

	/**
	 * set NewsDAO
	 * 
	 * @param newsDAO
	 */
	void setNewsDAO(INewsDAO newsDAO);
}
