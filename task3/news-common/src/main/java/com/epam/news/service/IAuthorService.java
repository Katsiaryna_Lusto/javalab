package com.epam.news.service;

import java.util.List;

import com.epam.news.dao.IAuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.exception.ServiceException;

/**
 * Interface that provides actions with authors
 *
 */
public interface IAuthorService {
	/**
	 * Adds new author and set inserted id into author
	 * 
	 * @param author
	 *            to be added
	 */
	void add(Author author) throws ServiceException;

	/**
	 * Deletes given author by author id
	 * 
	 * @param id
	 *            of author
	 */
	void delete(Long id) throws ServiceException;

	/**
	 * Updates given author by comment id
	 * 
	 * @param author
	 *            to be updated
	 * @throws ServiceException
	 */
	void update(Author author) throws ServiceException;

	/**
	 * Finds author by id
	 * 
	 * @param id
	 * @return author or null
	 * @throws ServiceException
	 */
	Author findById(Long id) throws ServiceException;

	/**
	 * Finds all authors
	 * 
	 * @return list of authors
	 * @throws ServiceException
	 */
	List<Author> findAll() throws ServiceException;

	/**
	 * set AuthorDAO
	 */
	public void setAuthorDAO(IAuthorDAO authorDAO);

}
