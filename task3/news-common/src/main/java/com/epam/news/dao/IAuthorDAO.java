package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;

/**
 * Interface that provides operations with Author
 *
 */
public interface IAuthorDAO extends IGenericDAO<Author, Long> {
	/**
	 * Finds all authors
	 * 
	 * @return List of authors
	 * @throws DAOException
	 */
	List<Author> findAll() throws DAOException;

}
