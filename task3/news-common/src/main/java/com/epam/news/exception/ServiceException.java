package com.epam.news.exception;

public class ServiceException extends Exception {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 5316149255644102133L;
    public ServiceException(String message) {
        super(message);
    }
    public ServiceException(Throwable e) {
        super(e.getMessage(),e);
    }
    public ServiceException(String message,Throwable e) {
        super(message,e);
    }
}
