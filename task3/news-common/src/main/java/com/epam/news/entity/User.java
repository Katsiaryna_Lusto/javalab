package com.epam.news.entity;

import java.io.Serializable;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Class to store user info
 */
@Entity
@Table(name = "USERS")
public class User implements Serializable {

	private static final long serialVersionUID = -3641995940725268144L;
	/**
	 * User id
	 */
	@Id
	@GeneratedValue(generator = "user_seq")
	@SequenceGenerator(name = "user_seq", sequenceName = "USERS_SEQ", allocationSize = 1)
	@Column(name = "USER_ID", nullable = false, unique = true)
	private Long id;
	/**
	 * user name
	 */
	@Column(name = "USER_NAME")
	private String name;
	/**
	 * user login
	 */
	@Column(name = "LOGIN")
	private String login;
	/**
	 * user password
	 */
	@Column(name = "PASSWORD")
	private String password;
	/**
	 * user expired date
	 */
	@Column(name = "EXPIRED")
	private Date expired;

	public User(Long id, String name, String login, String password, Date expired) {
		this.id = id;
		this.name = name;
		this.login = login;
		this.password = password;
		this.expired = expired;
	}

	public User() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getExpired() {
		return expired;
	}

	public void setExpired(Date expired) {
		this.expired = expired;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		User user = (User) o;

		if (expired != null ? !expired.equals(user.expired) : user.expired != null)
			return false;
		if (id != null ? !id.equals(user.id) : user.id != null)
			return false;
		if (login != null ? !login.equals(user.login) : user.login != null)
			return false;
		if (name != null ? !name.equals(user.name) : user.name != null)
			return false;
		if (password != null ? !password.equals(user.password) : user.password != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (login != null ? login.hashCode() : 0);
		result = 31 * result + (password != null ? password.hashCode() : 0);
		result = 31 * result + (expired != null ? expired.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "User{" + "id=" + id + ", name='" + name + '\'' + ", login='" + login + '\'' + ", password='" + password
				+ '\'' + ", expired=" + expired + '}';
	}
}