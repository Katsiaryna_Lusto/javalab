package com.epam.news.dao;

import java.io.Serializable;

import com.epam.news.exception.DAOException;

/**
 * Interface that provides C.R.U.D. operations with instances
 *
 */
public interface IGenericDAO<T, ID extends Serializable> {

	/**
	 * Creates instance with given info
	 * 
	 * @param entity
	 * @return id of inserted entity
	 * @throws DAOException
	 */
	ID add(T entity) throws DAOException;

	/**
	 * Deletes instance by id
	 * 
	 * @param id
	 * @return true if delete is successful, returns false otherwise
	 * @throws DAOException
	 */
	void delete(ID id) throws DAOException;

	/**
	 * Finds instance by id
	 * 
	 * @param id
	 * @return instance if find is successful, returns null otherwise
	 * @throws DAOException
	 */
	T findById(ID id) throws DAOException;

	/**
	 * Updates instance info by id
	 * 
	 * @param entity
	 * @return true if update is successful, returns false otherwise
	 * @throws DAOException
	 */
	void update(T entity) throws DAOException;
}