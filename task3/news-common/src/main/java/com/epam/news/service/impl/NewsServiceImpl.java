package com.epam.news.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.dao.INewsDAO;
import com.epam.news.entity.FilterVO;
import com.epam.news.entity.News;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.INewsService;

/**
 * Class that provides actions with news
 *
 */
@Transactional(rollbackFor = { ServiceException.class, RuntimeException.class })
public class NewsServiceImpl implements INewsService {

	private static Logger logger = Logger.getLogger(NewsServiceImpl.class);

	private INewsDAO newsDAO;

	public void setNewsDAO(INewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}

	/**
	 * Adds news and set inserted id into news
	 * 
	 * @param news
	 *            to be added
	 */
	@Override
	public void add(News news) throws ServiceException {
		try {
			news.setId(newsDAO.add(news));
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}

	}

	/**
	 * Deletes given news by news id
	 * 
	 * @param id
	 *            of news
	 */
	@Override
	public void delete(Long id) throws ServiceException {
		try {
			newsDAO.delete(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}

	}

	/**
	 * Edits news information by news id
	 * 
	 * @param news
	 *            to be edited
	 */
	@Override
	public void update(News news) throws ServiceException {
		try {
			newsDAO.update(news);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	/**
	 * Returns list of news on given page number. Page numbers begin with 1.
	 * News ordered by creation date;
	 * 
	 * @param newsOnPage
	 *            how many news will be on page
	 * @param pageNumber
	 *            determine number of page which should be return
	 * @return list of news on given page
	 */
	@Override
	public List<News> findNewsByFilters(int newsOnPage, int pageNumber, FilterVO filterVO) throws ServiceException {
		List<News> result = null;
		try {
			result = newsDAO.findNewsByFilters(newsOnPage, pageNumber, filterVO);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return result;
	}

	/**
	 * Finds news by id
	 * 
	 * @param id
	 *            of news
	 * @return news by given id
	 */
	@Override
	public News findById(Long id) throws ServiceException {
		News result = null;
		try {
			result = newsDAO.findById(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return result;
	}

	/**
	 * Finds count of news by given tags, author, tags, pag
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@Override
	public int countNewsByFilter(FilterVO filterVO) throws ServiceException {
		int result = 0;
		try {
			result = newsDAO.countNewsByFilter(filterVO);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return result;
	}

	/**
	 * Finds news from filter by news number
	 * 
	 * @param newsNumber
	 * @return news
	 * @throws ServiceException
	 */
	@Override
	public News newsFromFilterByNumber(int newsNumber, FilterVO filterVO) throws ServiceException {
		News result = null;
		try {
			result = newsDAO.newsFromFilterByNumber(newsNumber, filterVO);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return result;
	}

	/**
	 * Finds news number in list of filtered news
	 * 
	 * @param newsId
	 * @return news number
	 * @throws ServiceException
	 */
	@Override
	public Integer newsNumberFromFilter(Long newsId, FilterVO filterVO) throws ServiceException {
		Integer result = null;
		try {
			result = newsDAO.newsNumberFromFilter(newsId, filterVO);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return result;
	}

}
