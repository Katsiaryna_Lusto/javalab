package com.epam.news.entity;

import java.io.Serializable;
import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Class to store tag info
 *
 */
@Entity
@Table(name = "TAG")
public class Tag implements Serializable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -5494264259688759151L;
	/**
	 * Tag id
	 */
	@Id
	@GeneratedValue(generator = "tag_seq")
	@SequenceGenerator(name = "tag_seq", sequenceName = "TAG_SEQ", allocationSize = 1)
	@Column(name = "TAG_ID", unique = true)
	private Long id;
	/**
	 * Tag name
	 */
	@Column(name = "TAG_NAME")
	private String name;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "NEWS_TAG", joinColumns = { @JoinColumn(name = "TAG_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "NEWS_ID") })
	List<News> news;

	public Tag() {
	}

	public Tag(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public Tag(Long id, String name, List<News> news) {
		this.id = id;
		this.name = name;
		this.news = news;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<News> getNews() {
		return news;
	}

	public void setNews(List<News> news) {
		this.news = news;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Tag tag = (Tag) o;

		if (id != null ? !id.equals(tag.id) : tag.id != null)
			return false;
		if (name != null ? !name.equals(tag.name) : tag.name != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Tag{" + "id=" + id + ", name='" + name + '\'' + '}';
	}
}