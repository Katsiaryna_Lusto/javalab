package com.epam.news.service;

import java.util.List;

import com.epam.news.dao.ITagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;

/**
 * Interface that provides actions with tags
 *
 */

public interface ITagService {
	/**
	 * Adds new tag and set inserted id into tag
	 * 
	 * @param tag
	 *            to be added
	 */
	void add(Tag tag) throws ServiceException;

	/**
	 * Deletes given tag by tag id
	 * 
	 * @param id
	 *            of tag
	 */
	void delete(Long id) throws ServiceException;

	/**
	 * Updates given tag by tag id
	 * 
	 * @param tag
	 *            to be updated
	 * @throws ServiceException
	 */
	void update(Tag tag) throws ServiceException;

	/**
	 * Finds tag by id
	 * 
	 * @param id
	 * @return tag or null
	 * @throws ServiceException
	 */
	Tag findById(Long id) throws ServiceException;

	/**
	 * Finds all tags
	 * 
	 * @return list of tags
	 * @throws ServiceException
	 */
	List<Tag> findAll() throws ServiceException;

	/**
	 * set TagDAO
	 * 
	 * @param tagDAO
	 */
	public void setTagDAO(ITagDAO tagDAO);
}
