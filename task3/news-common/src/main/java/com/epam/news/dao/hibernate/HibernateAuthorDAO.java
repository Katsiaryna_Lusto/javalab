package com.epam.news.dao.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;

import com.epam.news.dao.IAuthorDAO;
import com.epam.news.dao.IGenericDAO;
import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;
import com.epam.news.util.HibernateUtil;

public class HibernateAuthorDAO implements IGenericDAO<Author, Long>, IAuthorDAO {
	/**
	 * Finds all authors
	 * 
	 * @return List of authors
	 * @throws DAOException
	 */
	@Override
	public List<Author> findAll() throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		List<Author> result;
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Criteria criteria = session.createCriteria(Author.class);
			criteria.addOrder(Order.asc("name"));
			result = criteria.list();
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return result;
	}

	/**
	 * Finds author by id
	 * 
	 * @param id
	 * @throws DAOException
	 */
	@Override
	public Author findById(Long id) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Author author = null;
		try {
			author = (Author) session.get(Author.class, id);
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return author;
	}

	/**
	 * Deletes author by id
	 * 
	 * @param id
	 *            of author
	 * @throws DAOException
	 */
	@Override
	public void delete(Long id) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Author author = (Author) session.load(Author.class, id);
			session.delete(author);
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
	}

	/**
	 * Creates author with given info
	 * 
	 * @param entity
	 * @return id of inserted author
	 * @throws DAOException
	 */
	@Override
	public Long add(Author entity) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.save(entity);
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return entity.getId();

	}

	/**
	 * Updates author info by id
	 * 
	 * @param entity
	 * @throws DAOException
	 */
	@Override
	public void update(Author entity) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.update(entity);
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
	}

	private void closeSession(Session session) {
		if (session.isOpen()) {
			session.close();
		}
	}
}