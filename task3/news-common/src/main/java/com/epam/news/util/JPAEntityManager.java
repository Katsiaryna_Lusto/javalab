package com.epam.news.util;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Class to create EntityManagerFactory
 */
public final class JPAEntityManager {
	public static final String NEWS_JPA_UNIT = "newsJPAUnit";
	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory(NEWS_JPA_UNIT);

	public JPAEntityManager(String jpaUnit) {
		factory = Persistence.createEntityManagerFactory(jpaUnit);
	}

	public static EntityManagerFactory getEntityManagerFactory() {
		return factory;
	}
}