package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.FilterVO;
import com.epam.news.entity.News;
import com.epam.news.exception.DAOException;

/**
 * Interface that provides operations with News
 *
 */
public interface INewsDAO extends IGenericDAO<News, Long> {

	/**
	 * Finds count of news by given tags, author, tags, page
	 * 
	 * @param filterVO
	 * @return list of news
	 * @throws DAOException
	 */
	int countNewsByFilter(FilterVO filterVO) throws DAOException;

	/**
	 * Finds news number in list of filtered news
	 * 
	 * @param newsId
	 * @return news number
	 * @throws DAOException
	 */
	Integer newsNumberFromFilter(Long newsId, FilterVO filterVO) throws DAOException;

	/**
	 * Finds news from filter by news number
	 * 
	 * @param newsNumber
	 * @param filterVO
	 * @return news
	 * @throws DAOException
	 */
	News newsFromFilterByNumber(int newsNumber, FilterVO filterVO) throws DAOException;

	/**
	 * Returns list of news from filter on given page number. Page numbers begin
	 * with 1. News ordered by comments number and modification date
	 * 
	 * @param newsOnPage
	 *            how many news will be on page
	 * @param pageNumber
	 *            determine number of page which should be return
	 * @param filterVO
	 * @return list of news on given page
	 */
	public List<News> findNewsByFilters(int pageNumber, int newsOnPage, FilterVO filterVO) throws DAOException;

}
