package com.epam.news.dao.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;

import com.epam.news.dao.IGenericDAO;
import com.epam.news.dao.ITagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.util.HibernateUtil;

public class HibernateTagDAO implements IGenericDAO<Tag, Long>, ITagDAO {
	/**
	 * Finds all tags
	 * 
	 * @return List of tags
	 * @throws DAOException
	 */
	@Override
	public List<Tag> findAll() throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Tag> result = null;
		try {
			transaction = session.beginTransaction();
			Criteria criteria = session.createCriteria(Tag.class);
			criteria.addOrder(Order.asc("name"));
			result = criteria.list();
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return result;
	}

	/**
	 * Finds tag by id
	 * 
	 * @param id
	 * @return tag if find is successful, returns null otherwise
	 * @throws DAOException
	 */
	@Override
	public Tag findById(Long id) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Tag tag = null;
		try {
			tag = (Tag) session.get(Tag.class, id);
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}

		return tag;
	}

	/**
	 * Deletes tag by id
	 * 
	 * @param id
	 *            of tag
	 * @throws DAOException
	 */
	@Override
	public void delete(Long id) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Tag tag = (Tag) session.load(Tag.class, id);
			tag.setNews(null);
			session.delete(tag);
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
	}

	/**
	 * Creates tag with given info
	 * 
	 * @param entity
	 * @return id of inserted tag
	 * @throws DAOException
	 */
	@Override
	public Long add(Tag entity) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.save(entity);
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return entity.getId();
	}

	/**
	 * Updates tag info by id
	 * 
	 * @param entity
	 * @throws DAOException
	 */
	@Override
	public void update(Tag entity) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.update(entity);
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}

	}

	private void closeSession(Session session) {
		if (session.isOpen()) {
			session.close();
		}
	}
}
