package com.epam.news.service;

import com.epam.news.dao.IGenericDAO;
import com.epam.news.entity.Comment;
import com.epam.news.exception.ServiceException;

/**
 * Interface that provides actions with comments
 *
 */
public interface ICommentService {
	/**
	 * Adds new comment and set inserted id into comment
	 * 
	 * @param comment
	 *            to be added
	 */
	void add(Comment comment) throws ServiceException;

	/**
	 * Deletes given comment by comment id
	 * 
	 * @param id
	 *            of comment
	 */
	void delete(Long id) throws ServiceException;

	/**
	 * Updates given comment by comment id
	 * 
	 * @param comment
	 *            to be updated
	 * @throws ServiceException
	 */
	void update(Comment comment) throws ServiceException;

	/**
	 * Finds comment by id
	 * 
	 * @param id
	 * @return Comment or null
	 * @throws ServiceException
	 */
	Comment findById(Long id) throws ServiceException;

	/**
	 * set CommentDAO
	 * 
	 * @param commentDAO
	 */
	void setCommentDAO(IGenericDAO commentDAO);
}
