package com.epam.news.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.epam.news.dao.IGenericDAO;
import com.epam.news.dao.INewsDAO;
import com.epam.news.entity.FilterVO;
import com.epam.news.entity.News;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.util.HibernateUtil;

public class HibernateNewsDAO implements IGenericDAO<News, Long>, INewsDAO {
	/**
	 * Finds news by id
	 * 
	 * @param id
	 * @return
	 * @throws DAOException
	 */
	@Override
	public News findById(Long id) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		News news = null;
		try {
			news = (News) session.get(News.class, id);
			news.getComments().size();
			news.getTags().size();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return news;
	}

	/**
	 * Deletes news by id
	 * 
	 * @param id
	 *            of news
	 * @throws DAOException
	 */
	@Override
	public void delete(Long id) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction;
		try {
			transaction = session.beginTransaction();
			News news = (News) session.load(News.class, id);
			news.setAuthor(null);
			news.setTags(null);
			session.delete(news);
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
	}

	/**
	 * Creates news with given info
	 * 
	 * @param entity
	 * @return id of inserted news
	 * @throws DAOException
	 */
	@Override
	public Long add(News entity) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction;
		try {
			transaction = session.beginTransaction();
			session.save(entity);
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return entity.getId();
	}

	/**
	 * Updates news info by id
	 * 
	 * @param entity
	 * @throws DAOException
	 */
	@Override
	public void update(News entity) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction;
		try {
			transaction = session.beginTransaction();
			session.update(entity);
			Thread.sleep(3000); // To present optimistic lock
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
	}

	/**
	 * Finds count of news by given tags, author, tags, page
	 * 
	 * @param filterVO
	 * @return list of news
	 * @throws DAOException
	 */
	@Override
	public int countNewsByFilter(FilterVO filterVO) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction;
		int result = 0;
		try {
			transaction = session.beginTransaction();
			String hql = buildHQLFilter(filterVO);
			Query query = session.createQuery(hql);
			insertSearchCriteria(query, filterVO);
			result = query.list().size();
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return result;
	}

	/**
	 * Finds news number in list of filtered news
	 * 
	 * @param newsId
	 * @return news number
	 * @throws DAOException
	 */
	@Override
	public Integer newsNumberFromFilter(Long newsId, FilterVO filterVO) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction;
		Integer result = null;
		try {
			transaction = session.beginTransaction();
			String hql = buildHQLFilter(filterVO);
			Query query = session.createQuery(hql);
			insertSearchCriteria(query, filterVO);
			List<News> newsList = query.list();
			for (int i = 0; i < newsList.size(); i++) {

				if (newsId.equals(newsList.get(i).getId())) {
					result = i + 1;
					break;
				}
			}
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return result;
	}

	/**
	 * Finds news from filter by news number
	 * 
	 * @param newsNumber
	 * @param filterVO
	 * @return news
	 * @throws DAOException
	 */
	@Override
	public News newsFromFilterByNumber(int newsNumber, FilterVO filterVO) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction;
		News result = null;
		try {
			transaction = session.beginTransaction();
			String hql = buildHQLFilter(filterVO);
			Query query = session.createQuery(hql);
			insertSearchCriteria(query, filterVO);
			query.setFirstResult(newsNumber - 1);
			query.setMaxResults(1);
			result = (News) query.uniqueResult();
			result.getComments().size();// fetch lazy
			result.getTags().size();
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return result;
	}

	/**
	 * Returns list of news from filter on given page number. Page numbers begin
	 * with 1. News ordered by comments number and modification date
	 * 
	 * @param newsOnPage
	 *            how many news will be on page
	 * @param pageNumber
	 *            determine number of page which should be return
	 * @param filterVO
	 * @return list of news on given page
	 */
	@Override
	public List<News> findNewsByFilters(int newsOnPage, int pageNumber, FilterVO filterVO) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction;
		List<News> result = null;
		try {
			transaction = session.beginTransaction();
			String hql = buildHQLFilter(filterVO);
			Query query = session.createQuery(hql);
			insertSearchCriteria(query, filterVO);

			int startIndex;
			startIndex = (pageNumber - 1) * newsOnPage;
			query.setFirstResult(startIndex);
			query.setMaxResults(newsOnPage);
			result = query.list();
			if (result != null) {
				for (News temp : result) {
					temp.getComments().size();// fetch lazy
					temp.getTags().size();
				}
			}
			if (result.size() == 0)
				result = null;
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return result;
	}

	private void closeSession(Session session) {
		if (session.isOpen()) {
			session.close();
		}
	}

	private String buildHQLFilter(FilterVO filterVO) {
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT DISTINCT n FROM News n ");

		if (filterVO.getTagsSearch() != null) {
			jpql.append("JOIN n.tags as t ");
		}
		jpql.append("WHERE ");
		if (filterVO.getTagsSearch() != null) {
			jpql.append(" (t.id IN (:tagIds) ) ");
		} else {
			jpql.append(" 1=1 ");
		}
		jpql.append(" AND");
		if (filterVO.getAuthorSearch() != null) {
			jpql.append(" (n.author.id = :authorId) ");
		} else {
			jpql.append(" 1=1 ");
		}

		jpql.append("ORDER BY size(n.comments) DESC , n.modificationDate DESC");
		return jpql.toString();
	}

	/**
	 * Insert parameters from FilterVO into Query
	 */
	private void insertSearchCriteria(Query query, FilterVO filterVO) {

		if (filterVO.getTagsSearch() != null) {
			List<Long> tagIds = new ArrayList<Long>();
			for (Tag tag : filterVO.getTagsSearch()) {
				tagIds.add(tag.getId());
			}
			query.setParameterList("tagIds", tagIds);
		}
		if (filterVO.getAuthorSearch() != null) {
			query.setParameter("authorId", filterVO.getAuthorSearch().getId());
		}

	}
}