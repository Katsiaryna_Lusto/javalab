package com.epam.news.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;

/**
 * Interface that provides operations with Tags
 *
 */
@Transactional
public interface ITagDAO extends IGenericDAO<Tag, Long> {
	/**
	 * Finds all tags
	 * 
	 * @return List of tags
	 * @throws DAOException
	 */
	List<Tag> findAll() throws DAOException;

}