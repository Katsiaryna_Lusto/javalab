package com.epam.news.dao.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.epam.news.dao.IGenericDAO;
import com.epam.news.dao.IUserDAO;
import com.epam.news.entity.Role;
import com.epam.news.entity.User;
import com.epam.news.exception.DAOException;
import com.epam.news.util.HibernateUtil;

public class HibernateUserDAO implements IGenericDAO<User, Long>, IUserDAO {
	/**
	 * Finds all users
	 * 
	 * @return list of users
	 * @throws DAOException
	 */
	@Override
	public List<User> findAll() throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<User> result = null;
		try {
			transaction = session.beginTransaction();
			result = session.createCriteria(User.class).list();
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return result;
	}

	/**
	 * Finds user by id
	 * 
	 * @param id
	 * @return user
	 * @throws DAOException
	 */
	@Override
	public User findById(Long id) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		User user = null;
		try {
			user = (User) session.get(User.class, id);
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return user;
	}

	/**
	 * Deletes user by id
	 * 
	 * @param id
	 * @throws DAOException
	 */
	@Override
	public void delete(Long id) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			User user = (User) session.load(User.class, id);
			session.delete(user);
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
	}

	/**
	 * Creates user with given info
	 * 
	 * @param entity
	 * @return inserted id
	 * @throws DAOException
	 */
	@Override
	public Long add(User entity) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.save(entity);
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return entity.getId();
	}

	/**
	 * Updates user info by id
	 * 
	 * @param entity
	 * @throws DAOException
	 */
	@Override
	public void update(User entity) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(entity);
			tx.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
	}

	/**
	 * Finds user by login
	 * 
	 * @param login
	 * @return user
	 * @throws DAOException
	 */
	@Override
	public User findByLogin(String login) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		User user = null;
		try {
			Criteria criteria = session.createCriteria(User.class);
			user = (User) criteria.add(Restrictions.eq("login", login)).uniqueResult();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return user;
	}

	/**
	 * Finds user role
	 * 
	 * @param id
	 * @return user role
	 * @throws DAOException
	 */
	@Override
	public String findUserRole(Long id) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Role role = null;
		try {
			Criteria criteria = session.createCriteria(Role.class);
			role = (Role) criteria.add(Restrictions.eq("userId", id)).uniqueResult();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return role.getRoleName();
	}

	private void closeSession(Session session) {
		if (session.isOpen()) {
			session.close();
		}
	}
}