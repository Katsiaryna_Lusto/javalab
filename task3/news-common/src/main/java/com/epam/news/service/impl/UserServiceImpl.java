package com.epam.news.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.epam.news.dao.IUserDAO;
import com.epam.news.dao.IUserDAO;
import com.epam.news.entity.User;
import com.epam.news.entity.User;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.IUserService;
import com.epam.news.service.IUserService;

/**
 * Class provides operations with users
 */
@Service
public class UserServiceImpl implements IUserService {

	private static Logger logger = Logger.getLogger(UserServiceImpl.class);
	private IUserDAO userDAO;

	/**
	 * Finds all users
	 * 
	 * @return list of users
	 * @throws ServiceException
	 */
	@Override
	public List<User> findAll() throws ServiceException {
		List<User> result = null;
		try {
			result = userDAO.findAll();
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return result;
	}

	/**
	 * Finds user by id
	 * 
	 * @param id
	 * @return user
	 * @throws ServiceException
	 */
	@Override
	public User findById(Long id) throws ServiceException {
		User result = null;
		try {
			result = userDAO.findById(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return result;
	}

	/**
	 * Deletes user by id
	 * 
	 * @param id
	 * @throws ServiceException
	 */
	@Override
	public void delete(Long id) throws ServiceException {
		try {
			userDAO.delete(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	/**
	 * Creates user with given info
	 * 
	 * @param entity
	 * @return inserted id
	 * @throws ServiceException
	 */
	@Override
	public void add(User entity) throws ServiceException {
		try {
			entity.setId(userDAO.add(entity));
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	/**
	 * Updates user info by id
	 * 
	 * @param entity
	 * @throws ServiceException
	 */
	@Override
	public void update(User entity) throws ServiceException {
		try {
			userDAO.update(entity);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	/**
	 * Finds user by login
	 * 
	 * @param login
	 * @return user
	 * @throws ServiceException
	 */
	@Override
	public User findByLogin(String login) throws ServiceException {
		User result = null;
		try {
			result = userDAO.findByLogin(login);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return result;
	}

	/**
	 * Finds user role
	 * 
	 * @param id
	 * @return user role
	 * @throws ServiceException
	 */
	@Override
	public String findUserRole(Long id) throws ServiceException {
		String result = null;
		try {
			result = userDAO.findUserRole(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return result;
	}

	@Override
	public void setUserDAO(IUserDAO userDAO) {
		this.userDAO = userDAO;
	}
}