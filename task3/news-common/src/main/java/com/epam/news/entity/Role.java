package com.epam.news.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ROLES")
public class Role implements Serializable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7424987411456832916L;
	@Id
	@Column(name = "USER_ID")
	Long userId;
	@Column(name = "ROLE_NAME")
	String roleName;

	public Role() {
	}

	public Role(Long userId, String roleName) {
		this.userId = userId;
		this.roleName = roleName;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Role role = (Role) o;

		if (roleName != null ? !roleName.equals(role.roleName) : role.roleName != null)
			return false;
		if (userId != null ? !userId.equals(role.userId) : role.userId != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = userId != null ? userId.hashCode() : 0;
		result = 31 * result + (roleName != null ? roleName.hashCode() : 0);
		return result;
	}
}