package com.epam.news.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Class to store news info
 *
 */
@Entity
@Table(name = "NEWS")
public class News implements Serializable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 883279937885116359L;
	/**
	 * News id
	 */
	@Id
	@GeneratedValue(generator = "news_seq")
	@SequenceGenerator(name = "news_seq", sequenceName = "NEWS_SEQ", allocationSize = 1)
	@Column(name = "NEWS_ID", nullable = false, unique = true)
	private Long id;
	/**
	 * News short text
	 */
	@Column(name = "SHORT_TEXT")
	private String shortText;
	/**
	 * News full text
	 */
	@Column(name = "FULL_TEXT")
	private String fullText;
	/**
	 * News title
	 */

	@Column(name = "TITLE")
	private String title;
	/**
	 * News creation date
	 */
	@Column(name = "CREATION_DATE")
	private Date creationDate;
	/**
	 * News modification date
	 */
	@Column(name = "MODIFICATION_DATE")
	private Date modificationDate;
	/**
	 * News author
	 */
	@ManyToOne(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
	@JoinTable(name = "NEWS_AUTHOR", joinColumns = { @JoinColumn(name = "NEWS_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "AUTHOR_ID") })
	private Author author;
	/**
	 * News tags
	 */
	@OneToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "NEWS_TAG", joinColumns = { @JoinColumn(name = "NEWS_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "TAG_ID") })
	private List<Tag> tags;
	/**
	 * News comments
	 */
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "NEWS_ID")
	@OrderBy("creationDate DESC")
	private List<Comment> comments;

	@Version
	@Column(name = "OPTLOCK")
	Integer version;// Optimistic lock

	public News() {
		comments = new ArrayList<Comment>();
		tags = new ArrayList<Tag>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tag) {
		this.tags = tag;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		News news = (News) o;

		if (author != null ? !author.equals(news.author) : news.author != null)
			return false;
		if (comments != null ? !comments.equals(news.comments) : news.comments != null)
			return false;
		if (creationDate != null ? !creationDate.equals(news.creationDate) : news.creationDate != null)
			return false;
		if (fullText != null ? !fullText.equals(news.fullText) : news.fullText != null)
			return false;
		if (id != null ? !id.equals(news.id) : news.id != null)
			return false;
		if (modificationDate != null ? !modificationDate.equals(news.modificationDate) : news.modificationDate != null)
			return false;
		if (shortText != null ? !shortText.equals(news.shortText) : news.shortText != null)
			return false;
		if (tags != null ? !tags.equals(news.tags) : news.tags != null)
			return false;
		if (title != null ? !title.equals(news.title) : news.title != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (shortText != null ? shortText.hashCode() : 0);
		result = 31 * result + (fullText != null ? fullText.hashCode() : 0);
		result = 31 * result + (title != null ? title.hashCode() : 0);
		result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
		result = 31 * result + (modificationDate != null ? modificationDate.hashCode() : 0);
		result = 31 * result + (author != null ? author.hashCode() : 0);
		result = 31 * result + (tags != null ? tags.hashCode() : 0);
		result = 31 * result + (comments != null ? comments.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "News{" + "id=" + id + ", shortText='" + shortText + '\'' + ", fullText='" + fullText + '\''
				+ ", title='" + title + '\'' + ", creationDate=" + creationDate + ", modificationDate="
				+ modificationDate + ", author=" + author + ", tag=" + tags + ", comments=" + comments + '}';
	}

}
