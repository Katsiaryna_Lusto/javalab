package com.epam.news.service;

import java.util.List;

import com.epam.news.dao.IUserDAO;
import com.epam.news.entity.User;
import com.epam.news.exception.ServiceException;

/**
 * Interface that provides orepations with User
 */
public interface IUserService {
	/**
	 * Finds all users
	 * 
	 * @return list of users
	 * @throws ServiceException
	 */
	List<User> findAll() throws ServiceException;

	/**
	 * Finds user by id
	 * 
	 * @param id
	 * @return user
	 * @throws ServiceException
	 */
	User findById(Long id) throws ServiceException;

	/**
	 * Deletes user by id
	 * 
	 * @param id
	 * @throws ServiceException
	 */
	void delete(Long id) throws ServiceException;

	/**
	 * Creates user with given info
	 * 
	 * @param entity
	 * @return inserted id
	 * @throws ServiceException
	 */
	void add(User entity) throws ServiceException;

	/**
	 * Updates user info by id
	 * 
	 * @param entity
	 * @throws ServiceException
	 */
	void update(User entity) throws ServiceException;

	/**
	 * Finds user by login
	 * 
	 * @param login
	 * @return user
	 * @throws ServiceException
	 */
	User findByLogin(String login) throws ServiceException;

	/**
	 * Finds user role
	 * 
	 * @param id
	 * @return user role
	 * @throws ServiceException
	 */
	String findUserRole(Long id) throws ServiceException;

	void setUserDAO(IUserDAO userDAO);
}
