package com.epam.news.dao.hibernate;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.epam.news.dao.IGenericDAO;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;
import com.epam.news.util.HibernateUtil;

public class HibernateCommentDAO implements IGenericDAO<Comment, Long> {
	/**
	 * Finds comment by id
	 * 
	 * @param id
	 * @return comment if find is successful, returns null otherwise
	 * @throws DAOException
	 */
	@Override
	public Comment findById(Long id) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Comment comment = null;
		try {
			comment = (Comment) session.get(Comment.class, id);
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return comment;
	}

	/**
	 * Deletes comment by id
	 * 
	 * @param id
	 *            of comment
	 * @throws DAOException
	 */
	@Override
	public void delete(Long id) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Comment comment = (Comment) session.load(Comment.class, id);
			comment.setNews(null);
			session.delete(comment);
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
	}

	/**
	 * Creates comment with given info
	 * 
	 * @param entity
	 * @return id of inserted comment
	 * @throws DAOException
	 */
	@Override
	public Long add(Comment entity) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.save(entity);
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return entity.getId();
	}

	/**
	 * Updates comment info by id
	 * 
	 * @param entity
	 * @throws DAOException
	 */
	@Override
	public void update(Comment entity) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.update(entity);
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
	}

	private void closeSession(Session session) {
		if (session.isOpen()) {
			session.close();
		}
	}
}