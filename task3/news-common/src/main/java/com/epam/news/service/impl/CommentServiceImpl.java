package com.epam.news.service.impl;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.dao.IGenericDAO;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.ICommentService;

/**
 * Class that provides actions with comments
 *
 */

@Transactional(rollbackFor = { ServiceException.class, RuntimeException.class })
public class CommentServiceImpl implements ICommentService {

	private static Logger logger = Logger.getLogger(CommentServiceImpl.class);

	private IGenericDAO<Comment, Long> commentDAO;

	public void setCommentDAO(IGenericDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	/**
	 * Adds new comment and set inserted id into comment
	 * 
	 * @param comment
	 *            to be added
	 */
	@Override
	public void add(Comment comment) throws ServiceException {
		try {
			comment.setId(commentDAO.add(comment));
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}

	}

	/**
	 * Deletes given comment by comment id
	 * 
	 * @param id
	 *            of comment
	 */
	@Override
	public void delete(Long id) throws ServiceException {
		try {
			commentDAO.delete(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}

	}

	/**
	 * Updates given comment by comment id
	 * 
	 * @param comment
	 *            to be updated
	 * @throws ServiceException
	 */
	@Override
	public void update(Comment comment) throws ServiceException {
		try {
			commentDAO.update(comment);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	/**
	 * Finds comment by id
	 * 
	 * @param id
	 * @return Comment or null
	 * @throws ServiceException
	 */
	@Override
	public Comment findById(Long id) throws ServiceException {
		Comment comment = null;
		try {
			comment = commentDAO.findById(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return comment;
	}

}
