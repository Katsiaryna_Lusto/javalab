package com.epam.news.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Class to store comment info
 *
 */
@Entity
@Table(name = "COMMENTS")
public class Comment implements Serializable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -5697896094322498108L;
	/**
	 * Comment id
	 */
	@Id
	@GeneratedValue(generator = "comment_seq")
	@SequenceGenerator(name = "comment_seq", sequenceName = "COMMENTS_SEQ", allocationSize = 1)
	@Column(name = "COMMENT_ID", nullable = false, unique = true)
	private Long id;
	/**
	 * Comment text
	 */
	@Column(name = "COMMENT_TEXT")
	private String commentText;
	/**
	 * Comment creation date
	 */
	@Column(name = "CREATION_DATE")
	private Date creationDate;

	/**
	 * Id of the news which the comment is added to
	 */
	@ManyToOne(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
	@JoinColumn(name = "NEWS_ID")
	private News news;

	public Comment() {
	}

	public Comment(Long id, String commentText, Date creationDate, News news) {
		this.id = id;
		this.commentText = commentText;
		this.creationDate = creationDate;
		this.news = news;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Comment comment = (Comment) o;

		if (commentText != null ? !commentText.equals(comment.commentText) : comment.commentText != null)
			return false;
		if (creationDate != null ? !creationDate.equals(comment.creationDate) : comment.creationDate != null)
			return false;
		if (id != null ? !id.equals(comment.id) : comment.id != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (commentText != null ? commentText.hashCode() : 0);
		result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Comment{" + "id=" + id + ", commentText='" + commentText + '\'' + ", creationDate=" + creationDate
				+ '}';
	}

}
