create database task1;

create table User (
USER_ID number(20) not null primary key,
USER_NAME nvarchar2(50) not null,
LOGIN nvarchar2(30) not null,
PASSWORD nvarchar2(30) not null
) 
create table Roles (
USER_ID number(20) not null,
ROLE_NAME nvarchar2(50) not null
foreig key (USER_ID)
) 

create table Tag (
TAG_ID number(20) not null primary key,
TAG_NAME nvarchar2(30) not null
) 

create table News_Tag(
NEWS_ID number(20) not null,
TAG_ID number(20) not null,
foreig key (NEWS_ID),
foreig key (TAG_ID)
)

create table News(
NEWS_ID number(20) not null primary key,
TITLE nvarchar2(20) not null,
SHORT_TEXT nvarchar2(100) not null,
FULL_TEXT nvarchar2(2000) not null,
CREATION_DATE timestamp not null,
MODIFICATION_DATE date not null
)

create table Comments(
COMMENT_ID number(20) not null primary key,
NEWS_ID number(20) not null,
COMMENT_TEXT nvarchar2(100) not null,
CREATION_DATE timestamp not null,
foreig key (NEWS_ID)
)

create table News_Author (
NEWS_ID number(20) not null,
AUTHOR_ID number(20) not null,
foreig key (NEWS_ID),
foreig key (AUTHOR_ID)
) 

create table Author (
AUTHOR_ID number(20) not null primary key,
AUTHOR_NAME nvarchar2(30) not null,
EXPIRED timestamp
) 




