package com.epam.calendar.model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * CLass to store comment info
 *
 */
public class Comment implements Serializable {
	
	/**
     * serialVersionUID
     */
	private static final long serialVersionUID = -3589566626084662546L;
	  /**
     * Comment ID
     */
	private Long idComment;
	  /**
     * Comment text
     */
	private String text;
	  /**
     * Comment date and time
     */
	private Timestamp dateTime;
	  /**
     * Event ID
     */
	private Long idEvent;
	
	public Comment() {
	}

	public Comment(Long idComment, String text, Timestamp dateTime, Long idEvent) {
		super();
		this.idComment = idComment;
		this.text = text;
		this.dateTime = dateTime;
		this.idEvent = idEvent;
	}

	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Timestamp getDateTime() {
		return dateTime;
	}
	public void setDateTime(Timestamp dateTime) {
		this.dateTime = dateTime;
	}

	public Long getIdComment() {
		return idComment;
	}

	public void setIdComment(Long idComment) {
		this.idComment = idComment;
	}

	public Long getIdEvent() {
		return idEvent;
	}

	public void setIdEvent(Long idEvent) {
		this.idEvent = idEvent;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateTime == null) ? 0 : dateTime.hashCode());
		result = prime * result + ((idComment == null) ? 0 : idComment.hashCode());
		result = prime * result + ((idEvent == null) ? 0 : idEvent.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Comment comment = (Comment) obj;
        if (idComment != null ? !idComment.equals(comment.idComment) : comment.idComment != null) return false;
        if (text != null ? !text.equals(comment.text) : comment.text != null) return false;
        if (dateTime != null ? !dateTime.equals(comment.dateTime) : comment.dateTime != null) return false;
        if (idEvent != null ? !idEvent.equals(comment.idEvent) : comment.idEvent != null) return false;

        return true;
	}

	
	
}