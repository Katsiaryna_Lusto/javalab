package com.epam.calendar.dao;

import java.io.Serializable;

import javax.sql.DataSource;

import com.epam.calendar.exception.DAOException;

public interface IGenericDAO<T, ID extends Serializable> {
	/**
	 * Creates instance with given info
	 * 
	 * @param entity
	 * @throws DAOException
	 * @return id of inserted entity
	 */
	ID create(T entity) throws DAOException;

	/**
	 * Finds instance by id
	 * 
	 * @param id
	 * @throws DAOException
	 * @return instance if find is successful, returns null otherwise
	 */
	T read(ID id) throws DAOException;

	/**
	 * Updates instance info by id
	 * 
	 * @param entity
	 * @throws DAOException
	 * @return true if update is successful, returns false otherwise
	 */
	boolean update(T entity) throws DAOException;

	/**
	 * Deletes instance by id
	 * 
	 * @param id
	 * @throws DAOException
	 * @return true if delete is successful, returns false otherwise
	 */
	boolean delete(ID id) throws DAOException;

	/**
	 * Creates dataSource bean
	 * 
	 * @param dataSource
	 */

	public void setDataSource(DataSource dataSource);

}
