package com.epam.calendar.dao.impl;

import static com.epam.calendar.util.ColumnNames.dateTime;
import static com.epam.calendar.util.ColumnNames.idComment;
import static com.epam.calendar.util.ColumnNames.idEvent;
import static com.epam.calendar.util.ColumnNames.text;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;


import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.calendar.dao.ICommentDAO;
import com.epam.calendar.exception.DAOException;
import com.epam.calendar.model.Comment;

/**
 * Class that realize operations with comment using JDBC
 *
 */
public class CommentDAO implements ICommentDAO{
	
	private DataSource dataSource;

	private static final String CREATE_COMMENT = "INSERT INTO comment (text,date_time,id_event) VALUES (?,?,?);";
	private static final String READ_COMMENT = "SELECT id_comment,text,date_time,id_event FROM  comment WHERE id_comment=?;";
	private static final String UPDATE_COMMENT = "UPDATE  comment SET text=?, date_time=?,id_event=? WHERE id_comment=?;";
	private static final String DELETE_COMMENT = "DELETE FROM  comment WHERE id_comment=?;";
	private static final String READ_COMMENTS_BY_EVENT_ID = "SELECT id_comment,text,date_time,id_event FROM  comment WHERE id_event=?;";
	
	/**
	 * Creates dataSource bean
	 * 
	 * @param dataSource
	 */
	@Override
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	/**
	 * Creates comment with given info
	 * 
	 * @param entity
	 * @return id of inserted comment, 0 if insert was unsuccessful
	 * @throws DAOException
	 */
	@Override
	public Long create(Comment entity) throws DAOException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection connection = null;
		Long insertId = 0L;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			ps = connection.prepareStatement(CREATE_COMMENT,PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setString(1, entity.getText());
			ps.setTimestamp(2, entity.getDateTime());
			ps.setLong(3,entity.getIdEvent());
			ps.execute();
			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				insertId = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return insertId;
	}
	/**
	 * Deletes comment by id
	 * 
	 * @param id
	 * @return true if delete is successful, returns false otherwise
	 * @throws DAOException
	 */
	@Override
	public boolean delete(Long id) throws DAOException {
		PreparedStatement ps = null;
		Connection connection = null;
		int deleteFlag = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			ps = connection.prepareStatement(DELETE_COMMENT);
			ps.setLong(1, id);
			deleteFlag = ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return deleteFlag == 1;
	}

	/**
	 * Updates comment info by id
	 * 
	 * @param entity
	 * @return true if update is successful, returns false otherwise
	 * @throws DAOException
	 */
	@Override
	public boolean update(Comment entity) throws DAOException {
		int updateFlag = 0;
		PreparedStatement ps = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			ps = connection.prepareStatement(UPDATE_COMMENT);
			ps.setString(1, entity.getText());
			ps.setTimestamp(2, entity.getDateTime());
			ps.setLong(3, entity.getIdEvent());
			ps.setLong(4, entity.getIdComment());
			updateFlag = ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return updateFlag == 1;
	}
	
	/**
	 * Finds comment by id
	 * 
	 * @param id
	 * @return comment if find is successful, returns null otherwise
	 * @throws DAOException
	 */
	@Override
	public Comment read(Long id) throws DAOException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Comment result = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			ps = connection.prepareStatement(READ_COMMENT);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			if (rs.next()) {
				result = buildComment(rs);
			}
		} catch (SQLException e) {
			throw new DAOException(e.getMessage());
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return result;
	}

	/**
	 * Finds all comments by event id
	 * 
	 * @return List of comments
	 * @throws DAOException
	 */
	@Override
	public List<Comment> readCommentsByEventID(Long idEvent) throws DAOException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection connection = null;
		List<Comment> resultList = new ArrayList<Comment>();
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			ps = connection.prepareStatement(READ_COMMENTS_BY_EVENT_ID);
			ps.setLong(1, idEvent);
			rs = ps.executeQuery();
			while (rs.next()) {
				Comment news = buildComment(rs);
				resultList.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
			closeConnection(connection);
		}

		return resultList;
	}
	
	/**
	 * Closes prepared statement
	 * 
	 * @param ps
	 * @throws DAOException
	 */
	private void closePreparedStatement(PreparedStatement ps) throws DAOException {
		try {
			if (ps != null) {
				ps.close();
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	/**
	 * Closes result set
	 * 
	 * @param rs
	 * @throws DAOException
	 */
	private void closeResultSet(ResultSet rs) throws DAOException {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	/**
	 * Closes connection
	 * 
	 * @param cn
	 * @throws DAOException
	 */
	private void closeConnection(Connection cn) {
		if (cn != null) {
			DataSourceUtils.releaseConnection(cn, dataSource);
		}
	}
	/**
	 * Builds comment from ResultSet
	 * 
	 * @throws SQLException
	 */
	private Comment buildComment(ResultSet rs) throws SQLException {
		Comment comment = new Comment(rs.getLong(idComment), rs.getString(text), rs.getTimestamp(dateTime),rs.getLong(idEvent));
		return comment;
	}

}
