var today = new Date();
$(document).ready(function(){

    $("#addNews").validate({
        
       rules:{ 
        
    	   title:{
                required: true,
                minlength: 10,
                maxlength: 30,
            },
            
            shortText:{
            	required: true,
                minlength: 10,
                maxlength: 50,
            },
            creationDate:{
            	equalTo : today, 
            }
       },
       
       messages:{
        
    	   title:{
                required: " Это поле обязательно для заполнения",
                minlength: " Название должно быть минимум 10 символов",
                maxlength: " Максимальное число символов - 30",
            },
            
            shortText:{
                required: " Это поле обязательно для заполнения",
                minlength: " Краткое описание должно быть минимум 10 символов",
                maxlength: " Максимальное число символов - 50",
            },   
            creationDate:{
            	equalTo : " Дата не соответствует нынешней", 
            }
       }
       
        
    });


}); //end of ready