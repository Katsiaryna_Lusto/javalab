$(document)
		.ready(
				function() {
					$("#submit")
							.click(
									function() {
										var title = $("#title").val();
										var shortText = $("#shortText").val();
										var creationDate = $("#creationDate").val();
										var errorMsg = "";

										if (title.length == 0) {
											errorMsg = " Название обязательно для заполнения";
											$('#labelTitle').text(errorMsg);
											$("#tooltipTitle").addClass("error");
										}else{
											if($("#tooltipTitle").hasClass("error"))
												$("#tooltipTitle").removeClass("error");
										}		
										
										if (shortText.length == 0) {
											errorMsg = " Краткое описание обязательно для заполнения";
											$('#labelShort').text(errorMsg);
											$("#tooltipShort").addClass("error");
										}else{
											if($("#tooltipShort").hasClass("error"))
												$("#tooltipShort").removeClass("error");
										}	
										
										if ((errorMsg = checkDate(creationDate)) != "") {
											$("#tooltipDate").addClass("error");
											$('#labelDate').text(errorMsg);
										} else if ((errorMsg = checkIfDateAfterToday(creationDate)) != "") {
											$("#tooltipDate").addClass("error");
											$('#labelDate').text(errorMsg);
										} else {
											if($("#tooltipDate").hasClass("error"))
												$("#tooltipDate").removeClass("error");
										}
										return false;
									});

					function checkDate(field) {
						var minYear = 1902;
						var maxYear = (new Date()).getFullYear();
						errorMsg = "";

						re = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;

						if (field != '') {
							var regs = field.match(re);
							if (regs) {
								if (regs[1] < 1 || regs[1] > 31) {
									errorMsg = " Неправильно введена дата: "
											+ regs[1];
								} else if (regs[2] < 1 || regs[2] > 12) {
									errorMsg = " Неправильно введен месяц: "
											+ regs[2];
								} else if (regs[3] < minYear
										|| regs[3] > maxYear) {
									errorMsg = " Неправильно введен год: "
											+ regs[3] + " - должен быть между "
											+ minYear + " и " + maxYear;
								}
							} else {
								errorMsg = " Неправильный формат даты: " + field;
							}
						} else if (field == '')
							errorMsg = " Введите дату";
						return errorMsg;
					}

					function checkIfDateAfterToday(field) {
						var today = (new Date());
						var dd = today.getDate();
						var mm = today.getMonth() + 1;
						var yyyy = today.getFullYear();
						var msg = "Дата ещё не настала";

						re = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
						var regs = field.match(re);
						if (regs[3] == yyyy) {
							if (regs[2] > mm) {
								return msg;
							}
						} else if (regs[2] = mm) {
							if (regs[1] > dd) {
								return msg;
							}
						}
						if (regs[3] > yyyy) {
							return msg;
						}
						return "";
					}
				});
