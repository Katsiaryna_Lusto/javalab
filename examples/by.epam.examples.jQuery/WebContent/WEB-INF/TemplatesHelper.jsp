<!DOCTYPE html>
<!-- To run the current sample code in your own environment, copy this to an html page. -->

<html>
<head>
	<title>TemplateHelper</title>
    <script type="text/javascript" src="js/jquery-2.2.2.js"></script>
	<script type="text/javascript" src="js/jsviews.js"></script>
</head>
<body>

<div id="team"></div>

<script id="teamTemplate" type="text/x-jsrender">
  <button id="add">Add</button>
<br/>
  {^{for members}}
      {^{:name}} 
<br/>
  {{/for}}

</script>


<script>
var team = {members: []},
	cnt = 1;
$.templates("#teamTemplate").link("#team", team).on("click", "#add", function() {
    $.observable(team.members).insert({name: "new" + cnt++})
  });
</script>

</body>
</html>
