<!DOCTYPE html>
<!-- To run the current sample code in your own environment, copy this to an html page. -->

<html>
<head>
	<title>TemplateExample</title>
    <script type="text/javascript" src="js/jquery-2.2.2.js"></script>
	<script type="text/javascript" src="js/jsviews.js"></script>
</head>

<body>
<input type="submit" value="add" onclick="add()"/>
<div id="people" class="people"></div>

<script type="text/javascript">

</script>
<script id="tmpl" type="text/x-jsrender">
<div data-link="counter">{^{:counter}}</div>
</script>


<script> 
var counter = 1;
function add() {
	var data = {"counter": counter};
	var template =  $.templates("#tmpl");
	$("<div class='com'></div>").appendTo(".people");
	template.link(".com", data);
	counter++;
}
</script>

</body>
</html>

